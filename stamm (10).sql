-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2016 at 07:07 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stamm`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('119772e622b1623dce62db794b10d45a52540c86', '::1', 1456830826, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833303532343b757365725f69647c733a313a2231223b),
('20b7db67f4eb8c078d1b70dbfa1dd10f1e26823b', '::1', 1456826759, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832363438353b757365725f69647c733a323a223134223b),
('2b10337a24f0a59518b37eed7377ed4d81bb3d75', '::1', 1456848730, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363834383132333b757365725f69647c733a313a2231223b),
('32f3cfe3196ca4aa5cc553e4dab98c130c6bb1d9', '::1', 1456827286, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832363939383b757365725f69647c733a313a2231223b),
('36f1bced420c5a5437163f3abf0bd6994019ebe1', '::1', 1456826464, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832363238333b757365725f69647c733a313a2231223b),
('3845bf8e4a3bc10c1a6b8580cc7d43f0d560d4a7', '::1', 1456765384, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736353133333b757365725f69647c733a313a2231223b),
('5a69d17f31e34b91ae2cb8dac18868c00024fd50', '::1', 1456766981, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736363639363b757365725f69647c733a313a2231223b),
('6d81cfacf0c7955909aba551ad71317073664e8f', '::1', 1456831055, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833303834353b757365725f69647c733a313a2231223b),
('72ad43c54407a5859dfe70c89c4c40cb06a86e5d', '::1', 1456835289, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833343739323b757365725f69647c733a313a2231223b),
('737fcb52190b6ee7432259efa36b6648f5316c38', '::1', 1456830336, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833303230383b757365725f69647c733a313a2231223b),
('7cb6a46f6fe99dbca5bb82aa976151a7eb5a6214', '::1', 1456827534, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832373330343b757365725f69647c733a313a2231223b),
('7ea489b694294d33a590c75d79a7c6fea7a10c3a', '::1', 1456829266, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832383938313b757365725f69647c733a313a2231223b),
('89c2796379313365ff559e47b6d34d637ebfe5b1', '::1', 1456766250, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736363137363b757365725f69647c733a313a2231223b),
('90f4915bbef55583a453b4a69527f1a7bd655682', '::1', 1456764946, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736343636373b757365725f69647c733a313a2231223b),
('942361a55c4e12a0dd62676937181ff5b6a110f4', '::1', 1456767479, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736373137353b757365725f69647c733a313a2231223b),
('9994e14e3014234759fb9300cfdd6924f7e71bc3', '::1', 1456825118, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832353131383b),
('a3c9c9787b0c9a37230fac2503ef56564d7601ae', '::1', 1456840432, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363834303033323b757365725f69647c733a313a2231223b),
('a53e3df76bc4ad110b7b82675b21a7fef404ce34', '::1', 1456840871, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363834303435313b757365725f69647c733a313a2231223b),
('b49134ee6cdcbd08ae2b7d1393e76cf02c79944b', '::1', 1456841425, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363834313139333b757365725f69647c733a313a2231223b),
('be2e9ec07f743a52ddd31030b9a457294dd76bf0', '::1', 1456827890, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832373631323b757365725f69647c733a313a2231223b),
('c096be36cf9384d400d95dfd0383e2b9cb5260b8', '::1', 1456767800, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736373538373b757365725f69647c733a313a2231223b),
('cd6987b851e21ba5c39cc43071c9cb1d3d232d98', '::1', 1456830159, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832393736333b757365725f69647c733a313a2231223b),
('cf2b5c00fbdcb6b6ac48ce876584db1b143df4c6', '::1', 1456825469, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832353131393b757365725f69647c733a313a2231223b),
('d2442782d2edf8d7994a9eb668bf479150c5264d', '::1', 1456848739, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363834383733323b757365725f69647c733a313a2231223b),
('d90af6dacff44392d38d1b190ae5c44c7d6d959a', '::1', 1456764290, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736333739303b757365725f69647c733a313a2231223b),
('db397b7e1f2cd0782934608556e491ce0b844bb0', '::1', 1456829663, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832393431303b757365725f69647c733a313a2231223b),
('dca2234729307a5609738d8494da18c7aa15ed75', '::1', 1456762580, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736323537333b757365725f69647c733a313a2231223b),
('dcb5970f80f32d265a9e38a318b33784ba3f8e6d', '::1', 1456833822, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833333739353b757365725f69647c733a313a2231223b),
('def7ce4d603ab9edea026ac5836e4dc8a7acd540', '::1', 1456832440, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833323434303b757365725f69647c733a313a2231223b),
('e21ca10f230759fa6ead569a4fb917e6d2b22594', '::1', 1456839887, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833393539383b757365725f69647c733a313a2231223b),
('e5bbb0a718730efec164e976b72706359fb1855e', '::1', 1456828949, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363832383636383b757365725f69647c733a313a2231223b),
('ea88ca9beddec1184ae973c4e99fc6d31f1a1045', '::1', 1456834614, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363833343438363b757365725f69647c733a313a2231223b),
('f654fa4b963b345d501e4c7f6423536384808f53', '::1', 1456768678, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363736383637323b757365725f69647c733a313a2231223b);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`) VALUES
(1, 'Client 1'),
(2, 'Client 2'),
(3, 'Client 3');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `address` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `email` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `phone` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `is_supplier` int(1) NOT NULL DEFAULT '0',
  `is_company` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `address`, `email`, `phone`, `is_supplier`, `is_company`) VALUES
(2, 'Contact 1', 'teststrasse 23\r\n1030 wien\r\nösterreich', 'test@test.com', '123124235324', 0, 0),
(3, 'Contact person 2', 'test', 'asdf@jkl.asd', '12345678', 0, 0),
(4, 'Contact 3 (supplier)', '', '', '', 1, 0),
(5, 'Contact 4 (company)', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contract`
--

CREATE TABLE IF NOT EXISTS `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maintenance_number` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `service_agreement_number` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `company` text CHARACTER SET latin1,
  `external_contact` int(255) DEFAULT NULL,
  `internal_contact` int(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `costs` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `comment` text CHARACTER SET latin1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contract`
--

INSERT INTO `contract` (`id`, `maintenance_number`, `service_agreement_number`, `company`, `external_contact`, `internal_contact`, `start_date`, `end_date`, `costs`, `comment`) VALUES
(1, '0001', '123456', 'My company', 3, 2, '2015-12-15', '2015-12-31', '123', 'smthing');

-- --------------------------------------------------------

--
-- Table structure for table `contract_documents`
--

CREATE TABLE IF NOT EXISTS `contract_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(255) DEFAULT NULL,
  `contract_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contract_objects`
--

CREATE TABLE IF NOT EXISTS `contract_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(255) NOT NULL,
  `object_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `costcenter`
--

CREATE TABLE IF NOT EXISTS `costcenter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `costcenter`
--

INSERT INTO `costcenter` (`id`, `name`) VALUES
(1, 'KST 001'),
(2, 'KST 002'),
(3, 'KST 003'),
(4, 'KST 004');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text,
  `comment` text,
  `user` int(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipmentgroup`
--

CREATE TABLE IF NOT EXISTS `equipmentgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `equipmentgroup`
--

INSERT INTO `equipmentgroup` (`id`, `name`) VALUES
(2, 'Gruppe 1'),
(3, 'Gruppe 2'),
(4, 'Gruppe 3'),
(5, 'Gruppe 4'),
(7, 'asdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tablename` varchar(50) NOT NULL,
  `fieldname` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE IF NOT EXISTS `histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int(255) DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `object`
--

CREATE TABLE IF NOT EXISTS `object` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(5) unsigned zerofill DEFAULT NULL,
  `inventory_number` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `equipmentgroup_id` int(255) DEFAULT NULL,
  `name` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT '',
  `description` text CHARACTER SET latin1,
  `location` varchar(500) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `purchase_date` date DEFAULT NULL,
  `installation_date` date DEFAULT NULL,
  `warranty_date` date DEFAULT NULL,
  `purchase_price` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `serial_number` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `manufacturer_service_number` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `client_id` int(255) DEFAULT NULL,
  `created_costcenter` int(255) DEFAULT NULL,
  `booked_costcenter` int(255) DEFAULT NULL,
  `serviceinterval` int(255) DEFAULT NULL,
  `next_maintenance` date DEFAULT NULL,
  `last_maintenance` date DEFAULT NULL,
  `GLT_number` varchar(255) CHARACTER SET latin1 DEFAULT '',
  `supplier_contact_id` int(255) DEFAULT NULL,
  `contact1` int(255) DEFAULT NULL,
  `contact2` int(255) DEFAULT NULL,
  `status_id` int(1) DEFAULT NULL,
  `retired_date` date DEFAULT NULL,
  `contract_id` int(255) DEFAULT NULL,
  `image_fname` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `object`
--

INSERT INTO `object` (`id`, `object_id`, `inventory_number`, `equipmentgroup_id`, `name`, `manufacturer`, `description`, `location`, `purchase_date`, `installation_date`, `warranty_date`, `purchase_price`, `serial_number`, `manufacturer_service_number`, `client_id`, `created_costcenter`, `booked_costcenter`, `serviceinterval`, `next_maintenance`, `last_maintenance`, `GLT_number`, `supplier_contact_id`, `contact1`, `contact2`, `status_id`, `retired_date`, `contract_id`, `image_fname`) VALUES
(1, 00001, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, '1456330755_1pMXoedrdBLW.jpg'),
(2, 00002, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', NULL, '2009-01-14', NULL, '', '0123456789', '', 1, 1, 1, 3, '2016-07-21', NULL, '', 3, 2, 3, 3, NULL, 1, ''),
(3, 00003, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', NULL, '2009-01-14', NULL, '', '0123456789', '', 1, 0, 0, 10, '2016-07-21', NULL, '', 2, 2, 3, 2, NULL, 1, ''),
(4, 00004, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', NULL, '2009-01-14', NULL, '', '0123456789', '', 1, 0, 0, 14, '2016-07-21', NULL, '', 2, 2, 3, 4, NULL, 1, ''),
(6, 00006, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', NULL, '2009-01-14', NULL, '', '0123456789', '', 1, 1, 2, 9, '2016-07-21', NULL, '', 2, 5, 3, 3, NULL, 1, ''),
(7, 00007, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', NULL, '2009-01-14', NULL, '', '0123456789', '', 1, 0, 0, 3, '2016-07-21', NULL, '', 2, 2, 3, 2, NULL, 1, ''),
(8, 00008, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', NULL, '2013-02-01', NULL, '', '0123456789', '', 1, 1, 1, 36, '2016-07-21', '2016-02-03', 'asdfasdf', 3, 2, 3, 2, NULL, 1, ''),
(9, 00009, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', NULL, '2016-01-14', NULL, '', '0123456789', '', 1, 0, 0, 20, '2016-07-21', NULL, '', 2, 2, 3, 3, NULL, 1, ''),
(10, 00010, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, '1456330605_O3GxdxBFKPwG.jpg'),
(11, 00011, '<<asdfasdf<<<<', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, '1456330614_yXbuNTmoar.jpg'),
(12, 00012, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(13, 00013, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', '2016-02-08', 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(14, 00014, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(15, 00015, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(16, 00016, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', '2016-02-03', 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(17, 00017, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(18, 00018, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(19, 00019, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(20, 00020, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(21, 00021, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', '2016-02-03', 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(22, 00022, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'kill me please', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(23, 00023, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(24, 00024, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(25, 00025, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(26, 00026, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 2, NULL, 1, ''),
(27, 00027, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(28, 00028, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(29, 00029, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(30, 00030, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(31, 00031, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(32, 00032, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(33, 00033, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(34, 00034, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(35, 00035, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(36, 00036, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(37, 00037, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(38, 00038, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(39, 00039, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(40, 00040, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(41, 00041, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(42, 00042, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(43, 00043, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(44, 00044, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(45, 00045, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(46, 00046, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(47, 00047, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(48, 00048, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(49, 00049, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(50, 00050, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(51, 00051, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(52, 00052, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(53, 00053, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(54, 00054, 'AAAASDFFSSDD', 2, 'testobjectaaaa', '', 'asdfasdfasdfasdfaaaaa', 'vienna', '1996-03-19', '2014-01-08', NULL, '3000', '0123456789', 'aaaaa', 1, 1, 1, 2, '2016-07-21', NULL, 'asdfasdf', 3, 2, 3, 1, NULL, 1, ''),
(58, 00058, 'AAAASDFFSSDD', 2, 'testobject', '', '', 'vienna', '0000-00-00', '2014-01-20', '0000-00-00', '', '0123456789', '', 1, 1, 2, 9, NULL, NULL, '', 2, 5, 3, 3, '0000-00-00', 1, ''),
(59, 00059, 'aöskdjfföalsk', 2, 'asdfasdfasdfasdfa', 'sdfasdfasdf', 'asdfasdfasdfasdf', '', '1970-01-01', '1970-01-01', '1970-01-01', '', 'aölskljslkdfj', 'ösldkfjaösldkfj', 1, 1, 1, 3, NULL, NULL, 'alskdjfaaaaaaa', NULL, NULL, NULL, 1, '1970-01-01', NULL, ''),
(60, 00060, 'asdfasdf', NULL, 'asdfasdf', 'asdfasdf', '', '', '1970-01-01', '1970-01-01', '1970-01-01', '', 'asdfasdfasdf', 'asdfasdfasdf', NULL, 1, 1, 2, NULL, NULL, 'asdfasdf', 2, 2, 2, 1, '1970-01-01', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `objectfiles`
--

CREATE TABLE IF NOT EXISTS `objectfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(255) DEFAULT NULL,
  `filedescription` varchar(500) NOT NULL DEFAULT '',
  `filename` varchar(255) DEFAULT '',
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `objectfiles`
--

INSERT INTO `objectfiles` (`id`, `object_id`, `filedescription`, `filename`, `createddate`, `createdby`) VALUES
(7, NULL, 'adfasdfff', '1453733217_zh19ZHcNN2An.png', '2016-01-25 14:46:57', 1),
(8, NULL, 'asdfasdfasdfa', '1453733502_3hITJV8jb59d.jpg', '2016-01-25 14:51:42', 1),
(9, NULL, 'asdfasdfasdfasdf', '1453733540_08l4V1GYnIx.jpg', '2016-01-25 14:52:20', 1),
(10, 1, 'asdfasdf', '1453733590_A3q2xjLO4YZ.jpg', '2016-01-25 14:53:10', 1),
(11, 1, 'asdfasdfasdfasdf', '1453733662_Nyy5MykUtgU.png', '2016-01-25 14:54:22', 1),
(12, 1, 'aaasdffasdf', '1453737984_m8V13DxliXzN.png', '2016-01-25 16:06:24', 1),
(13, 1, 'aaaaaaaaaaa', '1453738001_38fKLjv8kMbI.jpg', '2016-01-25 16:06:41', 1),
(14, 1, 'fiiiiiiiiiiiiiiiii', '1453738021_P8nH3e3DMgi3.jpg', '2016-01-25 16:07:01', 1),
(15, 1, 'asdfasdf', '1453740497_G3yxlpM0ps1J.jpg', '2016-01-25 16:48:17', 1),
(16, 1, 'asdfasdf', '1454501834_Tphu6GiYVcqs.jpg', '2016-02-03 12:17:14', 1),
(17, 2, 'asdfasdfasdf', '1454501927_dqSe00eqrOMU.jpg', '2016-02-03 12:18:47', 1),
(18, 1, 'testlogo', '1456139286_98BJxltbGYs.png', '2016-02-22 11:08:06', 1),
(19, 1, 'aaaaaa', '1456139307_cEipayA4se92.png', '2016-02-22 11:08:27', 1),
(20, 1, 'asdfasdf', '1456139427_bUbJ4NjeCrr.jpg', '2016-02-22 11:10:27', 1),
(21, 11, '<yxc<yxc', '1456327906_veF59diMZMP.pdf', '2016-02-24 15:31:46', 1),
(22, 11, 'asdfasdf', '1456327920_q7txGFTMfJr.jpg', '2016-02-24 15:32:00', 1),
(23, 11, 'aaaaa', '1456328712_RxCwFHVLjeyd.jpg', '2016-02-24 15:45:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `objecthistory`
--

CREATE TABLE IF NOT EXISTS `objecthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(255) DEFAULT NULL,
  `comment` text NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `objecthistory`
--

INSERT INTO `objecthistory` (`id`, `object_id`, `comment`, `createdby`, `createddate`) VALUES
(1, 2, 'testcomment', 1, '2016-01-11 11:09:25'),
(2, 2, 'asdfasdfasdf\r\nasdfasdfasdf\r\nasdfasdfasdf\r\nasdfasdfasdf\r\nasdfasdfasdf', 1, '2016-01-11 11:09:25'),
(3, 2, 'asdfasdfasdf', 1, '2016-01-12 10:18:41'),
(5, 2, 'asdfasdfsdfasdfasdf', 1, '2016-01-12 10:21:24'),
(6, 2, 'asdfasdf', 1, '2016-01-12 10:24:30'),
(7, 2, 'asdfasdf', 1, '2016-01-12 10:30:01'),
(8, 2, 'aaaaaaaa', 1, '2016-01-12 10:37:42'),
(9, 1, 'asdfasdfasdf', 1, '2016-01-12 10:38:30'),
(10, 1, 'aaaaa', 1, '2016-01-12 10:38:39'),
(11, 1, 'ffffdsasdfasdf', 1, '2016-01-12 10:38:42'),
(12, 1, 'asdfasdfasdf', 1, '2016-01-12 10:41:36'),
(13, 2, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, '2016-01-21 23:00:00'),
(14, 5, 'adasdfasdfasdfasdfasd\r\nfasdfas\r\ndfasdfasdfasdf\r\nasdfasdfasdfasdf', 1, '2016-01-22 13:04:17'),
(15, 58, 'asdfasdfasdf', 1, '2016-01-26 17:35:41'),
(16, 58, 'asdfasdfasdfasdfasdfasdfasdfa\r\nsdfasd\r\nfa\r\nsdf\r\nasdfasdfasfd', 1, '2016-01-26 17:35:49'),
(17, 7, 'testadfasdfasdfa\r\ndfas\r\ndf\r\nas\r\ndfasdfasdf', 1, '2016-02-03 11:43:31'),
(18, 16, 'Ein Service wurde durchgeführt!', 1, '2016-02-03 11:57:10'),
(19, 21, 'Ein Service wurde durchgeführt!', 1, '2016-02-03 11:59:04'),
(20, 8, 'Ein Service wurde durchgeführt!', 1, '2016-02-03 12:34:21'),
(21, 13, 'test test test \nservice test', 1, '2016-02-08 10:35:12');

-- --------------------------------------------------------

--
-- Table structure for table `objectstatus`
--

CREATE TABLE IF NOT EXISTS `objectstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `objectstatus`
--

INSERT INTO `objectstatus` (`id`, `name`) VALUES
(1, 'NEU'),
(2, 'AKTIV'),
(3, 'INAKTIV'),
(4, 'EX');

-- --------------------------------------------------------

--
-- Table structure for table `sorting`
--

CREATE TABLE IF NOT EXISTS `sorting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tablename` varchar(50) NOT NULL,
  `fieldname` varchar(100) NOT NULL,
  `direction` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `sorting`
--

INSERT INTO `sorting` (`id`, `user_id`, `tablename`, `fieldname`, `direction`) VALUES
(59, 1, 'object', 'object_id', 'desc'),
(67, 1, 'user', 'lastname', 'asc'),
(69, 1, 'client', 'name', 'asc'),
(72, 1, 'equipmentgroup', 'name', 'desc');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `pword` varchar(255) NOT NULL DEFAULT '$2a$08$PKNAW4qBFK26mf8Kc/2xp.AzmfAIyjMGKRT2N.DvjbmRsWcMslieO',
  `role` int(1) NOT NULL DEFAULT '1',
  `items_per_page` int(11) NOT NULL DEFAULT '30',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `email`, `pword`, `role`, `items_per_page`) VALUES
(1, 'admin', 'admin', 'admin', 'developer@istvanszilagyi.com', '$2a$08$0hkJsE1M8.1MxiKwimNZ8OyoLUyN/rYsPJvta2zfn/yO8dUaMr7ke', 0, 30),
(14, 'user1', 'user1', 'user1', 'junior.developer@istvanszilagyi.com', '$2a$08$1eMSqh9run8fEwpcypyKJuptNZLTqBTIVeQW0VRaaalnhABWCeOra', 1, 30),
(17, 'user23', '3', '3', 'asdf@asdf.com', '$2a$08$x2JDQaJYscchPKWIE8.44eayDodJZwkwvD0k9VvzHL9UjHGS80xNW', 0, 30);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
