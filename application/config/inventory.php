<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('INV_DATEFORMAT', 'd.m.Y');

define('USER_PRIV_ADMIN', 0);
define('USER_PRIV_EDIT', 1);
define('USER_PRIV_VIEW', 2);


$config['user_role_names'] = array(
    USER_PRIV_ADMIN => 'Administrator',
    USER_PRIV_EDIT => 'Editor',
    USER_PRIV_VIEW => 'Benutzer',
);

define('CONTACT_NOSUPPLIER', 0);
define('CONTACT_SUPPLIER', 1);

$config['contact_supplier_options'] = array(
    CONTACT_NOSUPPLIER => 'Nein',
    CONTACT_SUPPLIER => 'Ja',
);

define('OBJECTSTATUS_NEW', 1);
define('OBJECTSTATUS_ACTIVE', 2);
define('OBJECTSTATUS_INACTIVE', 3);
define('OBJECTSTATUS_EX', 4);


define('NOTIFICATION_TYPE_CONTRACTEXPIRE', 0);