<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MyContact extends MyCore
{
    protected $contactId = null;
    protected $contact = null;
    protected $contactData = null;
    
    public function __construct($contactId)
    {
        $this->ci = & get_instance();
        $this->ci->load->model('Inventory_model', 'im');
        $this->contactId = $contactId;
        $this->ContactData();
    }
    
    private function ContactData()
    {
        $data = array();
        $this->contact = $this->ci->im->getContactById($this->contactId)->row();
        
        $data['contact'] = $this->contact;
        
        $this->contactData = $data;
    }
    
    public static function contactList()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $ci->load->helper('cookie');
        
        $sorting = MyContact::getSorting('contact');
        $filters = MyContact::getFilters('contact');
        
        if($sorting != null)
        {
            $data['sortfield'] = $sorting->fieldname;
            $data['sortdirection'] = $sorting->direction;
        }
        else
        {
            $data['sortfield'] = 'name';
            $data['sortdirection'] = 'asc';
        }    
        
        $contacts = $ci->im->getContactsLimited($data['sortfield'], $data['sortdirection'], $filters)->result_array();
        
        if($data['sortdirection'] == 'asc')
            $data['sorticon'] = 'ui-icon-circle-triangle-s';
        else
            $data['sorticon'] = 'ui-icon-circle-triangle-n';
        
        foreach($contacts as $key => $value)
        {
            $contacts[$key]['isSupplier'] = $ci->config->item('contact_supplier_options')[$value['is_supplier']];
        }
        $data['contacts'] = $contacts;
        
        return $ci->load->view('inventory/contact_list', $data, true);
    }
    
    public static function contactMenu()
    {
        $ci = & get_instance();
        $data['filterActive'] = count(MyCore::getFilters('contact')) > 0 ? true : false;
        return $ci->load->view('inventory/contact_menu', $data, true);        
    }
    
    public static function addView()
    {
        $ci = & get_instance();
        $data = array();
        $data['modeForm'] = 'contactAddForm';
        
        $data['contact'] = (object)array(
            'id' => null,
            'name' => '',
            'address' => '',
            'email' => '',
            'phone' => '',
            'is_supplier' => -1,    
        );
        
        $data['supplierOptions'] = array(
            CONTACT_NOSUPPLIER => $ci->lang->line('no'),
            CONTACT_SUPPLIER => $ci->lang->line('yes'),
        );
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $ci->lang->line('contact_title_new'),
                'html' => $ci->load->view('inventory/contact_edit', $data, true),
                'contactId' => -2),
            'message' => 'Contact found!',
        ));
    }
    
    public static function insert($data)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        unset($data['id']);
        if(MyContact::validateContact($data))
        {
            require_once(APPPATH.'libraries/PasswordHash.php');
            
            $newId = $ci->im->insertContact($data);
            if($newId != null)
            {
                $success = true;
                $message = '';
            }
            else
            {
                $success = false;
                $message = $ci->lang->line('error_insert');
            }
                
            echo json_encode(array(
                'success' => $success,
                'new_id' => $newId,
                'message' => $message,
            ));
        }
    }
    
    public function clone_()
    {
        $data = $this->contactData;
        $data['modeForm'] = 'contactAddForm';
        
        $data['supplierOptions'] = array(
            CONTACT_NOSUPPLIER => $this->ci->config->item('contact_supplier_options')[CONTACT_NOSUPPLIER],
            CONTACT_SUPPLIER => $this->ci->config->item('contact_supplier_options')[CONTACT_SUPPLIER],
        );
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('contact_title_new'),
                'html' => $this->ci->load->view('inventory/contact_edit', $data, true),
                'contactId' => -2),
            'message' => 'Contact found!',
        ));
    }
    
    public function view()
    {
        $data['overview'] = $this->overviewView();
        $data['edit'] = $this->editView();
        $data['contact'] = $this->contact;
        	
        $html = $this->ci->load->view('inventory/contact_detail', $data, true);
         
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('contact_tab') . ' - ' . $this->contact->name,
                'html' => $html,
                'contactId' => $this->contact->id),
            'message' => 'Contact found!',
        ));
    }
    
    private function overviewView()
    {
        $data['contact'] = $this->contact;
        $data['supplierOption'] = $this->ci->config->item('contact_supplier_options')[$this->contact->is_supplier];
        
        return $this->ci->load->view('inventory/contact_overview', $data, true);
    }
    
    private function editView($mode = 'edit')
    {
        $data = $this->contactData;
        $data['supplierOptions'] = array(
            CONTACT_NOSUPPLIER => $this->ci->config->item('contact_supplier_options')[CONTACT_NOSUPPLIER],
            CONTACT_SUPPLIER => $this->ci->config->item('contact_supplier_options')[CONTACT_SUPPLIER],
        );
        
        $data['modeForm'] = $mode == 'edit' ? 'contactEditForm' : 'contactAddForm';
        return $this->ci->load->view('inventory/contact_edit', $data, true);
    }
    
    
    public function update($data)
    {
        if(MyContact::validateContact($data))
        {
            $id = $data['id'];
            unset($data['id']);
            
            $this->ci->im->updateContact($id, $data);
            $this->ContactData();
            
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('success_update'),
                'overview' => $this->overviewView(),
            ));
        }
    }
    
    public function delete()
    {
        $this->ci->im->deleteContact($this->contactId);
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete')
        ));
    }
    
    public static function validateContact($data)
    {
        $ci = & get_instance();
        $ci->load->helper('form');
        
        $form_validation = new Besc_Form_validation();
        $form_validation->set_data($data);

        
        $form_validation->set_rules('name', $ci->lang->line('contactlist_name'), 'required|max_length[255]');
        $form_validation->set_rules('address', $ci->lang->line('contactlist_email'), 'max_length[255]');
        $form_validation->set_rules('email', $ci->lang->line('contactlist_email'), 'valid_email|max_length[255]');
        $form_validation->set_rules('phone', $ci->lang->line('contactlist_phone'), 'max_length[255]');
        $form_validation->set_rules('is_supplier', $ci->lang->line('contactlist_issupplier'), 'required');
        
        
        if ($form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => $form_validation->error_string('', '')));
        }
        else
            return true;
    }
    
 
    
    public static function filterView()
    {
        $ci = & get_instance();
        $data = array();
        
        $data['supplierOptions'] = array(
            CONTACT_NOSUPPLIER => $ci->config->item('contact_supplier_options')[CONTACT_NOSUPPLIER],
            CONTACT_SUPPLIER => $ci->config->item('contact_supplier_options')[CONTACT_SUPPLIER],
        );
        $data['filters'] = MyContact::getFilters('contact');
        
        return $ci->load->view('inventory/contact_filter', $data, true);
    }
    
}

?>