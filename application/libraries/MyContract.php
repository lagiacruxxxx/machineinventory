<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyContract
{
    protected $contractId = null;
    protected $contract = null;
    protected $contractData = null;
    protected $ci = null;

    public function __construct($id)
    {
        $this->ci = & get_instance(); 
        $this->ci->load->model('Inventory_model', 'im');
        
        
        $contract = $this->ci->im->getContractById($id);
        if($contract->num_rows() == 1)
        {
            $this->contractId = $id;
            $this->contractData();
        }
        else 
            die('Object not found!');
    }
    
    private function contractData()
    {
        $data = array();
        $this->contract = $this->ci->im->getContractById($this->contractId)->row();
        
        $data['contract'] = $this->contract;
        $data['contact1'] = $this->ci->im->getContactById($this->contract->external_contact)->row();
        $data['contact2'] = $this->ci->im->getContactById($this->contract->internal_contact)->row();
        $data['contacts'] = $this->ci->im->getContacts();
        $data['autorenewOptions'] = array(
            0 => 'Nein',
            1 => 'Ja',
        );
        
        $this->contractData = $data;
    }
    
    public static function contractList()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $ci->load->helper('cookie');
        
        $sorting = MyContact::getSorting('contract');
        $filters = MyContact::getFilters('contract');
        
        if($sorting != null)
        {
            $data['sortfield'] = $sorting->fieldname;
            $data['sortdirection'] = $sorting->direction;
        }
        else
        {
            $data['sortfield'] = 'maintenance_number';
            $data['sortdirection'] = 'asc';
        }    
        
        $contracts = $ci->im->getContractsLimited($data['sortfield'], $data['sortdirection'], $filters)->result_array();

        
        if($data['sortdirection'] == 'asc')
            $data['sorticon'] = 'ui-icon-circle-triangle-s';
        else
            $data['sorticon'] = 'ui-icon-circle-triangle-n';
        
        $data['contracts'] = $contracts;
        
        return $ci->load->view('inventory/contract_list', $data, true);
    }
    
    public static function contractMenu()
    {
        $ci = & get_instance();
        return $ci->load->view('inventory/contract_menu', array(), true);        
    }
    
    public static function addView()
    {
        $ci = & get_instance();
        $data = array();
        $data['modeForm'] = 'contractAddForm';
        
        $contract['id'] = null;
        $contract['maintenance_number'] = '';
        $contract['service_agreement_number'] = '';
        $contract['company'] = '';
        $contract['external_contact'] = -1;
        $contract['internal_contact'] = -1;
        $contract['start_date'] = null;
        $contract['costs'] = '';
        $contract['comment'] = '';
        $contract['auto_renew'] = 1;
        $contract['renewInterval'] = 12;
        $contract['reminderTolerance'] = 3;
        
        $data['contract'] = (object)$contract;
        $data['contacts'] = $ci->im->getContacts();
        $data['autorenewOptions'] = array(
            0 => 'Nein',
            1 => 'Ja',
        );
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $ci->lang->line('contract_title_new'),
                'html' => $ci->load->view('inventory/contract_edit', $data, true),
                'objectId' => -2),
            'message' => 'Contract found!',
        ));
    }
    
    public static function insert($data)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        unset($data['id']);
        if(MyContract::validateContract($data))
        {
            $data['external_contact'] = $data['external_contact'] == -1 ? null : $data['external_contact'];
            $data['internal_contact'] = $data['internal_contact'] == -1 ? null : $data['internal_contact'];
            
            $data['start_date'] = $data['start_date'] != '' ? date('Y-m-d', strtotime($data['start_date'])) : null;
            $data['end_date'] = date('Y-m-d', strtotime($data['start_date'] . " + 365 day"));
            $newId = $ci->im->insertContract($data);
            if($newId != null)
            {
                $success = true;
                $message = '';
            }
            else
            {
                $success = false;
                $message = $ci->lang->line('error_insert');
            }
                
            echo json_encode(array(
                'success' => $success,
                'new_id' => $newId,
                'message' => $message,
            ));
        }
    }
    
    public function clone_()
    {
        $data = $this->contractData;
        $data['modeForm'] = 'contractAddForm';
    
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('contract_title_new'),
                'html' => $this->ci->load->view('inventory/contract_edit', $data, true),
                'objectId' => -2),
            'message' => 'Contract found!',
        ));
    }
    
    public function view()
    {
        $data['overview'] = $this->overviewView();
        $data['edit'] = $this->editView();
        $data['files'] = $this->filesView();
        $data['objects'] = $this->objectView();
        $data['objectAdd'] = $this->objectAddView(array());
        $data['contract'] = $this->contract;
        $data['filterview'] = $this->objectFilterView();
        	
        $html = $this->ci->load->view('inventory/contract_detail', $data, true);
         
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('contractlist_header') . ' - ' . $this->contract->company,
                'html' => $html,
                'contractId' => $this->contractId),
            'message' => 'Object found!',
        ));
    }
    
    private function overviewView()
    {
        return $this->ci->load->view('inventory/contract_overview', $this->contractData, true);
    }
    
    private function editView($mode = 'edit')
    {
        $data = $this->contractData;
        $data['modeForm'] = $mode == 'edit' ? 'contractEditForm' : 'contractAddForm';
        return $this->ci->load->view('inventory/contract_edit', $data, true);
    }
    
    private function objectView()
    {
        $objects = $this->ci->im->getObjectsByContractId($this->contractId)->result_array();
        
        foreach($objects as $key => $object)
        {
            $objects[$key]['nextMaintenance'] = MyObject::calculateNextMaintenance((object)$object);
        }
        
        $data['objects'] = $objects;
        
        return $this->ci->load->view('inventory/contract_objectlist', $data, true);
    }
    
    private function objectFilterView()
    {
        $data['contracts'] = $this->ci->im->getContracts();
        
        return $this->ci->load->view('inventory/contract_objectfilter', $data, true);
    }
    
    private function objectAddView($filters)
    {
        $objects = $this->ci->im->getObjectsToAdd($this->contractId, $filters)->result_array();
        foreach($objects as $key => $object)
        {
            $objects[$key]['nextMaintenance'] = MyObject::calculateNextMaintenance((object)$object);
            $objects[$key]['contract_name'] = $object['contract_id'] != null ? $this->ci->im->getContractById($object['contract_id'])->row()->company : '';
        }
    
        $data['objects'] = $objects;
    
        return $this->ci->load->view('inventory/contract_objectadd', $data, true);
    }
    
    
    private function filesView()
    {
        $data['upload_data'] = $this->ci->im->getContractFilesByContractID($this->contractId);
        
        return $this->ci->load->view('inventory/contract_files', $data, true);
    }
    
    public function update($data)
    {
        if(MyContract::validateContract($data))
        {
            $id = $data['id'];
            unset($data['id']);
            $data['start_date'] = $data['start_date'] != '' ? date('Y-m-d', strtotime($data['start_date'])) : null;
            
            $data['external_contact'] = $data['external_contact'] == -1 ? null : $data['external_contact'];
            $data['internal_contact'] = $data['internal_contact'] == -1 ? null : $data['internal_contact'];
            
            $this->ci->im->updateContract($data, $id);
            $this->ContractData();
            
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('success_update'),
                'overview' => $this->overviewView(),
            ));
        }
    }
    
    public function delete()
    {
        $this->ci->im->deleteContract($this->contractId);
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete')
        ));
    }
    
    public static function validateContract($data)
    {
        $ci = & get_instance();
        $ci->load->helper('form');
        $ci->load->library('form_validation');
        $ci->form_validation->set_data($data);

        $ci->form_validation->set_rules('company', $ci->lang->line('contractlist_company'), 'required|max_length[255]');
        $ci->form_validation->set_rules('maintenance_number', $ci->lang->line('contractlist_maintenancenumber'), 'required|max_length[255]');
        $ci->form_validation->set_rules('service_agreement_number', $ci->lang->line('contractlist_serviceagreementnumber'), 'max_length[255]');
        $ci->form_validation->set_rules('start_date', $ci->lang->line('contractlist_startdate'), 'required');
        $ci->form_validation->set_rules('renewInterval', $ci->lang->line('contractlist_renewinterval'), 'required|is_natural');
        $ci->form_validation->set_rules('reminderTolerance', $ci->lang->line('contractlist_reminderTolerance'), 'required|is_natural');
        
        if ($ci->form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => validation_errors()));
        }
        else
            return true;
    }
    
    
    public function insertFile($data, $files)
    {
        if($this->validateFile($data, $files))
        {
            $filename = $this->ci->input->post('filename');
            $desc = $this->ci->input->post('filedescription');
            $contractId = $this->ci->input->post('contract_id');
            
            $rnd = rand_string(12);
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $serverFile = time() . "_" . $rnd . "." . $ext;
            
            $success = move_uploaded_file($files['data']['tmp_name'], getcwd() . "/items/uploads/contractfiles/$serverFile");
            
            if($success)
            {
                $data = array(
                    'contract_id' => $contractId,
                    'filedescription' => $desc,
                    'filename' => $serverFile,
                    'createdby' => $this->ci->user->id,
                    'createddate' => date('Y-m-d H:i:s'),
                );
            
                $success = $this->ci->im->insertContractFile($data);
                if($success)
                {
                    $msg = $this->ci->lang->line('success_update');
                    $filelist = $this->filesView();
                }
                else
                {
                    $msg = $this->ci->lang->line('error_update');
                    $filelist = '';
                }
                
                echo json_encode(array(
                    'success' => true,
                    'message' => $msg,
                    'files' => $filelist,
                ));
            }
        }
    }
    
    private function validateFile($data, $files)
    {
        $this->ci->load->helper('form');
        $this->ci->load->library('form_validation');
        $this->ci->form_validation->set_data($data);
        
        $this->ci->form_validation->set_rules('filedescription', $this->ci->lang->line('objectdetail_files_textdesc'), 'required|max_length[255]|min_length[5]');
        
        if ($this->ci->form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => validation_errors()));
        }
        else
            return true;
    }
    
    public static function filterView()
    {
        $ci = & get_instance();
        $data = array();
        $ci->load->helper('cookie');
        
        return $ci->load->view('inventory/contract_filter', $data, true);
    }
    
    public static function serviceView()
    {
        $ci = & get_instance();
        return $ci->load->view('inventory/contract_service', array(), true);
    }
    
    public function serviceObjects()
    {
        foreach(json_decode($this->ci->input->post('objects')) as $objectId)
        {
            $object = new MyObject($objectId);
            $object->service($this->ci->input->post('comment'), false);
        }
    
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('object_servicecomment'),
            'table' => $this->objectView(),
        ));
    }
    
    public function addObjects()
    {
        $objects = array();
        foreach(json_decode($this->ci->input->post('objects')) as $objectId)
        {
            $objects[] = $objectId;
        }
        $this->ci->im->setObjectContractId($objects, $this->ci->input->post('contractId'));
    
        $filters = array();
        if($this->ci->input->post('filters') != null)
        {
            foreach($this->ci->input->post('filters') as $filter)
            {
                $filters[$filter[0]] = $filter[1];
            }
        }
        
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('contract_addobject'),
            'table' => $this->objectAddView($filters),
            'list' => $this->objectView(),
        ));
    }
    
    
    public static function serviceContracts()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        
        
        foreach($ci->im->getContracts()->result() as $contract)
        {
            $new = date('Y-m-d',strtotime($contract->end_date . " + " . $contract->renewInterval . " month"));
            
            if(strtotime(date('Y-m-d')) > strtotime($contract->end_date) && $contract->auto_renew == 1)
            {
                $ci->im->serviceContract($contract->id, $new);
                $ci->im->deleteNotifications($contract->id, NOTIFICATION_TYPE_CONTRACTEXPIRE);
            }
        }
    }
    
    public static function checkExpireNotifications()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        
        foreach($ci->im->getContracts()->result() as $contract)
        {
            $cutoff = date('Y-m-d',strtotime($contract->end_date . " - " . $contract->reminderTolerance . " month"));
        
            if(strtotime(date('Y-m-d')) > strtotime($cutoff))
            {
                if($ci->im->getExpireNotificationsByContractId($contract->id)->num_rows() == 0)
                {
                    $ci->im->insertNotification(array(
                        'dismissed' => 0,
                        'type' => NOTIFICATION_TYPE_CONTRACTEXPIRE,
                        'entity_id' => $contract->id,    
                    ));
                }
            }
        }
    }
    
    public function refreshObjectAdd()
    {
        $this->ci->load->model('Inventory_model', 'im');
        
        $filters = array();
        if($this->ci->input->post('filters') != null)
        {
            foreach($this->ci->input->post('filters') as $filter)
            {
                $filters[$filter[0]] = $filter[1];
            }
        }
        
        echo json_encode(
            array(
                'html' => $this->objectAddView($filters),
                'success' => true,
            )
        );
    }
}

?>
