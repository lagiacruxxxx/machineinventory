<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MyClient extends MyCore
{
    protected $clientId = null;
    protected $client = null;
    protected $clientData = null;
    
    public function __construct($clientId)
    {
        $this->ci = & get_instance();
        $this->ci->load->model('Inventory_model', 'im');
        $this->clientId = $clientId;
        $this->ClientData();
    }
    
    private function ClientData()
    {
        $data = array();
        $this->client = $this->ci->im->getClientById($this->clientId)->row();
        
        $data['client'] = $this->client;
        
        $this->clientData = $data;
    }
    
    public static function clientList()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $ci->load->helper('cookie');
        
        $sorting = MyUser::getSorting('client');
        $filters = MyUser::getFilters('client');
        
        if($sorting != null)
        {
            $data['sortfield'] = $sorting->fieldname;
            $data['sortdirection'] = $sorting->direction;
        }
        else
        {
            $data['sortfield'] = 'name';
            $data['sortdirection'] = 'asc';
        }    
        
        $clients = $ci->im->getClientsLimited($data['sortfield'], $data['sortdirection'], $filters)->result_array();
        
        if($data['sortdirection'] == 'asc')
            $data['sorticon'] = 'ui-icon-circle-triangle-s';
        else
            $data['sorticon'] = 'ui-icon-circle-triangle-n';
        
        $data['clients'] = $clients;
        
        return $ci->load->view('inventory/client_list', $data, true);
    }
    
    public static function clientMenu()
    {
        $ci = & get_instance();
        $data['filterActive'] = count(MyClient::getFilters('client')) > 0 ? true : false;
        return $ci->load->view('inventory/client_menu', $data, true);        
    }
    
    public static function addView()
    {
        $ci = & get_instance();
        $data = array();
        $data['modeForm'] = 'clientAddForm';
        
        $data['client'] = (object)array(
            'id' => null,
            'name' => '',
        );
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $ci->lang->line('client_title_new'),
                'html' => $ci->load->view('inventory/client_edit', $data, true),
                'clientId' => -2),
            'message' => 'Client found!',
        ));
    }
    
    public static function insert($data)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        unset($data['id']);
        if(MyClient::validateClient($data))
        {
            
            $newId = $ci->im->insertClient($data);
            if($newId != null)
            {
                $success = true;
                $message = '';
            }
            else
            {
                $success = false;
                $message = $ci->lang->line('error_insert');
            }
                
            echo json_encode(array(
                'success' => $success,
                'new_id' => $newId,
                'message' => $message,
            ));
        }
    }
    
    public function clone_()
    {
        $data = $this->clientData;
        $data['modeForm'] = 'clientAddForm';
    
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('client_title_new'),
                'html' => $this->ci->load->view('inventory/client_edit', $data, true),
                'clientId' => -2),
            'message' => 'Client found!',
        ));
    }
    
    public function view()
    {
        $data['overview'] = $this->overviewView();
        $data['edit'] = $this->editView();
        $data['client'] = $this->client;
        	
        $html = $this->ci->load->view('inventory/client_detail', $data, true);
         
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('client_tab') . ' - ' . $this->client->name,
                'html' => $html,
                'clientId' => $this->client->id),
            'message' => 'Client found!',
        ));
    }
    
    private function overviewView()
    {
        $data['client'] = $this->client;
        
        return $this->ci->load->view('inventory/client_overview', $data, true);
    }
    
    private function editView($mode = 'edit')
    {
        $data = $this->clientData;
        $data['modeForm'] = $mode == 'edit' ? 'clientEditForm' : 'clientAddForm';
        return $this->ci->load->view('inventory/client_edit', $data, true);
    }
    
    
    public function update($data)
    {
        if(MyClient::validateClient($data))
        {
            $id = $data['id'];
            unset($data['id']);
            
            $this->ci->im->updateClient($id, $data);
            $this->ClientData();
            
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('success_update'),
                'overview' => $this->overviewView(),
            ));
        }
    }
    
    public function delete()
    {
        $this->ci->im->deleteClient($this->clientId);
        $this->ci->im->resetObjectsByClientId($this->clientId);
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete')
        ));
    }
    
    public static function validateClient($data)
    {
        $ci = & get_instance();
        $ci->load->helper('form');
        
        $form_validation = new Besc_Form_validation();
        $form_validation->set_data($data);

        $form_validation->set_rules('name', $ci->lang->line('clientlist_name'), 'required|min_length[3]|max_length[255]');
        
        if ($form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => $form_validation->error_string('', '')));
        }
        else
            return true;
    }
    
 
    
    public static function filterView()
    {
        $ci = & get_instance();
        $data = array();
        
        $data['filters'] = MyClient::getFilters('client');
        
        return $ci->load->view('inventory/client_filter', $data, true);
    }
    
}

?>