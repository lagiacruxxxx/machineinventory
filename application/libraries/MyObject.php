<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MyObject extends MyCore
{
    protected $objectId = null;
    protected $object = null;
    protected $objectData = null;
    
    public function __construct($objectId)
    {
        $this->ci = & get_instance();
        $this->ci->load->model('Inventory_model', 'im');
        
        $object = $this->ci->im->getObjectById($objectId);
        if($object->num_rows() == 1)
        {
            $this->objectId = $objectId;
            $this->ObjectData();
        }
        else 
            die('Object not found!');
    }
    
    private function ObjectData()
    {
        $data = array();
        $this->object = $this->ci->im->getObjectById($this->objectId)->row();
        
        $data['object'] = $this->object;
        $data['equipmentgroup'] = $this->ci->im->getEquipmentGroupById($this->object->equipmentgroup_id)->row();
        $data['client'] = $this->ci->im->getClientById($this->object->client_id)->row();
        $data['contract'] = $this->ci->im->getContractById($this->object->contract_id)->row();
        $data['nextMaintenance'] = MyObject::calculateNextMaintenance($this->object);
        $data['supplier'] = $this->ci->im->getContactById($this->object->supplier_contact_id)->row();
        $data['contact1'] = $this->ci->im->getContactById($this->object->contact1)->row();
        $data['contact2'] = $this->ci->im->getContactById($this->object->contact2)->row();
        $data['status'] = $this->ci->im->getObjectStatusById($this->object->status_id)->row();
        $data['equipmentgroups'] = $this->ci->im->getEquipmentGroups();
        $data['costcenters'] = $this->ci->im->getCostCenters();
        $data['clients'] = $this->ci->im->getClients();
        $data['contracts'] = $this->ci->im->getContracts();
        $data['contacts'] = $this->ci->im->getContacts();
        $data['stati'] = $this->ci->im->getObjectStatus();
    
        $this->objectData = $data;
    }
    
    public static function objectList()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $ci->load->helper('cookie');
        
        $sorting = MyObject::getSorting('object');
        $filters = MyObject::getFilters('object');
        
        if($sorting != null)
        {
            $data['sortfield'] = $sorting->fieldname;
            $data['sortdirection'] = $sorting->direction;
        }
        else
        {
            $data['sortfield'] = 'object_id';
            $data['sortdirection'] = 'asc';            
        }
        
        if($data['sortfield'] != 'next')
            $objects = $ci->im->getObjectsLimited($data['sortfield'], $data['sortdirection'], $filters)->result_array();
        else
            $objects = $ci->im->getObjects($filters)->result_array();
        
        foreach($objects as $key => $object)
        {
            $objects[$key]['nextMaintenance'] = MyObject::calculateNextMaintenance((object)$object);
            $objects[$key]['nextM'] = $object['next_maintenance'];
            
            if(!MyCore::hasPrivilege(USER_PRIV_EDIT) && ($objects[$key]['status_id'] == OBJECTSTATUS_INACTIVE || $objects[$key]['status_id'] == OBJECTSTATUS_EX))
                unset($objects[$key]);
        }
        
        if($data['sortdirection'] == 'asc')
            $data['sorticon'] = 'ui-icon-circle-triangle-s';
        else
            $data['sorticon'] = 'ui-icon-circle-triangle-n';
        
        $data['objects'] = $objects;
        
        return $ci->load->view('inventory/object_list', $data, true);
    }
    
    public static function objectSortByNextAsc($a, $b)
    {
        if(strtotime($a['nextMaintenance']['next'] == $b['nextMaintenance']['next']))
            return 0;
        else 
            return strtotime($a['nextMaintenance']['next']) < strtotime($b['nextMaintenance']['next']) ? -1 : 1;
    }
    
    public static function objectSortByNextDesc($a, $b)
    {
        if(strtotime($a['nextMaintenance']['next'] == $b['nextMaintenance']['next']))
            return 0;
        else
            return strtotime($a['nextMaintenance']['next']) > strtotime($b['nextMaintenance']['next']) ? -1 : 1;
    }
    
    public static function objectMenu()
    {
        $ci = & get_instance();
        $data['filterActive'] = count(MyObject::getFilters('object')) > 0 ? true : false;
        return $ci->load->view('inventory/object_menu', $data, true);        
    }
    
    public static function calculateNextMaintenance($object)
    {
        $now = date('Y-m-d');
        $start = $object->last_maintenance == NULL || $object->last_maintenance == '1970-01-01' ? $object->installation_date : $object->last_maintenance;
        $start = new DateTime($start);
        $interval = new DateInterval('P' . $object->serviceinterval . 'M');
        $start->add($interval);
        $ret = array(
            'next' => $start->format('d.m.Y'),
            'clearDate' => $start->format('Y-m-d'),
        );
    
        if(strtotime($start->format('Y-m-d')) <= strtotime($now))
        {
            $ret['cc'] = 'ui-state-error';
        }
        elseif(strtotime($start->format('Y-m-d')) <= strtotime('+90 days'))
        {
            $ret['cc'] = 'ui-state-highlight';
        }
        else
        {
            $ret['cc'] = '';
        }
        
        if($object->status_id == OBJECTSTATUS_INACTIVE || $object->status_id == OBJECTSTATUS_EX)
            $ret['cc'] = 'ui-state-ex';
    
        return $ret;
    }
    
    public static function addView()
    {
        $ci = & get_instance();
        $data = array();
        $data['modeForm'] = 'objectAddForm';
        
        $object['id'] = null;
        $object['object_id'] = '';
        $object['inventory_number'] = '';
        $object['serial_number'] = '';
        $object['GLT_number'] = '';
        $object['manufacturer_service_number'] = '';
        $object['status_id'] = -1;
        $object['equipmentgroup_id'] = -1;
        $object['name'] = '';
        $object['manufacturer'] = '';
        $object['description'] = '';
        $object['purchase_date'] = null;
        $object['purchase_price'] = '';
        $object['installation_date'] = null;
        $object['retired_date'] = null;
        $object['client_id'] = -1;
        $object['location'] = '';
        $object['created_costcenter'] = -1;
        $object['booked_costcenter'] = -1;
        $object['contract_id'] = -1;
        $object['warranty_date'] = null;
        $object['serviceinterval'] = '';
        $object['supplier_contact_id'] = -1;
        $object['contact1'] = -1;
        $object['contact2'] = -1;
        $object['last_maintenance'] = null;
        $object['next_maintenance'] = null;
        
        $data['object'] = (object)$object;
        $data['equipmentgroups'] = $ci->im->getEquipmentGroups();
        $data['costcenters'] = $ci->im->getCostCenters();
        $data['clients'] = $ci->im->getClients();
        $data['contracts'] = $ci->im->getContracts();
        $data['contacts'] = $ci->im->getContacts();
        $data['stati'] = $ci->im->getObjectStatus();
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $ci->lang->line('object_title_new'),
                'html' => $ci->load->view('inventory/object_edit', $data, true),
                'objectId' => -2),
            'message' => 'Object found!',
        ));
    }
    
    public static function insert($data)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        unset($data['id']);
        $data['equipmentgroup_id'] = $data['equipmentgroup_id'] == -1 ? null : $data['equipmentgroup_id'];
        $data['created_costcenter'] = $data['created_costcenter'] == -1 ? null : $data['created_costcenter'];
        $data['client_id'] = $data['client_id'] == -1 ? null : $data['client_id'];
        $data['booked_costcenter'] = $data['booked_costcenter'] == -1 ? null : $data['booked_costcenter'];
        $data['contract_id'] = $data['contract_id'] == -1 ? null : $data['contract_id'];
        $data['supplier_contact_id'] = $data['supplier_contact_id'] == -1 ? null : $data['supplier_contact_id'];
        $data['supplier_contact_id'] = $data['supplier_contact_id'] == -1 ? null : $data['supplier_contact_id'];
        $data['contact1'] = $data['contact1'] == -1 ? null : $data['contact1'];
        $data['contact2'] = $data['contact2'] == -1 ? null : $data['contact2'];
        
        $data['purchase_date'] = $data['purchase_date'] != '' ? date('Y-m-d', strtotime($data['purchase_date'])) : null;
        $data['installation_date'] = $data['installation_date'] != '' ? date('Y-m-d', strtotime($data['installation_date'])) : null;
        $data['retired_date'] = $data['retired_date'] != '' ? date('Y-m-d', strtotime($data['retired_date'])) : null;
        $data['warranty_date'] = $data['warranty_date'] != '' ? date('Y-m-d', strtotime($data['warranty_date'])) : null;
        
        if(MyObject::validateObject($data))
        {
            $newId = $ci->im->insertObject($data);
            if($newId != null)
            {
                $next = MyObject::calculateNextMaintenance($ci->im->getObjectById($newId)->row());
                $data = array(
                    'object_id' => $newId,
                    'next_maintenance' =>  $next['clearDate'],
                );
                $ci->im->updateObjectId($newId, $data);
                $success = true;
                $message = '';
            }
            else
            {
                $success = false;
                $message = $ci->lang->line('error_insert');
            }
                
            echo json_encode(array(
                'success' => $success,
                'new_id' => $newId,
                'message' => $message,
            ));
        }
    }
    
    public function clone_()
    {
        $data = $this->objectData;
        $data['modeForm'] = 'objectAddForm';
    
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('object_title_new'),
                'html' => $this->ci->load->view('inventory/object_edit', $data, true),
                'objectId' => -2),
            'message' => 'Object found!',
        ));
    }
    
    public function view()
    {
        $data['overview'] = $this->overviewView();
        $data['edit'] = $this->editView();
        $data['history'] = $this->historyView();
        $data['files'] = $this->filesView();
        $data['object'] = $this->object;
        	
        $html = $this->ci->load->view('inventory/object_detail', $data, true);
         
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('object_title') . ' - ' . $this->object->object_id,
                'html' => $html,
                'objectId' => $this->object->id),
            'message' => 'Object found!',
        ));
    }
    
    private function overviewView()
    {
        return $this->ci->load->view('inventory/object_overview', $this->objectData, true);
    }
    
    private function editView($mode = 'edit')
    {
        $data = $this->objectData;
        $data['modeForm'] = $mode == 'edit' ? 'objectEditForm' : 'objectAddForm';
        return $this->ci->load->view('inventory/object_edit', $data, true);
    }
    
    private function historyView()
    {
        $data['history_data'] = $this->ci->im->getObjectHistoryByObjectID($this->objectId);
        return $this->ci->load->view('inventory/object_history', $data, true);
    }
    
    private function filesView()
    {
        $data['upload_data'] = $this->ci->im->getObjectFilesByObjectID($this->objectId);
        
        return $this->ci->load->view('inventory/object_files', $data, true);
    }
    
    public function update($data)
    {
        if(MyObject::validateObject($data))
        {
            $id = $data['id'];
            unset($data['id']);
            $data['purchase_date'] = $data['purchase_date'] != '' ? date('Y-m-d', strtotime($data['purchase_date'])) : null;
            $data['installation_date'] = $data['installation_date'] != '' ? date('Y-m-d', strtotime($data['installation_date'])) : null;
            $data['retired_date'] = $data['retired_date'] != '' ? date('Y-m-d', strtotime($data['retired_date'])) : null;
            $data['warranty_date'] = $data['warranty_date'] != '' ? date('Y-m-d', strtotime($data['warranty_date'])) : null;
            $data['next_maintenance'] = $data['next_maintenance'] != '' ? date('Y-m-d', strtotime($data['next_maintenance'])) : null;
            $data['last_maintenance'] = $data['last_maintenance'] != '' ? date('Y-m-d', strtotime($data['last_maintenance'])) : null;
            
            $data['equipmentgroup_id'] = $data['equipmentgroup_id'] == -1 ? null : $data['equipmentgroup_id'];
            $data['created_costcenter'] = $data['created_costcenter'] == -1 ? null : $data['created_costcenter'];
            $data['client_id'] = $data['client_id'] == -1 ? null : $data['client_id'];
            $data['booked_costcenter'] = $data['booked_costcenter'] == -1 ? null : $data['booked_costcenter'];
            $data['contract_id'] = $data['contract_id'] == -1 ? null : $data['contract_id'];
            $data['supplier_contact_id'] = $data['supplier_contact_id'] == -1 ? null : $data['supplier_contact_id'];
            $data['supplier_contact_id'] = $data['supplier_contact_id'] == -1 ? null : $data['supplier_contact_id'];
            $data['contact1'] = $data['contact1'] == -1 ? null : $data['contact1'];
            $data['contact2'] = $data['contact2'] == -1 ? null : $data['contact2'];
            
            $this->ci->im->updateObject($data, $id);
            $this->ObjectData();
            
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('success_update'),
                'overview' => $this->overviewView(),
            ));
        }
    }
    
    public function delete()
    {
        $this->ci->im->deleteObject($this->objectId);
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete')
        ));
    }
    
    public static function validateObject($data)
    {
        $ci = & get_instance();
        $ci->load->helper('form');
        $ci->load->library('form_validation');
        $ci->form_validation->set_data($data);

        $ci->form_validation->set_rules('serial_number', $ci->lang->line('objectlist_serialnumber'), 'is_unique[object.inventory_number]');
        $ci->form_validation->set_rules('serviceinterval', $ci->lang->line('objectlist_serviceinterval'), 'required|is_natural|greater_than[0]');
        
        if ($ci->form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => validation_errors()));
        }
        else
            return true;
    }
    
    public function insertHistory($data, $response = true)
    {
        if($this->validateHistory($data))
        {
            $data['createdby'] = $this->ci->user->id;
            $data['createddate'] = date('Y-m-d H:i:s');
            if($this->ci->im->insertObjectHistory($data))
            {
                $success = true;
                $message = $this->ci->lang->line('success_update');
                $history = $this->historyView();
            }
            else
            {
                $success = false;
                $message = $this->ci->lang->line('error_update');
                $history = '';
            }
            
            if($response)
            {
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message,
                    'history' => $history,
                ));
            }
        }
    }
    
    private function validateHistory($data)
    {
        $this->ci->load->helper('form');
        $this->ci->load->library('form_validation');
        $this->ci->form_validation->set_data($data);
        
        $this->ci->form_validation->set_rules('comment', $this->ci->lang->line('objectdetail_history_comment'), 'required|max_length[500]');
        
        if ($this->ci->form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => validation_errors()));
        }
        else
            return true;
    }
    
    public function deleteObjectHistoryItem($history_id)
    {
        $this->ci->im->DeleteObjectHistoryItem($history_id);
        
        $success = true;
        $message = $this->ci->lang->line('success_delete');
        $history = $this->historyView();
        
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete'),
            'history' => $this->historyView(),
        ));
    }
    
    public function insertFile($data, $files)
    {
        if($this->validateFile($data, $files))
        {
            $filename = $this->ci->input->post('filename');
            $desc = $this->ci->input->post('filedescription');
            $objectId = $this->ci->input->post('object_id');
            
            $rnd = rand_string(12);
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $serverFile = time() . "_" . $rnd . "." . $ext;
            
            $success = move_uploaded_file($files['data']['tmp_name'], getcwd() . "/items/uploads/objectfiles/$serverFile");
            
            if($success)
            {
                $data = array(
                    'object_id' => $objectId,
                    'filedescription' => $desc,
                    'filename' => $serverFile,
                    'createdby' => $this->ci->user->id,
                    'createddate' => date('Y-m-d H:i:s'),
                );
            
                $success = $this->ci->im->insertObjectFile($data);
                if($success)
                {
                    $msg = $this->ci->lang->line('success_update');
                    $filelist = $this->filesView();
                }
                else
                {
                    $msg = $this->ci->lang->line('error_update');
                    $filelist = '';
                }
                
                echo json_encode(array(
                    'success' => $success,
                    'message' => $msg,
                    'files' => $filelist,
                ));
            }
        }
    }
    
    private function validateFile($data, $files)
    {
        $this->ci->load->helper('form');
        $this->ci->load->library('form_validation');
        $this->ci->form_validation->set_data($data);
        
        $this->ci->form_validation->set_rules('filedescription', $this->ci->lang->line('objectdetail_files_textdesc'), 'required|max_length[255]|min_length[5]');
        
        if ($this->ci->form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => validation_errors()));
        }
        else
            return true;
    }
    
    public static function filterView()
    {
        $ci = & get_instance();
        $data = array();
        $ci->load->helper('cookie');
        
        $data['equipmentgroups'] = $ci->im->getEquipmentGroups();
        $data['costcenters'] = $ci->im->getCostCenters();
        $data['clients'] = $ci->im->getClients();
        $data['contracts'] = $ci->im->getContracts();
        $data['contacts'] = $ci->im->getContacts();
        $data['stati'] = $ci->im->getObjectStatus();
        $data['filters'] = MyObject::getFilters('object');
        
        return $ci->load->view('inventory/object_filter', $data, true);
    }
    
    
    public function service($comment, $echo = true)
    {
        $data['object_id'] = $this->objectId;
        $data['comment'] = $comment;
        $this->insertHistory($data, false);
        $data = array();
        $data['last_maintenance'] = date('Y-m-d H:i:s');
        $data['next_maintenance'] = MyObject::calculateNextMaintenance($this->object)['clearDate'];
        $this->ci->im->serviceObject($this->objectId, $data);
        
        if($echo)
        {
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('object_servicecomment'),
            ));
        }
    }
    
    public static function serviceView()
    {
        $ci = & get_instance();
        return $ci->load->view('inventory/object_service', array(), true);
    }
    
    public static function getServiceObjects($start, $end)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        
        $events = array();
        
        foreach($ci->im->getObjects(array())->result() as $object)
        {
            $next = strtotime($object->next_maintenance);
            $dummy = MyObject::calculateNextMaintenance($object);
            if( $next >= strtotime($start) && $next <= strtotime($end) )
            {
                $events[] = array(
                    'id' => $object->id,
                    'title' => $object->object_id . ' - ' . $object->name,
                    'start' => date('Y-m-d', $next),
                    'allDay' => true,
                    'editable' => false,
                    'className' => $dummy['cc'],
                );
            }
        }
        
        echo json_encode($events);
    }
    
    
    public function updateImage($files)
    {
        $filename = $this->ci->input->post('filename');
        $objectId = $this->ci->input->post('object_id');

        $rnd = rand_string(12);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $serverFile = time() . "_" . $rnd . "." . $ext;
        $serverFileThumb = 'thumb_' . $serverFile;

        $success = move_uploaded_file($files['data']['tmp_name'], getcwd() . "/items/uploads/objectimages/$serverFile");

        if($success)
        {
            $data = array(
                'image_fname' => $serverFile,
            );
            
            $this->generateImageThumbnail(getcwd() . "/items/uploads/objectimages/$serverFile", getcwd() . "/items/uploads/objectimages/$serverFileThumb", 200, 200);

            $this->ci->im->updateObject($data, $objectId);
            $this->ObjectData();
            $msg = $this->ci->lang->line('success_update');
            $filelist = $this->overviewView();
        }
        else
        {
            $msg = $this->ci->lang->line('error_update');
            $filelist = '';
        }

        echo json_encode(array(
            'success' => $success,
            'message' => $msg,
            'overview' => $filelist,
        ));
    }    
}

?>