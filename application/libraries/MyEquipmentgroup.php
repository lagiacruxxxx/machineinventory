<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MyEquipmentgroup extends MyCore
{
    protected $equipmentgroupId = null;
    protected $equipmentgroup = null;
    protected $equipmentgroupData = null;
    
    public function __construct($userId)
    {
        $this->ci = & get_instance();
        $this->ci->load->model('Inventory_model', 'im');
        $this->equipmentgroupId = $userId;
        $this->EquipmentgroupData();
    }
    
    private function EquipmentgroupData()
    {
        $data = array();
        $this->equipmentgroup = $this->ci->im->getEquipmentgroupById($this->equipmentgroupId)->row();
        
        $data['equipmentgroup'] = $this->equipmentgroup;
        
        $this->equipmentgroupData = $data;
    }
    
    public static function equipmentgroupList()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $ci->load->helper('cookie');
        
        $sorting = MyUser::getSorting('equipmentgroup');
        $filters = MyUser::getFilters('equipmentgroup');
        
        if($sorting != null)
        {
            $data['sortfield'] = $sorting->fieldname;
            $data['sortdirection'] = $sorting->direction;
        }
        else
        {
            $data['sortfield'] = 'name';
            $data['sortdirection'] = 'asc';
        }    
        
        $equipmentgroups = $ci->im->getEquipmentgroupLimited($data['sortfield'], $data['sortdirection'], $filters)->result_array();
        
        if($data['sortdirection'] == 'asc')
            $data['sorticon'] = 'ui-icon-circle-triangle-s';
        else
            $data['sorticon'] = 'ui-icon-circle-triangle-n';
        
        $data['equipmentgroups'] = $equipmentgroups;
        
        return $ci->load->view('inventory/equipmentgroup_list', $data, true);
    }
    
    public static function equipmentgroupMenu()
    {
        $ci = & get_instance();
        $data['filterActive'] = count(MyEquipmentgroup::getFilters('equipmentgroup')) > 0 ? true : false;
        return $ci->load->view('inventory/equipmentgroup_menu', $data, true);        
    }
    
    public static function addView()
    {
        $ci = & get_instance();
        $data = array();
        $data['modeForm'] = 'equipmentgroupAddForm';
        
        $data['equipmentgroup'] = (object)array(
            'id' => null,
            'name' => '',
        );
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $ci->lang->line('equipmentgroup_title_new'),
                'html' => $ci->load->view('inventory/equipmentgroup_edit', $data, true),
                'equipmentgroupId' => -2),
            'message' => 'Equipmentgroup found!',
        ));
    }
    
    public static function insert($data)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        unset($data['id']);
        if(MyEquipmentgroup::validateEquipmentgroup($data))
        {
            $newId = $ci->im->insertEquipmentgroup($data);
            if($newId != null)
            {
                $success = true;
                $message = '';
            }
            else
            {
                $success = false;
                $message = $ci->lang->line('error_insert');
            }
                
            echo json_encode(array(
                'success' => $success,
                'new_id' => $newId,
                'message' => $message,
            ));
        }
    }
    
    public function clone_()
    {
        $data = $this->equipmentgroupData;
        $data['modeForm'] = 'equipmentgroupAddForm';
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('user_title_new'),
                'html' => $this->ci->load->view('inventory/equipmentgroup_edit', $data, true),
                'equipmentgroupId' => -2),
            'message' => 'Equipmentgroup found!',
        ));
    }
    
    public function view()
    {
        $data['overview'] = $this->overviewView();
        $data['edit'] = $this->editView();
        $data['equipmentgroup'] = $this->equipmentgroup;
        	
        $html = $this->ci->load->view('inventory/equipmentgroup_detail', $data, true);
         
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('equipmentgroup_tab') . ' - ' . $this->equipmentgroup->name,
                'html' => $html,
                'equipmentgroupId' => $this->equipmentgroup->id),
            'message' => 'Equipmentgroup found!',
        ));
    }
    
    private function overviewView()
    {
        $data['equipmentgroup'] = $this->equipmentgroup;
        
        return $this->ci->load->view('inventory/equipmentgroup_overview', $data, true);
    }
    
    private function editView($mode = 'edit')
    {
        $data = $this->equipmentgroupData;
        $data['modeForm'] = $mode == 'edit' ? 'equipmentgroupEditForm' : 'equipmentgroupAddForm';
        return $this->ci->load->view('inventory/equipmentgroup_edit', $data, true);
    }
    
    
    public function update($data)
    {
        if(MyEquipmentgroup::validateEquipmentgroup($data))
        {
            $id = $data['id'];
            unset($data['id']);
            
            $this->ci->im->updateEquipmentgroup($id, $data);
            $this->EquipmentgroupData();
            
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('success_update'),
                'overview' => $this->overviewView(),
            ));
        }
    }
    
    public function delete()
    {
        $this->ci->im->deleteEquipmentgroup($this->equipmentgroupId);
        $this->ci->im->resetObjectsByEquipmentgroupId($this->equipmentgroupId);
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete')
        ));
    }
    
    public static function validateEquipmentgroup($data)
    {
        $ci = & get_instance();
        $ci->load->helper('form');
        
        $form_validation = new Besc_Form_validation();
        $form_validation->set_data($data);

        $form_validation->set_rules('name', $ci->lang->line('equipmentgrouplist_name'), 'required|min_length[3]|max_length[50]');
        
        if ($form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => $form_validation->error_string('', '')));
        }
        else
            return true;
    }
    
 
    
    public static function filterView()
    {
        $ci = & get_instance();
        $data = array();
        
        $data['filters'] = MyUser::getFilters('equipmentgroup');
        
        return $ci->load->view('inventory/equipmentgroup_filter', $data, true);
    }
    
}

?>