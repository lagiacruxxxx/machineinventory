<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyCore 
{
    protected $ci = null;

    public function __construct($objectId)
    {
        
    }
    
    
    public static function getFilters($entity)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $filters = array();
        
        foreach($ci->im->getFilters($entity, $ci->user->id)->result() as $filter)
        {
            $filters[$filter->fieldname] = $filter->value;
        }

        return $filters;
    }
    
    public static function getSorting($entity)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $filters = array();
        
        return $ci->im->getSorting($entity, $ci->user->id)->row();
    }
    
    public static function updateFilters()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        
        $filters = array();
        
        if($ci->input->post('filters') != null)
        {
            foreach($ci->input->post('filters') as $filter)
            {
                $filters[] = array(
                    'tablename' => $ci->input->post('tablename'),
                    'user_id' => $ci->user->id,
                    'fieldname' => $filter[0],
                    'value' => $filter[1],    
                );
            }
        }
        
        $ci->im->deleteFilters($ci->input->post('tablename'), $ci->user->id);
        if($filters != array())
            $ci->im->insertFilters($filters);
        
        echo json_encode(array('success' => true));
    }
    
    public static function updateSorting()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $data = array(
            'user_id' => $ci->user->id,
            'tablename' => $ci->input->post('tablename'),
            'fieldname' => $ci->input->post('sortfield'),
            'direction' => $ci->input->post('sortdirection'),
        );
        $ci->im->deleteSorting($ci->input->post('tablename'), $ci->user->id);
        $ci->im->insertSorting($data);
        
        echo json_encode(array('success' => true));
    }
    
    
    public function generateImageThumbnail($source_image_path, $thumbnail_image_path, $max_width, $max_height)
    {
        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
        switch ($source_image_type) 
        {
            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($source_image_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gd_image = imagecreatefromjpeg($source_image_path);
                break;
            case IMAGETYPE_PNG:
                $source_gd_image = imagecreatefrompng($source_image_path);
                break;
        }
        if ($source_gd_image === false) {
            return false;
        }
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = $max_width / $max_height;
        if ($source_image_width <= $max_width && $source_image_height <= $max_height) {
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
            $thumbnail_image_width = (int) ($max_height * $source_aspect_ratio);
            $thumbnail_image_height = $max_height;
        } else {
            $thumbnail_image_width = $max_width;
            $thumbnail_image_height = (int) ($max_width / $source_aspect_ratio);
        }
        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
        imagedestroy($source_gd_image);
        imagedestroy($thumbnail_gd_image);
        
        return true;
    }
    
    
    public static function hasPrivilege($required)
    {
        $ci = & get_instance();
        return $ci->user->role <= $required;
    }
    
    public static function getNotifications()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        
        $data['notification_count'] = 0;
        $data['notifications'] = array();
        foreach($ci->im->getNotifications()->result_array() as $not)
        {
            $contract = $ci->im->getContractById($not['entity_id'])->row();
            $not['text'] = $contract->company . ' ' . $contract->maintenance_number;
            $data['notification_count']++;
            $data['notifications'][] = $not;
            
        }
        
        return array(
            'count' => $data['notification_count'],
            'list' => $ci->load->view('inventory/notificationlist', $data, true),
        );
    }
    
    public static function dismissNotification($id)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        
        $ci->im->dismissNotification($id);
        
        echo json_encode(array('success' => true));
    }
}

require_once SYSDIR . '/libraries/Form_validation.php';

class Besc_Form_validation extends CI_Form_validation
{
    function __construct()
    {
        parent::__construct();
    }

    public function is_unique($str, $field)
    {

        if (substr_count($field, '.') == 3) {
            list($table, $field, $id_field, $id_val) = explode('.', $field);
            $query = $this->CI->db->limit(1)->where($field, $str)->where($id_field . ' != ', $id_val)->get($table);
        } else {
            list($table, $field) = explode('.', $field);
            $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
        }

        return $query->num_rows() === 0;
    }

    public function fix_is_unique_rule($pk, $id, $rules)
    {
        if($id != 'null')
        {
            $new_rule = $rules;
            $rules = explode('|', $rules);
            foreach($rules as $rule)
            {
                if(preg_match('/is_unique/', $rule))
                {
                    $replace = str_replace(']', ".$pk.$id]", $rule);
                    $new_rule = str_replace($rule, $replace, $new_rule);
                }
            }
        }
        else
            $new_rule = $rules;

        return $new_rule;
    }
    
    
}

?>