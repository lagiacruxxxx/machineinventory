<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MyUser extends MyCore
{
    protected $userId = null;
    protected $user = null;
    protected $userData = null;
    
    public function __construct($userId)
    {
        $this->ci = & get_instance();
        $this->ci->load->model('Inventory_model', 'im');
        $this->userId = $userId;
        $this->UserData();
    }
    
    private function UserData()
    {
        $data = array();
        $this->user = $this->ci->im->getUserById($this->userId)->row();
        
        $data['user'] = $this->user;
        $data['userRoles'] = array(
            USER_PRIV_ADMIN => $this->ci->config->item('user_role_names')[USER_PRIV_ADMIN],
            USER_PRIV_EDIT => $this->ci->config->item('user_role_names')[USER_PRIV_EDIT],
            USER_PRIV_VIEW => $this->ci->config->item('user_role_names')[USER_PRIV_VIEW],
        );
        
        $this->userData = $data;
    }
    
    public static function userList()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $ci->load->helper('cookie');
        
        $sorting = MyUser::getSorting('user');
        $filters = MyUser::getFilters('user');
        
        if($sorting != null)
        {
            $data['sortfield'] = $sorting->fieldname;
            $data['sortdirection'] = $sorting->direction;
        }
        else
        {
            $data['sortfield'] = 'username';
            $data['sortdirection'] = 'asc';
        }    
        
        $users = $ci->im->getUsersLimited($data['sortfield'], $data['sortdirection'], $filters)->result_array();
        
        if($data['sortdirection'] == 'asc')
            $data['sorticon'] = 'ui-icon-circle-triangle-s';
        else
            $data['sorticon'] = 'ui-icon-circle-triangle-n';
        
        foreach($users as $key => $value)
        {
            $users[$key]['role_name'] = $ci->config->item('user_role_names')[$value['role']];
        }
        
        $data['users'] = $users;
        
        return $ci->load->view('inventory/user_list', $data, true);
    }
    
    public static function userMenu()
    {
        $ci = & get_instance();
        $data['filterActive'] = count(MyObject::getFilters('user')) > 0 ? true : false;
        return $ci->load->view('inventory/user_menu', $data, true);        
    }
    
    public static function addView()
    {
        $ci = & get_instance();
        $data = array();
        $data['modeForm'] = 'userAddForm';
        
        $data['user'] = (object)array(
            'id' => null,
            'username' => '',
            'firstname' => '',
            'lastname' => '',
            'email' => '',
            'role' => -1,    
        );
        
        $data['userRoles'] = array(
            USER_PRIV_ADMIN => $ci->config->item('user_role_names')[USER_PRIV_ADMIN],
            USER_PRIV_EDIT => $ci->config->item('user_role_names')[USER_PRIV_EDIT],
            USER_PRIV_VIEW => $ci->config->item('user_role_names')[USER_PRIV_VIEW],
        );
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $ci->lang->line('user_title_new'),
                'html' => $ci->load->view('inventory/user_edit', $data, true),
                'userId' => -2),
            'message' => 'User found!',
        ));
    }
    
    public static function insert($data)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        unset($data['id']);
        if(MyUser::validateUser($data, true))
        {
            require_once(APPPATH.'libraries/PasswordHash.php');
            
            $hasher = new PasswordHash(8, false);
            $data['pword'] = $hasher->HashPassword($data['password_change']);
            unset($data['password_change']);
            unset($data['password_confirm']);
            
            $newId = $ci->im->insertUser($data);
            if($newId != null)
            {
                $success = true;
                $message = '';
            }
            else
            {
                $success = false;
                $message = $ci->lang->line('error_insert');
            }
                
            echo json_encode(array(
                'success' => $success,
                'new_id' => $newId,
                'message' => $message,
            ));
        }
    }
    
    public function clone_()
    {
        $data = $this->userData;
        $data['modeForm'] = 'userAddForm';
        $data['userRole'] = $this->ci->config->item('user_role_names')[$this->user->role];
    
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('user_title_new'),
                'html' => $this->ci->load->view('inventory/user_edit', $data, true),
                'userId' => -2),
            'message' => 'Object found!',
        ));
    }
    
    public function view()
    {
        $data['overview'] = $this->overviewView();
        $data['edit'] = $this->editView();
        $data['user'] = $this->user;
        	
        $html = $this->ci->load->view('inventory/user_detail', $data, true);
         
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('user_tab') . ' - ' . $this->user->username,
                'html' => $html,
                'userId' => $this->user->id),
            'message' => 'User found!',
        ));
    }
    
    private function overviewView()
    {
        $data['user'] = $this->user;
        $data['userRole'] = $this->ci->config->item('user_role_names')[$this->user->role];
        
        return $this->ci->load->view('inventory/user_overview', $data, true);
    }
    
    private function editView($mode = 'edit')
    {
        $data = $this->userData;
        $data['modeForm'] = $mode == 'edit' ? 'userEditForm' : 'userAddForm';
        return $this->ci->load->view('inventory/user_edit', $data, true);
    }
    
    
    public function update($data)
    {
        if(MyUser::validateUser($data, false))
        {
            $id = $data['id'];
            unset($data['id']);
            
            if(isset($data['password_change']) && $data['password_change'] != '')
            {
                require_once(APPPATH.'libraries/PasswordHash.php');
                
                $hasher = new PasswordHash(8, false);
                $data['pword'] = $hasher->HashPassword($data['password_change']);
            }
            unset($data['password_confirm']);
            unset($data['password_change']);
            $this->ci->im->updateUser($id, $data);
            $this->UserData();
            
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('success_update'),
                'overview' => $this->overviewView(),
            ));
        }
    }
    
    public function delete()
    {
        $this->ci->im->deleteUser($this->userId);
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete')
        ));
    }
    
    public static function validateUser($data, $pw_required)
    {
        $ci = & get_instance();
        $ci->load->helper('form');
        
        $form_validation = new Besc_Form_validation();
        $form_validation->set_data($data);

        if(isset($data['id']))
            $form_validation->set_rules('username', $ci->lang->line('userlist_username'), $form_validation->fix_is_unique_rule('id', $data['id'], 'is_unique[users.username]|required'));
        else
            $form_validation->set_rules('username', $ci->lang->line('userlist_username'), 'is_unique[users.username]|required');
        
        $form_validation->set_rules('email', $ci->lang->line('userlist_email'), 'required|valid_email');
        
        if($pw_required || (isset($data['password_change']) && $data['password_change'] != '' && isset($data['password_confirm']) && $data['password_confirm'] != ''))
        {
            $form_validation->set_rules('password_change', $ci->lang->line('userlist_password'), 'required|min_length[5]|max_length[30]');
            $form_validation->set_rules('password_confirm', $ci->lang->line('userlist_confirm_pw'), 'required|min_length[5]|max_length[30]|matches[password_change]');
        }
        $form_validation->set_rules('role', $ci->lang->line('userlist_role'), 'required');
        
        
        if ($form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => $form_validation->error_string('', '')));
        }
        else
            return true;
    }
    
 
    
    public static function filterView()
    {
        $ci = & get_instance();
        $data = array();
        
        $data['userRoles'] = array(
            USER_PRIV_ADMIN => $ci->config->item('user_role_names')[USER_PRIV_ADMIN],
            USER_PRIV_EDIT => $ci->config->item('user_role_names')[USER_PRIV_EDIT],
            USER_PRIV_VIEW => $ci->config->item('user_role_names')[USER_PRIV_VIEW],
        );
        $data['filters'] = MyUser::getFilters('user');
        
        return $ci->load->view('inventory/user_filter', $data, true);
    }
    
}

?>