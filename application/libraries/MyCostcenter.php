<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MyCostcenter extends MyCore
{
    protected $costcenterId = null;
    protected $costcenter = null;
    protected $costcenterData = null;
    
    public function __construct($costcenterId)
    {
        $this->ci = & get_instance();
        $this->ci->load->model('Inventory_model', 'im');
        $this->costcenterId = $costcenterId;
        $this->CostcenterData();
    }
    
    private function CostcenterData()
    {
        $data = array();
        $this->costcenter = $this->ci->im->getCostcenterById($this->costcenterId)->row();
        
        $data['costcenter'] = $this->costcenter;

        $this->costcenterData = $data;
    }
    
    public static function costcenterList()
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        $ci->load->helper('cookie');
        
        $sorting = MyCostcenter::getSorting('costcenter');
        $filters = MyCostcenter::getFilters('costcenter');
        
        if($sorting != null)
        {
            $data['sortfield'] = $sorting->fieldname;
            $data['sortdirection'] = $sorting->direction;
        }
        else
        {
            $data['sortfield'] = 'name';
            $data['sortdirection'] = 'asc';
        }    
        
        $costcenters = $ci->im->getCostcenterLimited($data['sortfield'], $data['sortdirection'], $filters)->result_array();
        
        if($data['sortdirection'] == 'asc')
            $data['sorticon'] = 'ui-icon-circle-triangle-s';
        else
            $data['sorticon'] = 'ui-icon-circle-triangle-n';
        

        $data['costcenters'] = $costcenters;
        
        return $ci->load->view('inventory/costcenter_list', $data, true);
    }
    
    public static function costcenterMenu()
    {
        $ci = & get_instance();
        $data['filterActive'] = count(MyObject::getFilters('costcenter')) > 0 ? true : false;
        return $ci->load->view('inventory/costcenter_menu', $data, true);        
    }
    
    public static function addView()
    {
        $ci = & get_instance();
        $data = array();
        $data['modeForm'] = 'costcenterAddForm';
        
        $data['costcenter'] = (object)array(
            'id' => null,
            'name' => '',
        );
        
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $ci->lang->line('user_title_new'),
                'html' => $ci->load->view('inventory/costcenter_edit', $data, true),
                'costcenterId' => -2),
            'message' => 'Costcenter found!',
        ));
    }
    
    public static function insert($data)
    {
        $ci = & get_instance();
        $ci->load->model('Inventory_model', 'im');
        unset($data['id']);
        if(MyCostcenter::validateCostcenter($data))
        {
            $newId = $ci->im->insertCostcenter($data);
            if($newId != null)
            {
                $success = true;
                $message = '';
            }
            else
            {
                $success = false;
                $message = $ci->lang->line('error_insert');
            }
                
            echo json_encode(array(
                'success' => $success,
                'new_id' => $newId,
                'message' => $message,
            ));
        }
    }
    
    public function clone_()
    {
        $data = $this->costcenterData;
        $data['modeForm'] = 'costcenterAddForm';
    
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('costcenter_title_new'),
                'html' => $this->ci->load->view('inventory/costcenter_edit', $data, true),
                'userId' => -2),
            'message' => 'Object found!',
        ));
    }
    
    public function view()
    {
        $data['overview'] = $this->overviewView();
        $data['edit'] = $this->editView();
        $data['costcenter'] = $this->costcenter;
        	
        $html = $this->ci->load->view('inventory/costcenter_detail', $data, true);
         
        echo json_encode(array(
            'success' => true,
            'data' => array(
                'title' => $this->ci->lang->line('costcenterlist_name') . ' - ' . $this->costcenter->name,
                'html' => $html,
                'costcenterId' => $this->costcenter->id),
            'message' => 'Costcenter found!',
        ));
    }
    
    private function overviewView()
    {
        $data['costcenter'] = $this->costcenter;
        
        return $this->ci->load->view('inventory/costcenter_overview', $data, true);
    }
    
    private function editView($mode = 'edit')
    {
        $data = $this->costcenterData;
        $data['modeForm'] = $mode == 'edit' ? 'costcenterEditForm' : 'costcenterAddForm';
        return $this->ci->load->view('inventory/costcenter_edit', $data, true);
    }
    
    
    public function update($data)
    {
        if(MyCostcenter::validateCostcenter($data))
        {
            $id = $data['id'];
            unset($data['id']);
            
            $this->ci->im->updateCostcenter($id, $data);
            $this->CostcenterData();
            
            echo json_encode(array(
                'success' => true,
                'message' => $this->ci->lang->line('success_update'),
                'overview' => $this->overviewView(),
            ));
        }
    }
    
    public function delete()
    {
        $this->ci->im->deleteCostcenter($this->costcenterId);
        echo json_encode(array(
            'success' => true,
            'message' => $this->ci->lang->line('success_delete')
        ));
    }
    
    public static function validateCostcenter($data)
    {
        $ci = & get_instance();
        $ci->load->helper('form');
        
        $form_validation = new Besc_Form_validation();
        $form_validation->set_data($data);

        $form_validation->set_rules('name', $ci->lang->line('costcenter_name'), 'required|max_length[50]');
        
        if ($form_validation->run() == FALSE)
        {
            echo json_encode(array(
                'success' => false,
                'message' => $form_validation->error_string('', '')));
        }
        else
            return true;
    }
    
 
    
    public static function filterView()
    {
        $ci = & get_instance();
        $data = array();
        
        $data['filters'] = MyUser::getFilters('costcenter');
        
        return $ci->load->view('inventory/costcenter_filter', $data, true);
    }
    
}

?>