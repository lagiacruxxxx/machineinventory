<?php

class Inventory_model extends CI_Model
{

    function getFilters($table, $user_id)
    {
        $this->db->where('tablename', $table);
        $this->db->where('user_id', $user_id);
        return $this->db->get('filter');
    }
    
    function getSorting($table, $user_id)
    {
        $this->db->where('tablename', $table);
        $this->db->where('user_id', $user_id);
        return $this->db->get('sorting');
    }
    
    function deleteFilters($table, $user_id)
    {
        $this->db->where('tablename', $table);
        $this->db->where('user_id', $user_id);
        $this->db->delete('filter');
    }
    
    function insertFilters($data)
    {
        $this->db->insert_batch('filter', $data);
    }
    
    function deleteSorting($table, $user_id)
    {
        $this->db->where('tablename', $table);
        $this->db->where('user_id', $user_id);
        $this->db->delete('sorting');
    }
    
    function insertSorting($data)
    {
        $this->db->insert('sorting', $data);
    }
    
    
    
    /**************************
     * OBJECT
     ****************************/    

    function getObjects($filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        
        return $this->db->get('object');
    }
    
    function getObjectsLimited($sortfield, $sortdirection, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->order_by($sortfield, $sortdirection);
        
        return $this->db->get('object');
    }
    
    function getObjectById($objectId)
    {
        $this->db->where('id', $objectId);
        return $this->db->get('object');
    }
    
    function getObjectStatusById($statusId)
    {
        $this->db->where('id', $statusId);
        return $this->db->get('objectstatus');
    }
    
    function getObjectStatus()
    {
        return $this->db->get('objectstatus');
    }    
    
    function updateObject($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('object', $data);
    }
    
    function deleteObject($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('object');
    }
    
    function insertObject($data)
    {
        if($this->db->insert('object', $data))
            return $this->db->insert_id();
        else
            return null;
    }
    
    function updateObjectId($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('object', $data);
    }
    
    
    function getObjectHistoryByObjectID($objectId)
    {
        $this->db->select('objecthistory.*, users.username');
        $this->db->where('object_id', $objectId);
        $this->db->order_by('createddate', 'asc');
        $this->db->from('objecthistory');
        $this->db->join('users', 'users.id = objecthistory.createdby');
        return $this->db->get();
    }
    
    function insertObjectHistory($data)
    {
        return $this->db->insert('objecthistory', $data);
    }
    
    function DeleteObjectHistoryItem($history_id)
    {
        $this->db->where('id', $history_id);
        $this->db->delete('objecthistory');
    }
    
    
    function getObjectFilesByObjectID($objectId)
    {
        $this->db->select('objectfiles.*, users.username');
        $this->db->where('object_id', $objectId);
        $this->db->order_by('createddate', 'asc');
        $this->db->from('objectfiles');
        $this->db->join('users', 'users.id = objectfiles.createdby');
        return $this->db->get();
    }
    
    function serviceObject($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('object', $data);
    }
    
    function insertObjectFile($data)
    {
        return $this->db->insert('objectfiles', $data);
    }
    
    function getObjectsByContractId($contractId)
    {
        $this->db->where('contract_id', $contractId);
        return $this->db->get('object');
    }
    
    

    /**************************
     * USER
     ****************************/
    
    function getUserById($userId)
    {
        $this->db->where('id', $userId);
        return $this->db->get('users');
    }
    
    function getUsersLimited($sortfield, $sortdirection, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->order_by($sortfield, $sortdirection);
    
        return $this->db->get('users');
    }
    
    function updateUser($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }
    
    function insertUser($data)
    {
        if($this->db->insert('users', $data))
            return $this->db->insert_id();
        else
            return null;
    }
    
    function deleteUser($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('users');
    }
    
    /**************************
     * CLIENTS
     ****************************/
    
    function getClients()
    {
        return $this->db->get('client');
    }
    
    
    function getClientById($clientId)
    {
        $this->db->where('id', $clientId);
        return $this->db->get('client');
    }
    
    function getClientsLimited($sortfield, $sortdirection, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->order_by($sortfield, $sortdirection);
    
        return $this->db->get('client');
    }    
    
    function updateClient($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('client', $data);
    }
    
    function insertClient($data)
    {
        if($this->db->insert('client', $data))
            return $this->db->insert_id();
        else
            return null;
    }
    
    function deleteClient($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('client');
    }
        
    function resetObjectsByClientId($id)
    {
        $this->db->where('client_id', $id);
        $this->db->set('client_id', null);
        $this->db->update('object');
    }
    
    /**************************
     * EQUIPMENTGROUPS
     ****************************/
    
    function getEquipmentGroups()
    {
        return $this->db->get('equipmentgroup');
    }
    
    function getEquipmentgroupById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('equipmentgroup');
    }
    
    function getEquipmentgroupLimited($sortfield, $sortdirection, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->order_by($sortfield, $sortdirection);
    
        return $this->db->get('equipmentgroup');
    }
    
    function updateEquipmentgroup($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('equipmentgroup', $data);
    }
    
    function insertEquipmentgroup($data)
    {
        if($this->db->insert('equipmentgroup', $data))
            return $this->db->insert_id();
        else
            return null;
    }
    
    function deleteEquipmentgroup($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('equipmentgroup');
    }
    
    function resetObjectsByEquipmentgroupId($id)
    {
        $this->db->where('equipmentgroup_id', $id);
        $this->db->set('equipmentgroup_id', null);
        $this->db->update('object');
    }
    
    
    /**************************
     * COSTCENTER
     ****************************/
    function getCostCenters()
    {
        return $this->db->get('costcenter');
    }
    
    function getCostcenterById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('costcenter');
    }
    
    function getCostcenterLimited($sortfield, $sortdirection, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->order_by($sortfield, $sortdirection);
    
        return $this->db->get('costcenter');
    }
    
    function updateCostcenter($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('costcenter', $data);
    }
    
    function insertCostcenter($data)
    {
        if($this->db->insert('costcenter', $data))
            return $this->db->insert_id();
        else
            return null;
    }
    
    function deleteCostcenter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('costcenter');
    }
    
   /* function resetObjectsByCostcenterId($id)
    {
        $this->db->where('costcenter_id', $id);
        $this->db->set('costcenter_id', null);
        $this->db->update('object');
    }*/
    

    
    /**************************
     * CONTACTS
     ****************************/
    function getContacts()
    {
        return $this->db->get('contact');
    }
    
    function getContactById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('contact');
    }
    
    function getContactsLimited($sortfield, $sortdirection, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->order_by($sortfield, $sortdirection);
    
        return $this->db->get('contact');
    }
    
    function updateContact($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('contact', $data);
    }
    
    function insertContact($data)
    {
        if($this->db->insert('contact', $data))
            return $this->db->insert_id();
        else
            return null;
    }
    
    function deleteContact($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('contact');
    }
    
    
    
    
    
    
    /**************************
     * CONTRACTS
     ****************************/
    
    function getContractById($contractId)
    {
        $this->db->where('id', $contractId);
        return $this->db->get('contract');
    }
    
    
    function getContracts()
    {
        return $this->db->get('contract');
    }
    
    function getContractsLimited($sortfield, $sortdirection, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->order_by($sortfield, $sortdirection);
        
        return $this->db->get('contract');
    }
    
    function updateContract($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('contract', $data);
    }
    
    function insertContract($data)
    {
        if($this->db->insert('contract', $data))
            return $this->db->insert_id();
        else
            return null;
    }
    
    function deleteContract($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('contract');
    }
    
    function insertContractFile($data)
    {
        return $this->db->insert('contractfiles', $data);
    }
    
    function getContractFilesByContractID($contractId)
    {
        $this->db->select('contractfiles.*, users.username');
        $this->db->where('contract_id', $contractId);
        $this->db->order_by('createddate', 'asc');
        $this->db->from('contractfiles');
        $this->db->join('users', 'users.id = contractfiles.createdby');
        return $this->db->get();
    }
    
    function serviceContract($contractId, $new)
    {
        $this->db->set('end_date', $new);
        $this->db->where('id', $contractId);
        $this->db->update('contract');
    }
    
    
    function getExpireNotificationsByContractId($contractId)
    {
        $this->db->where('entity_id', $contractId);
        $this->db->where('type', NOTIFICATION_TYPE_CONTRACTEXPIRE);
        return $this->db->get('notification');
    }
    
    
    function getObjectsToAdd($contractId, $filters)
    {
        foreach($filters as $key => $value)
        {
            $this->db->like($key, $value);
        }
        $this->db->where('contract_id !=', $contractId);
        $this->db->or_where('contract_id', NULL);
        return $this->db->get('object');
    }
    
    function setObjectContractId($batch, $contractId)
    {
        $this->db->set('contract_id', $contractId);
        $this->db->where_in('object_id', $batch);
        $this->db->update('object');
    }
    
    /**************************
     * NOTIFICATIONS
     ****************************/
    
    function insertNotification($data)
    {
        $this->db->insert('notification', $data);
    }
    
    function deleteNotifications($entityId, $type)
    {
        $this->db->where('entity_id', $entityId);
        $this->db->where('type', $type);
        $this->db->delete('notification');
    }
    
    function getNotifications()
    {
        $this->db->where('dismissed', 0);
        return $this->db->get('notification');
    }
    
    function dismissNotification($id)
    {
        $this->db->where('id', $id);
        $this->db->update('notification', array('dismissed' => 1));
    }
    

}

?>