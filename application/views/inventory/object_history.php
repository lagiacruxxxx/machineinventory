

    <?php foreach($history_data->result() as $historyItem):?>
        <div class="history_item">
            <div class="history_item_header">
                <div class="history_item_user"><?= $historyItem->username?></div>
                <div class="history_item_date"><?= $historyItem->createddate?></div>
                <div class="history_item_delete" item_id="<?= $historyItem->id?>"><?= $this->lang->line('objectaction_delete')?></div>
            </div>
            <div class="history_item_content"><?= nl2br(htmlspecialchars($historyItem->comment))?></div>
        </div>
    <?php endforeach;?>    