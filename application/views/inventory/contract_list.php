
<div id="contracttable">
    <table class="ui-corner-all">
        <thead>
            <tr class="ui-state-default">
                <th class="listSortable" sortField="maintenance_number"><?= $this->lang->line('contractlist_maintenancenumber')?><?php if($sortfield == 'maintenance_number'):?><div class="listSortableIcon ui-icon <?= $sortdirection?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="service_agreement_number"><?= $this->lang->line('contractlist_serviceagreementnumber')?><?php if($sortfield == '"service_agreement_number"'):?><div class="listSortableIcon ui-icon <?= $sortdirection?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="company"><?= $this->lang->line('contractlist_company')?><?php if($sortfield == 'company'):?><div class="listSortableIcon ui-icon <?= $sortdirection?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="end_date"><?= $this->lang->line('contractlist_enddate')?><?php if($sortfield == 'end_date'):?><div class="listSortableIcon ui-icon <?= $sortdirection?>"></div><?php endif;?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($contracts as $contract):?>
                <tr class="contractrow" contract_id=<?= $contract['id']?>>
                    <td><?= $contract['maintenance_number']?></td>
                    <td><?= $contract['service_agreement_number']?></td>
                    <td><?= $contract['company']?></td>
                    <td><?= $contract['end_date']?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>