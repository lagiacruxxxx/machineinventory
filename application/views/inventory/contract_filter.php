
    <div id="contract_filter" class="filter">
        <form id="contractFilterForm">
            <table>
                <tbody>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('contractlist_maintenancenumber')?></td>
                        <td class="dataValue"><input type="text" name="maintenance_number" value="<?php if(isset($filters['maintenance_number'])):?><?= $filters['maintenance_number']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('contractlist_company')?></td>
                        <td class="dataValue"><input type="text" name="company" value="<?php if(isset($filters['company'])):?><?= $filters['company']?><?php endif;?>" /></td>
                    </tr>
                </tbody>
            </table>
            <br />
            <input type="submit" class="contractFilterSubmit filterButton" value="<?= $this->lang->line('filter')?>" />
            <input type="reset" class="contractFilterSubmit filterButton" value="<?= $this->lang->line('filter_reset')?>" />
            
        </form>
    </div>
    
    