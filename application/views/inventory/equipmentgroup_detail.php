
        <div class="equipmentgroupActions dialogActionbar">
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="equipmentgroupDelete dialogAction"><?= $this->lang->line('objectaction_delete')?></div>
                <div class="equipmentgroupClone dialogAction"><?= $this->lang->line('objectaction_clone')?></div>
            <?php endif;?>
        </div>

        <div class="equipmentgroupDetailTabs">
            <ul>
                <li class="equipmentgroupDetailTab" ><a href="#equipmentgroupDetail_tab_overview_<?= $equipmentgroup->id?>"><?= $this->lang->line('userdetail_tab_overview')?></a>
                <li class="equipmentgroupDetailTab" ><a href="#equipmentgroupDetail_tab_edit_<?= $equipmentgroup->id?>"><?= $this->lang->line('userdetail_tab_edit')?></a>
            </ul>
            
            <div id="equipmentgroupDetail_tab_overview_<?= $equipmentgroup->id?>" class="equipmentgroupDetail_tab_overview">
                <?= $overview?>
            </div>
            
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div id="equipmentgroupDetail_tab_edit_<?= $equipmentgroup->id?>"  class="equipmentgroupDetail_tab_edit">
                    <?= $edit?>
                </div>
            <?php endif;?>
            
        </div>
        