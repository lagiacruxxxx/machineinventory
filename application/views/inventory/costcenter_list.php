
<div id="costcentertable">
    <table class="ui-corner-all">
        <thead>
            <tr class="ui-state-default">
                <th class="listSortable" sortField="name"><?= $this->lang->line('costcenterlist_name')?><?php if($sortfield == 'name'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?> "></div><?php endif;?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($costcenters as $costcenter):?>
                <tr class="costcenterrow" costcenter_id=<?= $costcenter['id']?>>
                    <td><?= $costcenter['name']?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>