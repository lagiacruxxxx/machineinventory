
        <div class="contactActions dialogActionbar">
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="contactDelete dialogAction"><?= $this->lang->line('objectaction_delete')?></div>
                <div class="contactClone dialogAction"><?= $this->lang->line('objectaction_clone')?></div>
            <?php endif;?>
        </div>

        <div class="contactDetailTabs">
            <ul>
                <li class="contactDetailTab" ><a href="#contactDetail_tab_overview_<?= $contact->id?>"><?= $this->lang->line('contactdetail_tab_overview')?></a>
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?><li class="contactDetailTab" ><a href="#contactDetail_tab_edit_<?= $contact->id?>"><?= $this->lang->line('contactdetail_tab_edit')?></a><?php endif;?>
            </ul>
            
            <div id="contactDetail_tab_overview_<?= $contact->id?>" class="contactDetail_tab_overview">
                <?= $overview?>
            </div>
            
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
            <div id="contactDetail_tab_edit_<?= $contact->id?>"  class="contactDetail_tab_edit">
                <?= $edit?>
            </div>
            <?php endif;?>
            
        </div>
        