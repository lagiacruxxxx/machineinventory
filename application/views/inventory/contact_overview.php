        
        
    <div class="dialogColumnContainer">
        <div class="dialogColumn withMargin">
            <table>
                <tbody>
                    <tr><td class="dataHeader"><?= $this->lang->line('contactlist_name')?></td><td class="dataValue"><?= htmlspecialchars($contact->name)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contactlist_address')?></td><td class="dataValue"><?= htmlspecialchars($contact->address)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contactlist_email')?></td><td class="dataValue"><?= htmlspecialchars($contact->email)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contactlist_phone')?></td><td class="dataValue"><?= htmlspecialchars($contact->phone)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contactlist_issupplier')?></td><td class="dataValue"><?= htmlspecialchars($supplierOption)?></td></tr>
                </tbody>
            </table>
        </div>
    </div>    
    
