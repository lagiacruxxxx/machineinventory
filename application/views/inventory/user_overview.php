        
        
    <div class="dialogColumnContainer">
        <div class="dialogColumn withMargin">
            <table>
                <tbody>
                    <tr><td class="dataHeader"><?= $this->lang->line('userlist_username')?></td><td class="dataValue"><?= htmlspecialchars($user->username)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('userlist_firstname')?></td><td class="dataValue"><?= htmlspecialchars($user->firstname)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('userlist_lastname')?></td><td class="dataValue"><?= htmlspecialchars($user->lastname)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('userlist_email')?></td><td class="dataValue"><?= htmlspecialchars($user->email)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('userlist_role')?></td><td class="dataValue"><?= htmlspecialchars($userRole)?></td></tr>
                </tbody>
            </table>
        </div>
    </div>    
    
