
    <div id="object_filter" class="filter">
        <form id="objectFilterForm">
            <table>
                <tbody>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_inventorynumber')?></td>
                        <td class="dataValue"><input type="text" name="inventory_number" value="<?php if(isset($filters['inventory_number'])):?><?= $filters['inventory_number']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_objectname')?></td>
                        <td class="dataValue"><input type="text" name="name" value="<?php if(isset($filters['name'])):?><?= $filters['name']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_description')?></td>
                        <td class="dataValue"><input type="text" name="description" value="<?php if(isset($filters['description'])):?><?= $filters['description']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_serialnumber')?></td>
                        <td class="dataValue"><input type="text" name="serial_number" value="<?php if(isset($filters['serial_number'])):?><?= $filters['serial_number']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_manufacturerserialnumber')?></td>
                        <td class="dataValue"><input type="text" name="manufacturer_service_number" value="<?php if(isset($filters['manufacturer_service_number'])):?><?= $filters['manufacturer_service_number']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_client')?></td>
                        <td class="dataValue">
                            <select class="combobox" name="client_id">
                                <option value="-1"><?= $this->lang->line('combobox_nofilter')?></option>
                                <?php foreach($clients->result() as $client):?>
                                    <option value="<?= $client->id ?>" <?php if(isset($filters['client_id']) && $client->id == $filters['client_id']):?>SELECTED<?php endif;?>><?= $client->service_agreement_number . ' - ' . $client->name ?></option>
                                <?php endforeach;?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_suppliername')?></td>
                        <td class="dataValue">
                            <select class="combobox" name="supplier_contact_id">
                                <option value="-1"><?= $this->lang->line('combobox_nofilter')?></option>
                                <?php foreach($contacts->result() as $contact):?>
                                    <?php if($contact->is_supplier):?>
                                        <option value="<?= $contact->id ?>" <?php if(isset($filters['supplier_contact_id']) && $contact->id == $filters['supplier_contact_id']):?>SELECTED<?php endif;?>><?= $contact->name ?></option>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </select>
                        </td>
                    </tr>
                    
                    
                </tbody>
            </table>
            <br />
            <input type="submit" class="objectFilterSubmit filterButton" value="<?= $this->lang->line('filter')?>" />
            <input type="reset" class="objectFilterSubmit filterButton" value="<?= $this->lang->line('filter_reset')?>" />
            
        </form>
    </div>