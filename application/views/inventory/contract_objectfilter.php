
    <div class="contract_objectfilter" class="ui-corner-all">
        <form class="contractobjectFilterForm">
            <table>
                <tbody>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_objectname')?></td>
                        <td class="dataValue"><input type="text" name="name" value="<?php if(isset($filters['maintenance_number'])):?><?= $filters['maintenance_number']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_contractid')?></td>
                        <td class="dataValue">
                            <select class="combobox" name="contract_id">
                                <option value="-1"><?= $this->lang->line('combobox_nofilter')?></option>
                                <?php foreach($contracts->result() as $contract):?>
                                    <option value="<?= $contract->id ?>" ><?= $contract->company ?></option>
                                <?php endforeach;?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
            <input type="submit" class="contractobjectFilterSubmit filterButton" value="<?= $this->lang->line('filter')?>" />
            <input type="reset" class="contractobjectFilterSubmit filterButton" value="<?= $this->lang->line('filter_reset')?>" />
            
        </form>
    </div>
    
    