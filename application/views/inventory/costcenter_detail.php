
        <div class="costcenterActions dialogActionbar">
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="costcenterDelete dialogAction"><?= $this->lang->line('objectaction_delete')?></div>
                <div class="costcenterClone dialogAction"><?= $this->lang->line('objectaction_clone')?></div>
            <?php endif;?>
        </div>

        <div class="costcenterDetailTabs">
            <ul>
                <li class="costcenterDetailTab" ><a href="#costcenterDetail_tab_overview_<?= $costcenter->id?>"><?= $this->lang->line('userdetail_tab_overview')?></a>
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?><li class="costcenterDetailTab" ><a href="#costcenterDetail_tab_edit_<?= $costcenter->id?>"><?= $this->lang->line('userdetail_tab_edit')?></a><?php endif;?>
            </ul>
            
            <div id="costcenterDetail_tab_overview_<?= $costcenter->id?>" class="costcenterDetail_tab_overview">
                <?= $overview?>
            </div>
            
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
            <div id="costcenterDetail_tab_edit_<?= $costcenter->id?>"  class="costcenterDetail_tab_edit">
                <?= $edit?>
            </div>
            <?php endif;?>
            
        </div>
        