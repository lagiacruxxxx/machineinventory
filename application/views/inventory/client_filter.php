
    <div id="client_filter" class="filter">
        <form id="clientFilterForm">
            <table>
                <tbody>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('clientlist_name')?></td>
                        <td class="dataValue"><input type="text" name="name" value="<?php if(isset($filters['name'])):?><?= $filters['name']?><?php endif;?>" /></td>
                    </tr>
                </tbody>
            </table>
            <br />
            <input type="submit" class="clientFilterSubmit filterButton" value="<?= $this->lang->line('filter')?>" />
            <input type="reset" class="clientFilterSubmit filterButton" value="<?= $this->lang->line('filter_reset')?>" />
            
        </form>
    </div>