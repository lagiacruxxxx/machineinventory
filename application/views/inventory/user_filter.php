
    <div id="user_filter" class="filter">
        <form id="userFilterForm">
            <table>
                <tbody>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('userlist_username')?></td>
                        <td class="dataValue"><input type="text" name="username" value="<?php if(isset($filters['username'])):?><?= $filters['username']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('userlist_email')?></td>
                        <td class="dataValue"><input type="text" name="email" value="<?php if(isset($filters['email'])):?><?= $filters['email']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('userlist_role')?></td>
                        <td class="dataValue">
                            <select class="combobox" name="role">
                                <option value="-1"><?= $this->lang->line('combobox_nofilter')?></option>
                                <?php foreach($userRoles as $key => $value):?>
                                    <option value="<?= $key ?>" <?php if(isset($filters['role']) && $key == $filters['role']):?>SELECTED<?php endif;?>><?= $value ?></option>
                                <?php endforeach;?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
            <input type="submit" class="userFilterSubmit filterButton" value="<?= $this->lang->line('filter')?>" />
            <input type="reset" class="userFilterSubmit filterButton" value="<?= $this->lang->line('filter_reset')?>" />
            
        </form>
    </div>