
    <div id="object_service" class="filter">
        <form id="objectServiceForm">
            <table>
                <tbody>
                    
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_comment')?></td>
                        <td class="dataValue"><textarea id="service_comment"></textarea></td>
                    </tr>
                    
                </tbody>
            </table>
            <br />
            <input type="submit" class="objectServiceSubmit filterButton" value="<?= $this->lang->line('object_service_btn')?>" />
            <input type="reset" class="objectServiceSubmit filterButton" value="<?= $this->lang->line('cancel')?>" />
            
        </form>
    </div>