
<div id="clienttable">
    <table class="ui-corner-all">
        <thead>
            <tr class="ui-state-default">
                <th class="listSortable" sortField="name"><?= $this->lang->line('clientlist_name')?><?php if($sortfield == 'name'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?> "></div><?php endif;?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($clients as $client):?>
                <tr class="clientrow" client_id=<?= $client['id']?>>
                    <td><?= $client['name']?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>