        
        
    <div class="dialogColumnContainer">
        <form class="<?= $modeForm?>">
            <input type="hidden" name="id" value="<?= $user->id?>">
            <div class="dialogColumn withMargin">
                <table>
                    <tbody>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('userlist_username')?></td>
                            <td class="dataValue"><input type="text" name="username" value="<?= $user->username ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('userlist_firstname')?></td>
                            <td class="dataValue"><input type="text" name="firstname" value="<?= $user->firstname ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('userlist_lastname')?></td>
                            <td class="dataValue"><input type="text" name="lastname" value="<?= $user->lastname ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('userlist_email')?></td>
                            <td class="dataValue"><input type="text" name="email" value="<?= $user->email ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('userlist_role')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="role">
                                    <?php foreach($userRoles as $key => $value):?>
                                        <option value="<?= $key ?>" <?php if($user->role == $key):?>SELECTED<?php endif;?>><?= $value?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr class="table_separator"></tr>
                        <tr>
                            <td class="dataHeader"><?= $modeForm == 'userAddForm' ? $this->lang->line('userlist_password') : $this->lang->line('userlist_change_pw')?></td>
                            <td class="dataValue"><input type="password" name="password_change" value="" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('userlist_confirm_pw')?></td>
                            <td class="dataValue"><input type="password" name="password_confirm" value="" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br clear="both"/>
            <div class="DialogButtonHolder">
                <input type="submit" value="<?= $this->lang->line('userdetail_edit_save')?>">
            </div>
        </form>
    </div>    
    
