        
        <?php if(MyCore::hasPrivilege(USER_PRIV_ADMIN)):?>
        <div class="userActions dialogActionbar">
            <div class="userDelete dialogAction"><?= $this->lang->line('objectaction_delete')?></div>
            <div class="userClone dialogAction"><?= $this->lang->line('objectaction_clone')?></div>
        </div>

        <div class="userDetailTabs">
            <ul>
                <li class="userDetailTab" ><a href="#userDetail_tab_overview_<?= $user->id?>"><?= $this->lang->line('userdetail_tab_overview')?></a>
                <li class="userDetailTab" ><a href="#userDetail_tab_edit_<?= $user->id?>"><?= $this->lang->line('userdetail_tab_edit')?></a>
            </ul>
            
            <div id="userDetail_tab_overview_<?= $user->id?>" class="userDetail_tab_overview">
                <?= $overview?>
            </div>
            
            <div id="userDetail_tab_edit_<?= $user->id?>"  class="userDetail_tab_edit">
                <?= $edit?>
            </div>
            
        </div>
        <?php endif;?>
        