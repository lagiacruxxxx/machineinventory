
<div id="contacttable">
    <table class="ui-corner-all">
        <thead>
            <tr class="ui-state-default">
                <th class="listSortable" sortField="name"><?= $this->lang->line('contactlist_name')?><?php if($sortfield == 'name'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?> "></div><?php endif;?></th>
                <th class="listSortable" sortField="address"><?= $this->lang->line('contactlist_address')?><?php if($sortfield == 'address'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="email"><?= $this->lang->line('contactlist_email')?><?php if($sortfield == 'email'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="phone"><?= $this->lang->line('contactlist_phone')?><?php if($sortfield == 'phone'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="is_supplier"><?= $this->lang->line('contactlist_issupplier')?><?php if($sortfield == 'is_supplier'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($contacts as $contact):?>
                <tr class="contactrow" contact_id=<?= $contact['id']?>>
                    <td><?= $contact['name']?></td>
                    <td><?= $contact['address']?></td>
                    <td><?= $contact['email']?></td>
                    <td><?= $contact['phone']?></td>
                    <td><?= $contact['isSupplier']?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>