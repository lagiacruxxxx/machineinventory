

        <div id="notificationlist" class="ui-widget-content notificationlistHidden">
            <?php foreach($notifications as $not):?>
                <?php if($not['type'] == NOTIFICATION_TYPE_CONTRACTEXPIRE):?>
                    <div class="notification" type=<?= $not['type']?> notification_id=<?= $not['id']?> contract_id=<?= $not['entity_id']?>>
                        <div class="notification_text"><?= $this->lang->line('notification_contractexpire') . $not['text']?></div>
                        <div class="notification_dismiss"><img src="<?= site_url('items/inventory/img/dismiss.png') ?>" title="Meldung verwerfen"/></div>
                    </div>
                <?php endif;?>
            <?php endforeach;?>
        </div>