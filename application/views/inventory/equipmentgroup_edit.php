        
        
    <div class="dialogColumnContainer">
        <form class="<?= $modeForm?>">
            <input type="hidden" name="id" value="<?= $equipmentgroup->id?>">
            <div class="dialogColumn withMargin">
                <table>
                    <tbody>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('equipmentgrouplist_name')?></td>
                            <td class="dataValue"><input type="text" name="name" value="<?= $equipmentgroup->name ?>" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br clear="both"/>
            <div class="DialogButtonHolder">
                <input type="submit" value="<?= $this->lang->line('userdetail_edit_save')?>">
            </div>
        </form>
    </div>    
    
