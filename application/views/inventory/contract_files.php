


    <?php foreach($upload_data->result() as $uploadItem):?>
        <div class="upload_item">
            <div class="upload_item_user"><?= $uploadItem->username?></div>
            <div class="upload_item_date"><?= $uploadItem->createddate?></div>
            <a href="<?= site_url('items/uploads/contractfiles/' . $uploadItem->filename)?>" download target="_blank"><img class="upload_item_dl" src="<?= site_url('items/inventory/img/icon_download.png')?>" /></a>
            <div class="upload_item_file"><?= htmlspecialchars($uploadItem->filedescription)?></div>
        </div>
    <?php endforeach;?>    
        