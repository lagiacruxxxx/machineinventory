
    <div id="contract_service" class="filter">
        <form id="contractServiceForm">
            <table>
                <tbody>
                    
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('objectlist_comment')?></td>
                        <td class="dataValue"><textarea id="contract_service_comment"></textarea></td>
                    </tr>
                    
                </tbody>
            </table>
            <br />
            <input type="submit" class="contractServiceSubmit filterButton" value="<?= $this->lang->line('object_service_btn')?>" />
            <input type="reset" class="contractServiceSubmit filterButton" value="<?= $this->lang->line('cancel')?>" />
            
        </form>
    </div>