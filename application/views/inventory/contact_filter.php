
    <div id="contact_filter" class="filter">
        <form id="contactFilterForm">
            <table>
                <tbody>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('contactlist_name')?></td>
                        <td class="dataValue"><input type="text" name="name" value="<?php if(isset($filters['name'])):?><?= $filters['name']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('contactlist_address')?></td>
                        <td class="dataValue"><input type="text" name="address" value="<?php if(isset($filters['address'])):?><?= $filters['address']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('contactlist_email')?></td>
                        <td class="dataValue"><input type="text" name="email" value="<?php if(isset($filters['email'])):?><?= $filters['email']?><?php endif;?>" /></td>
                    </tr>
                    <tr>
                        <td class="dataHeader"><?= $this->lang->line('contactlist_issupplier')?></td>
                        <td class="dataValue">
                            <select class="combobox" name="is_supplier">
                                <option value="-1"><?= $this->lang->line('combobox_nofilter')?></option>
                                <?php foreach($supplierOptions as $key => $value):?>
                                    <option value="<?= $key ?>" <?php if(isset($filters['is_supplier']) && $key == $filters['is_supplier']):?>SELECTED<?php endif;?>><?= $value ?></option>
                                <?php endforeach;?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
            <input type="submit" id="contactFilterSubmit" class="contactFilterSubmit filterButton" value="<?= $this->lang->line('filter')?>" />
            <input type="reset" id="" class="contactFilterSubmit filterButton" value="<?= $this->lang->line('filter_reset')?>" />
            
        </form>
    </div>