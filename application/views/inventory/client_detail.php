
        <div class="clientActions dialogActionbar">
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="clientDelete dialogAction"><?= $this->lang->line('objectaction_delete')?></div>
                <div class="clientClone dialogAction"><?= $this->lang->line('objectaction_clone')?></div>
            <?php endif;?>
        </div>

        <div class="clientDetailTabs">
            <ul>
                <li class="clientDetailTab" ><a href="#clientDetail_tab_overview_<?= $client->id?>"><?= $this->lang->line('userdetail_tab_overview')?></a>
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?><li class="clientDetailTab" ><a href="#clientDetail_tab_edit_<?= $client->id?>"><?= $this->lang->line('userdetail_tab_edit')?></a><?php endif;?>
            </ul>
            
            <div id="clientDetail_tab_overview_<?= $client->id?>" class="clientDetail_tab_overview">
                <?= $overview?>
            </div>
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
            <div id="clientDetail_tab_edit_<?= $client->id?>"  class="clientDetail_tab_edit">
                <?= $edit?>
            </div>
            <?php endif;?>
            
        </div>
        