
<div id="usertable">
    <table class="ui-corner-all">
        <thead>
            <tr class="ui-state-default">
                <th class="listSortable" sortField="username"><?= $this->lang->line('userlist_username')?><?php if($sortfield == 'username'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?> "></div><?php endif;?></th>
                <th class="listSortable" sortField="firstname"><?= $this->lang->line('userlist_firstname')?><?php if($sortfield == 'firstname'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="lastname"><?= $this->lang->line('userlist_lastname')?><?php if($sortfield == 'lastname'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="email"><?= $this->lang->line('userlist_email')?><?php if($sortfield == 'email'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="role"><?= $this->lang->line('userlist_role')?><?php if($sortfield == 'role'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($users as $user):?>
                <tr class="userrow" user_id=<?= $user['id']?>>
                    <td><?= $user['username']?></td>
                    <td><?= $user['firstname']?></td>
                    <td><?= $user['lastname']?></td>
                    <td><?= $user['email']?></td>
                    <td><?= $user['role_name']?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>