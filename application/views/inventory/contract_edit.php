        
        
    <div class="dialogColumnContainer">
        <form class="<?= $modeForm?>">
            <input type="hidden" name="id" value="<?= $contract->id?>">
            <div class="dialogColumn withMargin">
                <table>
                    <tbody>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_company')?></td>
                            <td class="dataValue"><input type="text" name="company" value="<?= $contract->company ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_maintenancenumber')?></td>
                            <td class="dataValue"><input type="text" name="maintenance_number" value="<?= $contract->maintenance_number ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_serviceagreementnumber')?></td>
                            <td class="dataValue"><input type="text" name="service_agreement_number" value="<?= $contract->service_agreement_number ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_costs')?></td>
                            <td class="dataValue"><input type="text" name="costs" value="<?= $contract->costs ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_comment')?></td>
                            <td class="dataValue"><textarea name="comment"><?= $contract->comment ?></textarea></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="dialogColumn">
                <table>
                    <tbody>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_externalcontact')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="external_contact">
                                    <option value="-1" <?php if($contract->external_contact == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('contacts_select'))?></option>
                                    <?php foreach($contacts->result() as $c):?>
                                        <option value="<?= $c->id?>" <?php if($c->id == $contract->external_contact):?>SELECTED<?php endif;?>><?= $c->name?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_internalcontact')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="internal_contact">
                                    <option value="-1" <?php if($contract->internal_contact == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('contacts_select'))?></option>
                                    <?php foreach($contacts->result() as $c):?>
                                        <option value="<?= $c->id?>" <?php if($c->id == $contract->internal_contact):?>SELECTED<?php endif;?>><?= $c->name?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>                        
                        <tr class="table_separator"></tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_startdate')?></td>
                            <td class="dataValue"><input class="datepicker" type="text" name="start_date" value="<?= $contract->start_date ? date('d.m.Y', strtotime($contract->start_date)) : '' ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_autorenew')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="auto_renew">
                                    <?php foreach($autorenewOptions as $key => $value):?>
                                        <option value="<?= $key?>" <?php if($key == $contract->auto_renew):?>SELECTED<?php endif;?>><?= $value?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_renewinterval')?></td>
                            <td class="dataValue"><input type="text" name="renewInterval" value="<?= $contract->renewInterval ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contractlist_reminderTolerance')?></td>
                            <td class="dataValue"><input type="text" name="reminderTolerance" value="<?= $contract->reminderTolerance ?>" /></td>
                        </tr>
                        
                        
                    </tbody>
                </table>
            </div>
            <br clear="both"/>
            <div class="DialogButtonHolder">
                <input type="submit" value="<?= $this->lang->line('objectdetail_edit_save')?>">
            </div>
        </form>
    </div>    
    
