
    <div class="contract_objecttable">
        <table class="ui-corner-all">
            <thead>
                <tr class="ui-state-default">
                    <th></th>
                    <th ><?= $this->lang->line('objectlist_objectid')?></th>
                    <th ><?= $this->lang->line('objectlist_objectname')?></th>
                    <th ><?= $this->lang->line('objectlist_nextmaintenance')?></th>
                    <th ><?= $this->lang->line('objectlist_serialnumber')?></th>
                    <th ><?= $this->lang->line('objectlist_location')?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($objects as $object):?>
                <tr class="contract_objectrow <?= $object['nextMaintenance']['cc']?>" object_id=<?= $object['id']?>>
                    <td><input type="checkbox"></td>
                    <td><?= $object['object_id']?></td>
                    <td><?= $object['name']?></td>
                    <td><?= $object['next_maintenance']?></td>
                    <td><?= $object['serial_number']?></td>
                    <td><?= $object['location']?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
    