        
        
    <div class="dialogColumnContainer">
        <div class="dialogColumn withMargin">
            <table>
                <tbody>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_company')?></td><td class="dataValue"><?= htmlspecialchars($contract->company)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_maintenancenumber')?></td><td class="dataValue"><?= htmlspecialchars($contract->maintenance_number)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_serviceagreementnumber')?></td><td class="dataValue"><?= htmlspecialchars($contract->service_agreement_number)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_costs')?></td><td class="dataValue"><?= htmlspecialchars($contract->costs)?></td></tr>
                    <tr class="table_separator"></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_comment')?></td><td class="dataValue"><?= htmlspecialchars($contract->comment) ?></td></tr>
                </tbody>
            </table>
        </div>
        <div class="dialogColumn">
            <table>
                <tbody>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_externalcontact')?></td><td class="dataValue"><?= $contact1 != null ? htmlspecialchars($contact1->name) : ''?><br/><br/><?= $contact1 != null ? nl2br(htmlspecialchars($contact1->address)) : ''?><br/><br/><?= $contact1 != null ? mailto($contact1->email, $contact1->email) : ''?><br/><?= $contact1 != null ? htmlspecialchars($contact1->phone) : ''?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_internalcontact')?></td><td class="dataValue"><?= $contact2 != null ? htmlspecialchars($contact2->name) : ''?><br/><br/><?= $contact2 != null ? nl2br(htmlspecialchars($contact2->address)) : ''?><br/><br/><?= $contact2 != null ? mailto($contact2->email, $contact2->email) : ''?><br/><?= $contact2 != null ? htmlspecialchars($contact2->phone) : ''?></td></tr>
                    <tr class="table_separator"></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_startdate')?></td><td class="dataValue"><?= $contract->start_date ? date('d.m.Y', strtotime($contract->start_date)) : '' ?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_enddate')?></td><td class="dataValue"><?= $contract->end_date ? date('d.m.Y', strtotime($contract->end_date)) : '' ?></span></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_autorenew')?></td><td class="dataValue"><?= htmlspecialchars($autorenewOptions[$contract->auto_renew])?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_renewinterval')?></td><td class="dataValue"><?= htmlspecialchars($contract->renewInterval)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('contractlist_reminderTolerance')?></td><td class="dataValue"><?= htmlspecialchars($contract->reminderTolerance)?></td></tr>
                </tbody>
            </table>
        </div>
    </div>    
    
