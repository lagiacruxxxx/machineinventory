<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <title>Backend</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta charset="utf-8">
		
		<!-- <link rel="icon" href="<?=site_url("items/frontend/img/favicon.ico"); ?>" type="image/ico" /> -->
        <!-- CSS -->
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/reset.css"); ?>">
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/fonts.css"); ?>">
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/progress.css"); ?>">
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/jquery-ui.css"); ?>">
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/jquery-ui.theme.css"); ?>">
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/jquery.fancybox.css"); ?>">
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/fullcalendar.css"); ?>">
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/inventory/css/inventory.css"); ?>">
    
    	<!-- JS -->
       	<script type="text/javascript" src="<?=site_url("items/inventory/js/jquery-2.2.0.min.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/jquery-ui.min.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/jquery.mobile.custom.min.js"); ?>"></script> 
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/jquery.dialogextend.min.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/moment.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/fullcalendar.min.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/de.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/jquery.fancybox.pack.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/cookie.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/responsive.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/inventory.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/object.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/user.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/client.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/equipmentgroup.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/contract.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/costcenter.js"); ?>"></script>
    	<script type="text/javascript" src="<?=site_url("items/inventory/js/contact.js"); ?>"></script>
    	
		<!-- VARIABLES -->
		<script type="text/javascript">
            var rootUrl = "<?= site_url(); ?>";
		</script>
    </head>

    <body>
    
        <div id="dialogContainer"></div>
        <div id="progressOverlay">
            <div class='loader'>
                <div class='loader--dot'></div>
                <div class='loader--dot'></div>
                <div class='loader--dot'></div>
                <div class='loader--dot'></div>
                <div class='loader--dot'></div>
                <div class='loader--dot'></div>
              </div>
        </div>
	   