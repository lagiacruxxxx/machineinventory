
<div id="equipmentgrouptable">
    <table class="ui-corner-all">
        <thead>
            <tr class="ui-state-default">
                <th class="listSortable" sortField="name"><?= $this->lang->line('equipmentgrouplist_name')?><?php if($sortfield == 'name'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?> "></div><?php endif;?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($equipmentgroups as $equipmentgroup):?>
                <tr class="equipmentgrouprow" equipmentgroup_id=<?= $equipmentgroup['id']?>>
                    <td><?= $equipmentgroup['name']?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>