
    <div id="menubar" class="ui-state-active ui-corner-all">
        <div id="menutitle"><?= $this->lang->line('menutitle')?></div>
        <div id="logout"><a href="<?= site_url('Authentication/logout')?>"><?= $this->lang->line('logout')?></a></div>
        <?php if($isAdmin):?><div id="notifications" <?php if($notifications['count'] > 0):?>class="notificationsActive"<?php endif;?>><?= $this->lang->line('notifications')?></div><?php endif;?>
        <div id="profile"><?= $this->lang->line('profile')?></div>
        <div id="logged_in_user"><?= $logged_in_user?></div>
        <div id="logged_in_as"><?= $this->lang->line('logged_in_as')?></div> 
    </div>
    
    <?php if($isAdmin) echo $notifications['list'];?>
    
    <div id="container">
    
        
    
        <ul id="entitiyTabs">
            <li class="entityTab" ><a href="#calendarTab"><?= $this->lang->line('calendar_tab')?></a>
            <li class="entityTab" ><a href="#objectTab"><?= $this->lang->line('objectlist_header')?></a>
            <li class="entityTab" ><a href="#contractTab"><?= $this->lang->line('contract_tab')?></a>
            <li class="entityTab" ><a href="#contactTab"><?= $this->lang->line('contact_tab')?></a>
            <li class="entityTab" ><a href="#costcenterTab"><?= $this->lang->line('costcenter_tab')?></a>
            <li class="entityTab" ><a href="#equipmentgroupTab"><?= $this->lang->line('equipmentgroup_header')?></a>
            <li class="entityTab" ><a href="#clientTab"><?= $this->lang->line('client_tab')?></a>
            <?php if($isAdmin):?><li class="entityTab" ><a href="#userTab"><?= $this->lang->line('user_tab')?></a><?php endif;?>
        </ul>
        
        <div id="calendarTab" class="entityTabContent">
            <div id="calendar"></div>
        </div>
        
        <div id="objectTab" class="entityTabContent">
            <?= $objectmenu?>
            <?= $objectfilter?>
            <?= $objectservice?>
            <?= $objectlist?>
        </div>
        
        <div id="contractTab" class="entityTabContent">
            <?= $contractmenu?>
            <?= $contractfilter?>
            <?= $contractlist?>
            <?= $contractservice?>
        </div>
        
        <div id="contactTab" class="entityTabContent">
            <?= $contactmenu?>
            <?= $contactfilter?>
            <?= $contactlist?>             
        </div>

        <div id="costcenterTab" class="entityTabContent">
            <?= $costcentermenu?>
            <?= $costcenterfilter?>
            <?= $costcenterlist?>         
        </div>        
        
        <div id="equipmentgroupTab" class="entityTabContent">
            <?= $equipmentgroupmenu?>
            <?= $equipmentgroupfilter?>
            <?= $equipmentgrouplist?>         
        </div>
        
        <div id="clientTab" class="entityTabContent">
            <?= $clientmenu?>
            <?= $clientfilter?>
            <?= $clientlist?>        
        </div>
        
        <?php if($isAdmin):?>
        <div id="userTab" class="entityTabContent">
            <?= $usermenu?>
            <?= $userfilter?>
            <?= $userlist?>
        </div>
        <?php endif;?>
        
    </div>
    
