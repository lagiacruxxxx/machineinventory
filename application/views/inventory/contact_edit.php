        
        
    <div class="dialogColumnContainer">
        <form class="<?= $modeForm?>">
            <input type="hidden" name="id" value="<?= $contact->id?>">
            <div class="dialogColumn withMargin">
                <table>
                    <tbody>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contactlist_name')?></td>
                            <td class="dataValue"><input type="text" name="name" value="<?= $contact->name ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contactlist_address')?></td>
                            <td class="dataValue"><input type="text" name="address" value="<?= $contact->address ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contactlist_email')?></td>
                            <td class="dataValue"><input type="text" name="email" value="<?= $contact->email ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contactlist_phone')?></td>
                            <td class="dataValue"><input type="text" name="phone" value="<?= $contact->phone ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('contactlist_issupplier')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="is_supplier">
                                    <?php foreach($supplierOptions as $key => $value):?>
                                        <option value="<?= $key ?>" <?php if($contact->is_supplier == $key):?>SELECTED<?php endif;?>><?= $value?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br clear="both"/>
            <div class="DialogButtonHolder">
                <input type="submit" value="<?= $this->lang->line('contactdetail_edit_save')?>">
            </div>
        </form>
    </div>    
    
