
        <div class="contractActions dialogActionbar">
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="contractDelete dialogAction"><?= $this->lang->line('objectaction_delete')?></div>
                <div class="contractClone dialogAction"><?= $this->lang->line('objectaction_clone')?></div>
            <?php endif;?> 
        </div>

        <div class="contractDetailTabs">
            <ul>
                <li class="contactDetailTab" ><a href="#contractDetail_tab_overview_<?= $contract->id?>"><?= $this->lang->line('objectdetail_tab_overview')?></a>
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?><li class="contactDetailTab" ><a href="#contractDetail_tab_edit_<?= $contract->id?>"><?= $this->lang->line('objectdetail_tab_edit')?></a><?php endif;?>
                <li class="contactDetailTab"><a href="#contractDetail_tab_files_<?= $contract->id?>"><?= $this->lang->line('objectdetail_tab_files')?></a>
                <li class="contactDetailTab"><a href="#contractDetail_tab_objects_<?= $contract->id?>"><?= $this->lang->line('contractdetail_tab_objects')?></a>
                <li class="contactDetailTab"><a href="#contractDetail_tab_objectadd_<?= $contract->id?>"><?= $this->lang->line('contractdetail_tab_objectadd')?></a>
            </ul>
            
            <div id="contractDetail_tab_overview_<?= $contract->id?>" class="contractDetail_tab_overview">
                <?= $overview?>
            </div>
            
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
            <div id="contractDetail_tab_edit_<?= $contract->id?>"  class="contractDetail_tab_edit">
                <?= $edit?>
            </div>
            <?php endif;?>
            
            <div id="contractDetail_tab_objects_<?= $contract->id?>"  class="contractDetail_tab_objects">
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                    <input type="button" class="contract_object_select" value="<?= $this->lang->line('contract_objects_select')?>" checkstate="unchecked"/>
                    <input type="button" class="contract_object_service" value="<?= $this->lang->line('contract_object_service')?>" />
                <?php endif;?>
                <br clear="both" /> 
                <?= $objects?>
            </div>
            
            <div id="contractDetail_tab_objectadd_<?= $contract->id?>"  class="contractDetail_tab_objectadd">
            
                <div class="">
                    <?= $filterview ?>
                </div>
                <br clear="both" /> 
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                    <input type="button" class="contract_object_select_add" value="<?= $this->lang->line('contract_objects_select')?>" checkstate="unchecked"/>
                    <input type="button" class="contract_object_add" value="<?= $this->lang->line('contract_object_add')?>" />
                <?php endif;?>
                <br clear="both" /> 
                <?= $objectAdd?>
            </div>
            
            <div id="contractDetail_tab_files_<?= $contract->id?>"  class="contractDetail_tab_files">
                <div class="bc_message_container"></div>
                <div class="fileupload_items">
                    <?= $files?>
                </div>
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="divider"></div>
                
                <div class="fileupload_edit">
                    <div class="tab_header"><?= $this->lang->line('objectdetail_tabheader_files')?></div>
                    <form class="fileupload_edit_form">
                        <span class="contractDetail_editspan"><?= $this->lang->line('objectdetail_files_textdesc')?></span>
                        <input type="hidden" class="contractFileId" name="contract_id" value="<?= $contract->id ?>">
                        <input type="text" class="contractFileDesc" name="filedescription" class="fileupload_edit_desc" /><br/>
                        <input type="file" name="filename" class="fileupload_edit_file" error_empty="<?= $this->lang->line('error_upload_empty')?>"/><br/>
                        <input type="submit" class="fileupload_edit_save" value="<?= $this->lang->line('objectdetail_history_save')?>"/>
                    </form>
                </div>
                <?php endif;?>
            </div>
        </div>
        
        