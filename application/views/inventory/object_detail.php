
        <div class="objectActions dialogActionbar">
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="objectDelete dialogAction"><?= $this->lang->line('objectaction_delete')?></div>
                <div class="objectClone dialogAction"><?= $this->lang->line('objectaction_clone')?></div>
                <div class="objectService dialogAction"><?= $this->lang->line('objectaction_service')?></div>
            <?php endif;?>
        </div>

        <div class="objectDetailTabs">
            <ul>
                <li class="objectDetailTab" ><a href="#objectDetail_tab_overview_<?= $object->id?>"><?= $this->lang->line('objectdetail_tab_overview')?></a>
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?><li class="objectDetailTab" ><a href="#objectDetail_tab_edit_<?= $object->id?>"><?= $this->lang->line('objectdetail_tab_edit')?></a><?php endif;?>
                <li class="objectDetailTab"><a href="#objectDetail_tab_history_<?= $object->id?>"><?= $this->lang->line('objectdetail_tab_history')?></a>
                <li class="objectDetailTab"><a href="#objectDetail_tab_files_<?= $object->id?>"><?= $this->lang->line('objectdetail_tab_files')?></a>
            </ul>
            
            <div id="objectDetail_tab_overview_<?= $object->id?>" class="objectDetail_tab_overview">
                <?= $overview?>
            </div>
            
            <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
            <div id="objectDetail_tab_edit_<?= $object->id?>"  class="objectDetail_tab_edit">
                <?= $edit?>
            </div>
            <?php endif;?>
            
            <div id="objectDetail_tab_history_<?= $object->id?>" class="objectDetail_tab_history">
                 <div class="bc_message_container"></div>
                <div class="history_items">
                    <?= $history?>
                </div>
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="divider"></div>
                <div class="history_edit">
                    <div class="tab_header"><?= $this->lang->line('objectdetail_tabheader_history')?></div>
                    <form class="history_edit_form">
                        <input type="hidden" name="object_id" class="object_history_id_dummy" value="<?= $object->id?>" />
                        <span class="objectDetail_editspan"><?= $this->lang->line('objectdetail_history_comment')?></span>
                        <textarea name="comment" class="history_edit_content"></textarea>
                        <input type="submit" value="<?= $this->lang->line('objectdetail_edit_save') ?>" />
                    </form>
                </div>
                <?php endif;?>
            </div>
            
            <div id="objectDetail_tab_files_<?= $object->id?>"  class="objectDetail_tab_files">
                <div class="bc_message_container"></div>
                <div class="fileupload_items">
                    <?= $files?>
                </div>
                
                <?php if(MyCore::hasPrivilege(USER_PRIV_EDIT)):?>
                <div class="divider"></div>
                
                <div class="fileupload_edit">
                    <div class="tab_header"><?= $this->lang->line('objectdetail_tabheader_files')?></div>
                    <form class="fileupload_edit_form">
                        <span class="objectDetail_editspan"><?= $this->lang->line('objectdetail_files_textdesc')?></span>
                        <input type="hidden" class="objectFileId" name="object_id" value="<?= $object->id ?>">
                        <input type="text" class="objectFileDesc" name="filedescription" class="fileupload_edit_desc" /><br/>
                        <input type="file" name="filename" class="fileupload_edit_file" error_empty="<?= $this->lang->line('error_upload_empty')?>"/><br/>
                        <input type="submit" class="fileupload_edit_save" value="<?= $this->lang->line('objectdetail_history_save')?>"/>
                    </form>
                </div>
                <?php endif;?>
            </div>
        </div>
        