        
        
    <div class="dialogColumnContainer">
        <form class="<?= $modeForm?>">
            <input type="hidden" name="id" value="<?= $object->id?>">
            <div class="dialogColumn withMargin">
                <table>
                    <tbody>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_inventorynumber')?></td>
                            <td class="dataValue"><input type="text" name="inventory_number" value="<?= $object->inventory_number ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_serialnumber')?></td>
                            <td class="dataValue"><input type="text" name="serial_number" value="<?= $object->serial_number ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_GLTnumber')?></td>
                            <td class="dataValue"><input type="text" name="GLT_number" value="<?= $object->GLT_number ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_manufacturerserialnumber')?></td>
                            <td class="dataValue"><input type="text" name="manufacturer_service_number" value="<?= $object->manufacturer_service_number ?>" /></td>
                        </tr>
                        <tr class="table_separator"></tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_status')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="status_id">
                                    <?php foreach($stati->result() as $st):?>
                                        <option value="<?= $st->id?>" <?php if($st->id == $object->status_id):?>SELECTED<?php endif;?>><?= htmlspecialchars($st->name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr class="table_separator"></tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_equipmentgroup')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="equipmentgroup_id">
                                    <option value="-1" <?php if($object->equipmentgroup_id == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('equipmentgroup_select'))?></option>
                                    <?php foreach($equipmentgroups->result() as $eq):?>
                                        <option value="<?= $eq->id?>" <?php if($eq->id == $object->equipmentgroup_id):?>SELECTED<?php endif;?>><?= htmlspecialchars($eq->name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_objectname')?></td>
                            <td class="dataValue"><input type="text" name="name" value="<?= $object->name ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_manufacturer')?></td>
                            <td class="dataValue"><input type="text" name="manufacturer" value="<?= $object->manufacturer ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_description')?></td>
                            <td class="dataValue"><textarea name="description" ><?= $object->description ?></textarea></td>
                        </tr>
                        <tr class="table_separator"></tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_purchasedate')?></td>
                            <td class="dataValue"><input class="datepicker" type="text" name="purchase_date" value="<?= $object->purchase_date ? date('d.m.Y', strtotime($object->purchase_date)) : '' ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_purchaseprice')?></td>
                            <td class="dataValue"><input type="text" name="purchase_price" value="<?= $object->purchase_price ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_installationdate')?></td>
                            <td class="dataValue"><input class="datepicker" type="text" name="installation_date" value="<?= $object->installation_date ? date('d.m.Y', strtotime($object->installation_date)) : '' ?>" /></td>
                        </tr>
                        
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_retiredDate')?></td>
                            <td class="dataValue"><input class="datepicker" type="text" name="retired_date" value="<?= $object->retired_date ? date('d.m.Y', strtotime($object->retired_date)) : '' ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_client')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="client_id">
                                    <option value="-1" <?php if($object->client_id == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('mandant_select'))?></option>
                                    <?php foreach($clients->result() as $cl):?>
                                        <option value="<?= $cl->id?>" <?php if($cl->id == $object->client_id):?>SELECTED<?php endif;?>><?= htmlspecialchars($cl->name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_location')?></td>
                            <td class="dataValue"><input type="text" name="location" value="<?= $object->location ?>" /></td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
            <div class="dialogColumn">
                <table>
                    <tbody>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_anschaffendeKST')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="created_costcenter">
                                    <option value="-1" <?php if($object->created_costcenter == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('costcenter_select'))?></option>
                                    <?php foreach($costcenters->result() as $cc):?>
                                        <option value="<?= $cc->id?>" <?php if($cc->id == $object->created_costcenter):?>SELECTED<?php endif;?>><?= htmlspecialchars($cc->name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_gebuchteKST')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="booked_costcenter">
                                    <option value="-1" <?php if($object->booked_costcenter == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('costcenter_select'))?></option>
                                    <?php foreach($costcenters->result() as $cc):?>
                                        <option value="<?= $cc->id?>" <?php if($cc->id == $object->booked_costcenter):?>SELECTED<?php endif;?>><?= htmlspecialchars($cc->name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr class="table_separator"></tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_contractid')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="contract_id">
                                    <option value="-1" <?php if($object->contract_id == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('contract_select'))?></option>
                                    <?php foreach($contracts->result() as $co):?>
                                        <option value="<?= $co->id?>" <?php if($co->id == $object->contract_id):?>SELECTED<?php endif;?>><?= htmlspecialchars($co->maintenance_number) . ' - ' . htmlspecialchars($co->company)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_warrantydate')?></td>
                            <td class="dataValue"><input class="datepicker" type="text" name="warranty_date" value="<?= $object->warranty_date ? date('d.m.Y', strtotime($object->warranty_date)) : '' ?>" /></td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_serviceinterval')?> <?= $this->lang->line('months')?></td>
                            <td class="dataValue"><input type="text" name="serviceinterval" value="<?= $object->serviceinterval ?>" /></td>
                        </tr>
                        <tr class="table_separator"></tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_suppliername')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="supplier_contact_id">
                                    <option value="-1" <?php if($object->supplier_contact_id == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('contacts_select'))?></option>
                                    <?php foreach($contacts->result() as $c):?>
                                        <?php if($c->is_supplier == 1):?>
                                            <option value="<?= $c->id?>" <?php if($c->id == $object->supplier_contact_id):?>SELECTED<?php endif;?>><?= htmlspecialchars($c->name)?></option>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <?php if($modeForm == 'objectEditForm'):?>
                            <tr>
                                <td class="dataHeader"><?= $this->lang->line('objectlist_lastmaintenance')?></td>
                                <td class="dataValue"><input class="datepicker" type="text" name="last_maintenance" value="<?= $object->last_maintenance ? date('d.m.Y', strtotime($object->last_maintenance)) : '' ?>" /></td>
                            </tr>
                            <tr>
                                <td class="dataHeader"><?= $this->lang->line('objectlist_nextmaintenance')?></td>
                                <td class="dataValue"><input class="datepicker" type="text" name="next_maintenance" value="<?= $object->next_maintenance ? date('d.m.Y', strtotime($object->next_maintenance)) : '' ?>" /></td>
                            </tr>
                        <?php endif;?>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_contact1')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="contact1">
                                    <option value="-1" <?php if($object->contact1 == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('contacts_select'))?></option>
                                    <?php foreach($contacts->result() as $c):?>
                                        <option value="<?= $c->id?>" <?php if($c->id == $object->contact1):?>SELECTED<?php endif;?>><?= htmlspecialchars($c->name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataHeader"><?= $this->lang->line('objectlist_contact2')?></td>
                            <td class="dataValue">
                                <select class="combobox" name="contact2">
                                    <option value="-1" <?php if($object->contact2 == null):?>SELECTED<?php endif;?>><?= htmlspecialchars($this->lang->line('contacts_select'))?></option>
                                    <?php foreach($contacts->result() as $c):?>
                                        <option value="<?= $c->id?>" <?php if($c->id == $object->contact2):?>SELECTED<?php endif;?>><?= htmlspecialchars($c->name)?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                        <tr class="table_separator"></tr>
                        <?php if($modeForm == 'objectEditForm'):?>
                        <tr>
                            <td class="dataValue" colspan=2><div class="object_upload_img"><?= $this->lang->line('objectlist_upload_new_img')?></div><input type="file" class="object_upload_img_file" accept=".gif,.png,.jpg,.jpeg"/></td>
                        </tr>
                        <?php endif;?>
                        
                    </tbody>
                </table>
            </div>
            <br clear="both"/>
            <div class="DialogButtonHolder">
                <input type="submit" value="<?= $this->lang->line('objectdetail_edit_save')?>">
            </div>
        </form>
    </div>    
    
