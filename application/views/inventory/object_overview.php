        
        
    <div class="dialogColumnContainer">
        <div class="dialogColumn withMargin">
            <table>
                <tbody>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_objectid')?></td><td class="dataValue"><?= $object->object_id?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_inventorynumber')?></td><td class="dataValue"><?= htmlspecialchars($object->inventory_number)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_serialnumber')?></td><td class="dataValue"><?= htmlspecialchars($object->serial_number)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_manufacturerserialnumber')?></td><td class="dataValue"><?= htmlspecialchars($object->manufacturer_service_number)?></td></tr>
                    <tr class="table_separator"></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_status')?></td><td class="dataValue"><?= htmlspecialchars($status->name)?></td></tr>
                    <tr class="table_separator"></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_equipmentgroup')?></td><td class="dataValue"><?= $equipmentgroup != null ? htmlspecialchars($equipmentgroup->name) : ''?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_objectname')?></td><td class="dataValue"><?= htmlspecialchars($object->name)?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_manufacturer')?></td><td class="dataValue"><?= htmlspecialchars($object->manufacturer)?></td></tr>
                    <tr class="table_separator"></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_purchasedate')?></td><td class="dataValue"><?= $object->purchase_date ? date('d.m.Y', strtotime($object->purchase_date)) : '' ?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_installationdate')?></td><td class="dataValue"><?= $object->installation_date ? date('d.m.Y', strtotime($object->installation_date)) : '' ?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_retiredDate')?></td><td class="dataValue"><?= $object->retired_date ? date('d.m.Y', strtotime($object->retired_date)) : '' ?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_client')?></td><td class="dataValue"><?= $client != null ? htmlspecialchars($client->name) : '' ?></td></tr>
                    <tr class="table_separator"></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_contractid')?></td><td class="dataValue"><?= $contract != null ? htmlspecialchars($contract->maintenance_number) . ' - ' . htmlspecialchars($contract->company) : ''?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_warrantydate')?></td><td class="dataValue"><?= $object->warranty_date ? date('d.m.Y', strtotime($object->warranty_date)) : '' ?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_serviceinterval')?></td><td class="dataValue"><?= htmlspecialchars($object->serviceinterval)?> <?= $this->lang->line('months')?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_nextmaintenance')?></td><td class="dataValue"><span class="object_overview_highlight <?= $nextMaintenance['cc']?>"><?= $object->next_maintenance ? date('d.m.Y', strtotime($object->next_maintenance)) : '' ?></span></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_lastmaintenance')?></td><td class="dataValue"><?= $object->last_maintenance ? date('d.m.Y', strtotime($object->last_maintenance)) : '' ?></td></tr>
                </tbody>
            </table>
        </div>
        <div class="dialogColumn">
            <table>
                <tbody>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_suppliername')?></td><td class="dataValue"><?= $supplier != null ? htmlspecialchars($supplier->name) : ''?><br/><br/><?= $supplier != null ? nl2br(htmlspecialchars($supplier->address)) : ''?><br/><br/><?=  $supplier != null ? mailto($supplier->email, $supplier->email) : ''?><br/><?=  $supplier != null ? htmlspecialchars($supplier->phone) : ''?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_contact1')?></td><td class="dataValue"><?= $contact1 != null ? htmlspecialchars($contact1->name) : ''?><br/><br/><?= $contact1 != null ? nl2br(htmlspecialchars($contact1->address)) : ''?><br/><br/><?= $contact1 != null ? mailto($contact1->email, $contact1->email) : ''?><br/><?= $contact1 != null ? htmlspecialchars($contact1->phone) : ''?></td></tr>
                    <tr><td class="dataHeader"><?= $this->lang->line('objectlist_contact2')?></td><td class="dataValue"><?= $contact2 != null ? htmlspecialchars($contact2->name) : ''?><br/><br/><?= $contact2 != null ? nl2br(htmlspecialchars($contact2->address)) : ''?><br/><br/><?= $contact2 != null ?  mailto($contact2->email, $contact2->email) : ''?><br/><?= $contact2 != null ? htmlspecialchars($contact2->phone) : ''?></td></tr>
                    <tr class="table_separator"></tr>
                    <?php if($object->image_fname != ''):?>
                        <tr>
                            <td class="dataValue" colspan=2>
                                <div class="objectimg" href="<?= site_url('items/uploads/objectimages/' . $object->image_fname)?>" >
                                    <img src="<?= site_url('items/uploads/objectimages/thumb_' . $object->image_fname)?>" />
                                </div>
                            </td>
                        </tr>
                    <?php endif;?>
                                        
                </tbody>
            </table>
        </div>
    </div>    
    
