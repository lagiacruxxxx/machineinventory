
<div id="objecttable">
    <table class="ui-corner-all">
        <thead>
            <tr class="ui-state-default">
                <th class="listSortable" sortField="object_id"><?= $this->lang->line('objectlist_objectid')?><?php if($sortfield == 'object_id'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?> "></div><?php endif;?></th>
                <th class="listSortable" sortField="name"><?= $this->lang->line('objectlist_objectname')?><?php if($sortfield == 'name'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="next_maintenance"><?= $this->lang->line('objectlist_nextmaintenance')?><?php if($sortfield == 'next'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="serial_number"><?= $this->lang->line('objectlist_serialnumber')?><?php if($sortfield == 'serial_number'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                <th class="listSortable" sortField="location"><?= $this->lang->line('objectlist_location')?><?php if($sortfield == 'location'):?><div class="listSortableIcon ui-icon <?= $sortdirection . ' ' . $sorticon?>"></div><?php endif;?></th>
                </tr>
        </thead>
        <tbody>
            <?php foreach($objects as $object):?>
                <tr class="objectrow <?= $object['nextMaintenance']['cc']?>" object_id=<?= $object['id']?>>
                    <td><?= $object['object_id']?></td>
                    <td><?= $object['name']?></td>
                    <td><?= $object['nextM']?></td>
                    <td><?= $object['serial_number']?></td>
                    <td><?= $object['location']?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>