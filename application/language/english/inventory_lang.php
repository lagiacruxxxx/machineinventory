<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***********************************************************************************************
 * VARIABLES
 ***********************************************************************************************/


/***********************************************************************************************
 * STRINGS
 ***********************************************************************************************/
$lang['menu_objectlist'] = "Objektliste";
$lang['menu_calendarview'] = "Kalender";
$lang['menu_users'] = "Benutzer";
$lang['menu_contacts'] = "Kontakte";
$lang['menu_contracts'] = "Wartungsverträge";
$lang['menu_mandant'] = "Mandanten";
$lang['menu_equipmentgroups'] = "Gerätegruppen";
$lang['menu_costcenters'] = "Kostenstellen";

$lang['contacts_header'] = "Kontakte";
$lang['contacts_name'] = "Ansprechpartner";
$lang['contacts_adress'] = "Addresse";
$lang['contacts_email'] = "E-Mail";
$lang['contacts_phone'] = "Telefon";
$lang['contacts_issupplier'] = "ist Lieferant";
$lang['contacts_iscompany'] = "ist Firma";
$lang['contacts_select'] = "Kontakt auswählen ...";

$lang['costcenter_header'] = "Kostenstellen";
$lang['costcenter_name'] = "Kostenstelle";
$lang['costcenter_select'] = "Kostenstelle auswählen ...";

$lang['equipmentgroup_header'] = "Gerätegruppe";
$lang['equipmentgroup_name'] = "Gerätegruppenname";
$lang['equipmentgroup_select'] = "Gerätegruppe auswählen ...";

$lang['mandant_header'] = "Mandanten";
$lang['mandant_name'] = "Mandant";
$lang['mandant_select'] = "Mandant auswählen ...";

$lang['objectlist_objectid'] = "Objektnummer";
$lang['objectlist_inventorynumber'] = "Inventarnummer";
$lang['objectlist_equipmentgroup'] = "Gerätegruppe";
$lang['objectlist_objectname'] = "Objektname";
$lang['objectlist_manufacturer'] = "Hersteller";
$lang['objectlist_description'] = "Beschreibung";
$lang['objectlist_location'] = "Ort";
$lang['objectlist_purchasedate'] = "Kaufdatum";
$lang['objectlist_installationdate'] = "Installationsdatum";
$lang['objectlist_warrantydate'] = "Garantie Ende";
$lang['objectlist_purchaseprice'] = "Kaufpreis";
$lang['objectlist_serialnumber'] = "Seriennummer";
$lang['objectlist_manufacturerserialnumber'] = "Hersteller Seriennummer";
$lang['objectlist_client'] = "Mandant";
$lang['objectlist_anschaffendeKST'] = "anschaffende KST";
$lang['objectlist_gebuchteKST'] = "gebuchte KST";
$lang['objectlist_serviceinterval'] = "Service Intervall";
$lang['objectlist_nextmaintenance'] = "Nächste Wartung";
$lang['objectlist_lastmaintenance'] = "Letzte Wartung";
$lang['objectlist_GLTnumber'] = "GLT Nummer";
$lang['objectlist_suppliername'] = "Lieferant";
$lang['objectlist_contact1'] = "Ansprechpartner 1";
$lang['objectlist_contact2'] = "Ansprechpartner 2";
$lang['objectlist_status'] = "Status";
$lang['objectlist_retiredDate'] = "ausgeschieden am";
$lang['objectlist_comment'] = "Kommentar";
$lang['objectlist_contractid'] = "Wartungsvertrag";
$lang['objectlist_header'] = "Objekte";

$lang['objectdetail_tab_edit'] = "Daten";
$lang['objectdetail_tab_history'] = "History";
$lang['objectdetail_tab_files'] = "Dateien";

$lang['objectdetail_history_save'] = "Speichern";
$lang['objectdetail_history_comment'] = "Kommentarfeld";
$lang['objectdetail_files_comment'] = "Beschreibung";

$lang['objectdetail_tabheader_history'] = "Fügen sie hier neue Einträge in die Objekthistorie hinzu";
$lang['objectdetail_tabheader_files'] = "Laden sie hier neue Dateien hoch";
$lang['objectdetail_files_textdesc'] = "Beschreibung:";
$lang['objectdetail_history_commentdesc'] = "Kommentar:";
