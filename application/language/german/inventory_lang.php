<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***********************************************************************************************
 * VARIABLES
 ***********************************************************************************************/


/***********************************************************************************************
 * STRINGS
 ***********************************************************************************************/
$lang['months'] = "Monat(e)";
$lang['cancel'] = "Abbrechen";
$lang['success_update'] = "Speichern erfolgreich";
$lang['success_delete'] = "Erfolgreich gelöscht";
$lang['logged_in_as'] = "Angemeldet als: ";
$lang['logout'] = "Abmelden";
$lang['profile'] = "Profil";
$lang['no'] = "Nein";
$lang['yes'] = "Ja";

$lang['menutitle'] = "CCRI Inventory";
$lang['notifications'] = "Meldungen";
$lang['notification_contractexpire'] = "Wartungsvertrag läuft aus: ";


$lang['error_update'] = "Fehler beim Speichern";
$lang['error_upload_empty'] = "Keine Datei ausgewählt";
$lang['error_insert'] = "Fehler beim anlegen";

$lang['menu_objectlist'] = "Objektliste";
$lang['menu_new_object'] = "Neues Objekt";
$lang['menu_clone_object'] = "Objekt klonen";
$lang['filter'] = "Filter";
$lang['filter_reset'] = "Zurücksetzen";
$lang['combobox_nofilter'] = "Kein Filter";

$lang['contacts_header'] = "Kontakte";
$lang['contacts_name'] = "Ansprechpartner";
$lang['contacts_adress'] = "Addresse";
$lang['contacts_email'] = "E-Mail";
$lang['contacts_phone'] = "Telefon";
$lang['contacts_issupplier'] = "ist Lieferant";
$lang['contacts_iscompany'] = "ist Firma";
$lang['contacts_select'] = "Kontakt auswählen ...";



$lang['object_title'] = "Objekt";
$lang['object_title_new'] = "Neues Objekt";
$lang['objectlist_objectid'] = "Objektnummer";
$lang['objectlist_inventorynumber'] = "Inventarnummer";
$lang['objectlist_equipmentgroup'] = "Gerätegruppe";
$lang['objectlist_objectname'] = "Objektname";
$lang['objectlist_manufacturer'] = "Hersteller";
$lang['objectlist_description'] = "Beschreibung";
$lang['objectlist_location'] = "Raum";
$lang['objectlist_purchasedate'] = "Kaufdatum";
$lang['objectlist_installationdate'] = "Installationsdatum";
$lang['objectlist_warrantydate'] = "Garantie Ende";
$lang['objectlist_purchaseprice'] = "Kaufpreis (in EUR)";
$lang['objectlist_serialnumber'] = "Seriennummer";
$lang['objectlist_manufacturerserialnumber'] = "Hersteller Seriennummer";
$lang['objectlist_client'] = "Mandant";
$lang['objectlist_anschaffendeKST'] = "anschaffende KST";
$lang['objectlist_gebuchteKST'] = "gebuchte KST";
$lang['objectlist_serviceinterval'] = "Service Intervall";
$lang['objectlist_nextmaintenance'] = "Nächste Wartung";
$lang['objectlist_lastmaintenance'] = "Letzte Wartung";
$lang['objectlist_GLTnumber'] = "GLT Nummer";
$lang['objectlist_suppliername'] = "Lieferant";
$lang['objectlist_contact1'] = "Interner Ansprechpartner";
$lang['objectlist_contact2'] = "Externer Ansprechpartner";
$lang['objectlist_status'] = "Status";
$lang['objectlist_retiredDate'] = "ausgeschieden am";
$lang['objectlist_comment'] = "Kommentar";
$lang['objectlist_contractid'] = "Wartungsvertrag";
$lang['objectlist_header'] = "Objekte";
$lang['objectlist_image'] = "";
$lang['objectlist_upload_new_img'] = "Neues Bild hochladen";

$lang['object_servicecomment'] = "Ein Service wurde durchgeführt!";

$lang['object_service_text'] = "Wollen sie ein Service durchführen?";
$lang['object_service_btn'] = "Service";

$lang['objectaction_delete'] = "Löschen";
$lang['objectaction_clone'] = "Klonen";
$lang['objectaction_report'] = "Report";
$lang['objectaction_service'] = "Service";

$lang['objectdetail_tab_overview'] = "Übersicht";
$lang['objectdetail_tab_edit'] = "Bearbeiten";
$lang['objectdetail_tab_history'] = "History";
$lang['objectdetail_tab_files'] = "Dateien";

$lang['objectdetail_edit_save'] = "Speichern";
$lang['objectdetail_delete_text'] = "Sind sie sicher, dass sie dieses Objekt löschen wollen?";
$lang['objectdetail_delete_yes'] = "Ja";
$lang['objectdetail_delete_no'] = "Nein";


$lang['objectdetail_history_save'] = "Speichern";
$lang['objectdetail_history_comment'] = "Kommentarfeld";
$lang['objectdetail_files_comment'] = "Beschreibung";

$lang['objectdetail_tabheader_history'] = "Fügen sie hier neue Einträge in die Objekthistorie hinzu";
$lang['objectdetail_tabheader_files'] = "Laden sie hier neue Dateien hoch";
$lang['objectdetail_files_textdesc'] = "Dateibeschreibung";
$lang['objectdetail_history_commentdesc'] = "Kommentar";




$lang['contract_tab'] = "Wartungsverträge";
$lang['contract_select'] = "Wartungsvertrag auswählen ...";
$lang['contractlist_company'] = "Vertragspartner";
$lang['contractlist_maintenancenumber'] = "Wartungsnummer";
$lang['contractlist_serviceagreementnumber'] = "Service Vertrag Nummer";
$lang['contractlist_externalcontact'] = "Externer Ansprechpartner";
$lang['contractlist_internalcontact'] = "Interner Ansprechpartner";
$lang['contractlist_startdate'] = "Vertragsdatum";
$lang['contractlist_enddate'] = "Ablaufdatum";
$lang['contractlist_costs'] = "Kosten";
$lang['contractlist_comment'] = "Zusätzliche Information";
$lang['contractlist_header'] = "Wartungsverträge";
$lang['contractlist_autorenew'] = "Automatisch verlängern";
$lang['contractlist_renewinterval'] = "Vertragslaufzeit (in Monaten)";
$lang['contractlist_reminderTolerance'] = "Erinnerung bevor Ablauf (in Monaten)";
$lang['menu_new_contract'] = "Neuer Wartungsvertrag";
$lang['contract_title_new'] = "Neuer Wartungsvertrag";
$lang['contractdetail_delete_text'] = "Sind sie sicher, dass sie diesen Wartungsvertrag löschen wollen?";
$lang['contractdetail_tab_objects'] = "Objekte";
$lang['contractdetail_tab_objectadd'] = "Objekte hinzufügen";
$lang['contract_object_service'] = "Service";
$lang['contract_object_add'] = "Hinzufügen";
$lang['contract_objects_select'] = "Alle selektieren / deselektieren";
$lang['contract_addobject'] = "Die Objekte wurden hinzugefügt";

$lang['user_tab'] = "Benutzer";
$lang['user_title_new'] = "Neuer Benutzer";
$lang['userlist_username'] = "Benutzername";
$lang['userlist_firstname'] = "Vorname";
$lang['userlist_lastname'] = "Nachname";
$lang['userlist_email'] = "E-mail";
$lang['userlist_role'] = "Rechte";
$lang['userlist_password'] = "Passwort";
$lang['userlist_change_pw'] = "Passwort ändern";
$lang['userlist_confirm_pw'] = "Passwort wiederholen";
$lang['userdetail_tab_overview'] = "Übersicht";
$lang['userdetail_tab_edit'] = "Bearbeiten";
$lang['userdetail_edit_save'] = "Speichern";
$lang['menu_new_user'] = "Neuer Benutzer";
$lang['userdetail_delete_text'] = "Sind sie sicher, dass sie dieses Benutzer löschen wollen?";


$lang['contact_tab'] = "Kontakte";
$lang['contact_title_new'] = "Neuer Kontakt";
$lang['contactlist_name'] = "Name";
$lang['contactlist_address'] = "Adresse";
$lang['contactlist_email'] = "E-mail";
$lang['contactlist_phone'] = "Telefon";
$lang['contactlist_issupplier'] = "Lieferant";
$lang['contactdetail_tab_overview'] = "Übersicht";
$lang['contactdetail_tab_edit'] = "Bearbeiten";
$lang['contactdetail_edit_save'] = "Speichern";
$lang['menu_new_contact'] = "Neuer Kontakt";
$lang['contactdetail_delete_text'] = "Sind sie sicher, dass sie diesen Kontakt löschen wollen?";

$lang['equipmentgroup_tab'] = "Gerätegruppen";
$lang['equipmentgrouplist_name'] = "Name";
$lang['menu_new_equipmentgroup'] = "Neue Gerätegruppe";
$lang['equipmentgroup_title_new'] = "Neue Gerätegruppe";
$lang['equipmentgroup_delete_text'] = "Sind sie sicher, dass sie diesen Gerätegruppe löschen wollen?";
$lang['equipmentgroup_header'] = "Gerätegruppe";
$lang['equipmentgroup_name'] = "Gerätegruppenname";
$lang['equipmentgroup_select'] = "Gerätegruppe auswählen ...";


$lang['client_tab'] = "Mandanten";
$lang['clientlist_name'] = "Name";
$lang['menu_new_client'] = "Neuer Mandant";
$lang['client_title_new'] = "Neuer Mandant";
$lang['client_delete_text'] = "Sind sie sicher, dass sie diesen Mandant löschen wollen?";
$lang['mandant_header'] = "Mandanten";
$lang['mandant_name'] = "Mandant";
$lang['mandant_select'] = "Mandant auswählen ...";

$lang['costcenter_tab'] = "Kostenstellen";
$lang['costcenter_header'] = "Kostenstellen";
$lang['costcenterlist_name'] = "Kostenstelle";
$lang['costcenter_select'] = "Kostenstelle auswählen ...";
$lang['costcenter_title_new'] = "Neue Kostenstelle";
$lang['menu_new_costcenter'] = 'Neue Kostenstelle';
$lang['costcenterdetail_delete_text'] = "Sind sie sicher, dass sie diese Kostenstelle löschen wollen?";



$lang['calendar_tab'] = "Kalender";