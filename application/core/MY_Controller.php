<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{
    	
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Europe/Vienna');
        
        $this->load->helper('besc_helper');
    }  

    public function connectDB()
    {
        if(strpos(site_url(), 'localhost') === false)
        {
            if(strpos(site_url(), 'fileserver') === false)
            {
                $this->load->database('productive');
            }
            else 
                $this->load->database('testing');
        }
        else
            $this->load->database('development');
    }   
    
    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->sess_destroy();
        $this->load->view('authentication/logout', array());
    }
    
    public function logged_in()
    {
        return isset($_SESSION['user_id']);
    }
    
    function my_hash($hash)
    {
        require_once(APPPATH.'libraries/PasswordHash.php');
        $hasher = new PasswordHash(8, false);
        return $hasher->HashPassword($hash);
    }
    
    function check_hash($hash, $stored_hash)
    {
        require_once(APPPATH.'libraries/PasswordHash.php');
        $hasher = new PasswordHash(8, false);
        return $hasher->CheckPassword($hash, $stored_hash);
    }    
}
