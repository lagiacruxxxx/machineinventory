<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends MY_Controller 
{
	public $user;
	
    function __construct()
    {
        parent::__construct();
        
		if(!$this->logged_in())
			redirect('authentication/showLogin');
		
		$this->config->load('inventory');
		$this->lang->load('inventory', 'german');
		
		$this->load->model('Authentication_model', 'am');
		$this->user = $this->am->getUserdataByID($this->session->userdata('user_id'))->row();
		$this->load->model('Inventory_model', 'im');
		
		require_once (APPPATH . 'libraries/MyCore.php');
		require_once (APPPATH . 'libraries/MyObject.php');
		require_once (APPPATH . 'libraries/MyContract.php');
		require_once (APPPATH . 'libraries/MyUser.php');
		require_once (APPPATH . 'libraries/MyClient.php');
		require_once (APPPATH . 'libraries/MyEquipmentgroup.php');
		require_once (APPPATH . 'libraries/MyCostcenter.php');
		require_once (APPPATH . 'libraries/MyContact.php');
    }  
    

	public function index()
	{
	    $data['objectlist'] = MyObject::objectList();
	    $data['objectmenu'] = MyObject::objectMenu();
	    $data['objectfilter'] = MyObject::filterView();
	    $data['objectservice'] = MyObject::serviceView();

	    $data['equipmentgrouplist'] = MyEquipmentgroup::equipmentgroupList();
	    $data['equipmentgroupfilter'] = MyEquipmentgroup::filterView();
	    $data['equipmentgroupmenu'] = MyEquipmentgroup::equipmentgroupMenu();	    

	    $data['costcenterlist'] = MyCostcenter::costcenterList();
	    $data['costcenterfilter'] = MyCostcenter::filterView();
	    $data['costcentermenu'] = MyCostcenter::costcenterMenu();

	    $data['contactlist'] = MyContact::contactList();
	    $data['contactfilter'] = MyContact::filterView();
	    $data['contactmenu'] = MyContact::contactMenu();
	    
	    $data['clientlist'] = MyClient::clientList();
	    $data['clientfilter'] = MyClient::filterView();
	    $data['clientmenu'] = MyClient::clientMenu();
	    
	    $data['userlist'] = MyUser::userList();
	    $data['userfilter'] = MyUser::filterView();
	    $data['usermenu'] = MyUser::userMenu();
	    
	    $data['contractlist'] = MyContract::contractList();
	    $data['contractfilter'] = MyContract::filterView();
	    $data['contractmenu'] = MyContract::contractMenu();
	    $data['contractservice'] = MyContract::serviceView();
	    
	    $data['isAdmin'] = MyCore::hasPrivilege(USER_PRIV_ADMIN);
	    $data['allowEdit'] = MyCore::hasPrivilege(USER_PRIV_EDIT);
	    $data['logged_in_user'] = $this->user->username;
	    
	    $data['notifications'] = MyCore::getNotifications();
 
	    $this->load->view('inventory/head');
	    $this->load->view('inventory/home', $data);
	    $this->load->view('inventory/footer');
	}
	
	public function getAppData()
	{
	    echo json_encode(array(
	        'lang' => $this->lang->language,
	        'user' => $this->user->id,
	    ));
	}
	
	public function updateFilters()
	{
	    MyCore::updateFilters();
	}
	
	public function updateSorting()
	{
	    MyCore::updateSorting();
	}	
	
	public function dismissNotification($id)
	{
	    MyCore::dismissNotification($id);
	}
	
	public function profile()
	{
	    $data = array(
            'user' => $this->user,   
	    );
	    
	    echo json_encode(
	        array(
	            'success' => true,
	            'html' => $this->load->view('inventory/profile', $data, true),
	        )
	    );
	}
	
	/***************************************************************************************************************
	 * OBJECT
	 *****************************************************************************************************************/
	public function getObject($objectId)
	{
	    $object = new MyObject($objectId);
	    $object->view();
	}
	
	public function newObject()
	{
	    if(!$this->input->get('objectId'))
            MyObject::addView();
	    else
	    {
	        $object = new MyObject($this->input->get('objectId'));
	        $object->clone_();
	    }    
	}
	
	public function updateObject()
	{
	    $object = new MyObject($this->input->post('id'));
	    $object->update($this->input->post());
	}
	
	public function deleteObject()
	{
	    $object = new MyObject($this->input->post('id'));
	    $object->delete();
	}
	
    public function insertObject()
    {
        MyObject::insert($this->input->post());
    }
	
	public function refreshObjectList()
	{
	    echo json_encode(array(
	        'success' => true,
	        'list' => MyObject::objectList(),
	    ));
	}
	
	public function insertObjectHistory()
	{
	    $object = new MyObject($this->input->post('object_id'));
	    $object->insertHistory($this->input->post());	    
	}
	
	public function deleteObjectHistoryItem()
	{
	    $object = new MyObject($this->input->post('object_id'));
	    $object->DeleteObjectHistoryItem($this->input->post('id'));
	}   
	
	public function insertObjectFile()
	{
	    $object = new MyObject($this->input->post('object_id'));
	    $object->insertFile($this->input->post(), $_FILES);
	}
	
    public function serviceObject()
    {
        $object = new MyObject($this->input->post('object_id'));
	    $object->service($this->input->post('comment'));
    }
    	
    
    public function calendarEvents()
    {
        $start = $this->input->get('start');
        $end = $this->input->get('end');

        MyObject::getServiceObjects($start, $end);
    }

    public function updateObjectImage()
    {
        $object = new MyObject($this->input->post('object_id'));
        $object->updateImage($_FILES);
    }

    

    /***************************************************************************************************************
     * EQUIPMENTGROUP
     *****************************************************************************************************************/
    
    public function refreshEquipmentgroupList()
    {
        echo json_encode(array(
            'success' => true,
            'list' => MyEquipmentgroup::equipmentgroupList(),
        ));
    }
    
    public function getEquipmentgroup($id)
    {
        $equipmentgroup = new MyEquipmentgroup($id);
        $equipmentgroup->view();
    }
    
    public function newEquipmentgroup()
    {
        if(!$this->input->get('equipmentgroupId'))
            MyEquipmentgroup::addView();
        else
        {
            $equipmentgroup = new MyEquipmentgroup($this->input->get('equipmentgroupId'));
            $equipmentgroup->clone_();
        }
    }
    
    public function updateEquipmentgroup()
    {
        $equipmentgroup = new MyEquipmentgroup($this->input->post('id'));
        $equipmentgroup->update($this->input->post());
    }
    
    public function deleteEquipmentgroup()
    {
        $equipmentgroup = new MyEquipmentgroup($this->input->post('id'));
        $equipmentgroup->delete();
    }
    
    public function insertEquipmentgroup()
    {
        MyEquipmentgroup::insert($this->input->post());
    }
    
    
    
    
    /***************************************************************************************************************
     * COSTCENTER
     *****************************************************************************************************************/
    
    public function refreshCostcenterList()
    {
        echo json_encode(array(
            'success' => true,
            'list' => MyCostcenter::costcenterList(),
        ));
    }
    
    public function getCostcenter($id)
    {
        $costcenter = new MyCostcenter($id);
        $costcenter->view();
    }
    
    public function newCostcenter()
    {
        if(!$this->input->get('costcenterId'))
            MyCostcenter::addView();
        else
        {
            $costcenter = new MyCostcenter($this->input->get('costcenterId'));
            $costcenter->clone_();
        }
    }
    
    public function updateCostcenter()
    {
        $costcenter = new MyCostcenter($this->input->post('id'));
        $costcenter->update($this->input->post());
    }
    
    public function deleteCostcenter()
    {
        $costcenter = new MyCostcenter($this->input->post('id'));
        $costcenter->delete();
    }
    
    public function insertCostcenter()
    {
        MyCostcenter::insert($this->input->post());
    }
    
    
    /***************************************************************************************************************
     * CONTACTS
     *****************************************************************************************************************/
    
    public function refreshContactList()
    {
        echo json_encode(array(
            'success' => true,
            'list' => MyContact::contactList(),
        ));
    }
    
    public function getContact($id)
    {
        $contact = new MyContact($id);
        $contact->view();
    }
    
    public function newContact()
    {
        if(!$this->input->get('contactId'))
            MyContact::addView();
        else
        {
            $contact = new MyContact($this->input->get('contactId'));
            $contact->clone_();
        }
    }
    
    public function updateContact()
    {
        $contact = new MyContact($this->input->post('id'));
        $contact->update($this->input->post());
    }
    
    public function deleteContact()
    {
        $contact = new MyContact($this->input->post('id'));
        $contact->delete();
    }
    
    public function insertContact()
    {
        MyContact::insert($this->input->post());
    }
        
    
    
    
    
    /***************************************************************************************************************
     * CLIENT
     *****************************************************************************************************************/
    
    public function refreshClientList()
    {
        echo json_encode(array(
            'success' => true,
            'list' => MyClient::clientList(),
        ));
    }
    
    public function getClient($clientId)
    {
        $client = new MyClient($clientId);
        $client->view();
    }
    
    public function newClient()
    {
        if(!$this->input->get('clientId'))
            MyClient::addView();
        else
        {
            $client = new MyClient($this->input->get('clientId'));
            $client->clone_();
        }
    }
    
    public function updateClient()
    {
        $client = new MyClient($this->input->post('id'));
        $client->update($this->input->post());
    }
    
    public function deleteClient()
    {
        $client = new MyClient($this->input->post('id'));
        $client->delete();
    }
    
    public function insertClient()
    {
        MyClient::insert($this->input->post());
    }    
    
    
    /***************************************************************************************************************
     * USER
     *****************************************************************************************************************/
    
    public function refreshUserList()
    {
        echo json_encode(array(
            'success' => true,
            'list' => MyUser::userList(),
        ));
    }
    
    public function getUser($userId)
    {
        $user = new MyUser($userId);
        $user->view();
    }
    
    public function newUser()
    {
        if(!$this->input->get('userId'))
            MyUser::addView();
        else
        {
            $user = new MyUser($this->input->get('userId'));
            $user->clone_();
        }
    }
    
    public function updateUser()
    {
        $user = new MyUser($this->input->post('id'));
        $user->update($this->input->post());
    }
    
    public function deleteUser()
    {
        $user = new MyUser($this->input->post('id'));
        $user->delete();
    }
    
    public function insertUser()
    {
        MyUser::insert($this->input->post());
    }
    
    
    
	
	/***************************************************************************************************************
	 * CONTRACT
	 *****************************************************************************************************************/
    public function refreshContractList()
    {
        echo json_encode(array(
            'success' => true,
            'list' => MyContract::contractList(),
        ));
    }
    
    public function getContract($contractId)
    {
        $contract = new MyContract($contractId);
        $contract->view();
    }
    
    public function newContract()
    {
        if(!$this->input->get('contractId'))
            MyContract::addView();
        else
        {
            $contract = new MyContract($this->input->get('contractId'));
            $contract->clone_();
        }
    }
    
    public function updateContract()
    {
        $contract = new MyContract($this->input->post('id'));
        $contract->update($this->input->post());
    }
    
    public function deleteContract()
    {
        $contract = new MyContract($this->input->post('id'));
        $contract->delete();
    }
    
    public function insertContract()
    {
        MyContract::insert($this->input->post());
    }
    
    public function insertContractFile()
    {
        $contract = new MyContract($this->input->post('contract_id'));
        $contract->insertFile($this->input->post(), $_FILES);
    }
    
    public function serviceContractObjects()
    {
        $contract = new MyContract($this->input->post('contractId'));
        $contract->serviceObjects();
    }	

    public function addContractObjects()
    {
        $contract = new MyContract($this->input->post('contractId'));
        $contract->addObjects();
    }
    
    public function updateContractObjectFilters()
    {
        $contract = new MyContract($this->input->post('contractId'));
        $contract->refreshObjectAdd();
    }
}