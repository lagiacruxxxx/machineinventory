<?php

// error_reporting(E_NOTICE);
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Authentication extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        
        $this->load->model('Authentication_model', 'am');
    }

    public function showLogin()
    {
        $data = array();
        $this->load->view('authentication/login', $data);
    }

    public function checkLogin()
    {
        if ($this->logged_in()) 
        {
            $result = array(
                'success' => true,
                'message' => 'logged_id',
                'menu' => $this->lang->line('header_menu_logout')
            );
        } 
        else 
        {
            $result = array(
                'success' => false,
                'message' => 'not_logged_in',
                'menu' => $this->lang->line('header_menu_login')
            );
        }
        echo json_encode($result);
    }

    public function loginUser()
    {
        $username = purify($_POST['username']);
        $pword = purify($_POST['pword']);
        if ($this->am->getPW($username)->num_rows() > 0) 
        {
            $pwcheck = check_hash($pword, $this->am->getPW($username)->row()->pword);
            if ($pwcheck) 
            {
                $userId = $this->am->getUserdataByUsername($username)->row()->id;
                $this->session->set_userdata('user_id', $userId);
                require_once (APPPATH . 'libraries/MyContract.php');
                MyContract::serviceContracts();
                redirect('inventory');
            } 
            else 
            {
                $data['errormessage'] = "Password incorrect";
                $this->load->view('authentication/login', $data);
            }
        } 
        else 
        {
            $data['errormessage'] = "User not found";
            $this->load->view('authentication/login', $data);
        }
    }
    
    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->sess_destroy();
        header( "refresh:2;url=" . site_url() );
        $this->load->view('authentication/logout', array());
    }

}

/* End of file authentication.php */
/* Location: ./application/controllers/authentication.php */