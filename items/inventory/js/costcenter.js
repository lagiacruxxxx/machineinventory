$(document).ready(function()
{
	toggleCostcenterMenuListeners(true);
	toggleCostcenterTableListeners(true);
});



function updateCostcenterTable()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/refreshCostcenterList/',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleCostcenterTableListeners(false);
				$('#costcentertable').replaceWith(ret.list);
				toggleCostcenterTableListeners(true);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}


function getCostcenter(costcenterId)
{
	if( costcenterId == null || (costcenterId != null && !isDialogOpen('costcenter', costcenterId)) )
	{	
		$.ajax(
		{
			url: rootUrl + 'Inventory/getCostcenter/' + costcenterId,
			data: 
			{
			},
			method: 'GET',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					var dID = contentDialog(ret.data.html, ret.data.title);
					dialogContainer[dID] = ['costcenter', ret.data.costcenterId, 'open'];
					var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
					fixCostcenterDialog(dialog);
					fixDialogCss(dialog, 142);
					toggleCostcenterDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			}
		});
	}
}


function newCostcenter(costcenterId)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/newCostcenter/',
		data: 
		{
			'costcenterId': costcenterId,
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.data.html, ret.data.title);
				dialogContainer[dID] = ['costcenter', ret.data.costcenterId, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixDialogCss(dialog, 0);
				fixCostcenterDialog(dialog);
				
				toggleCostcenterDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		}
	});
}


function updateCostcenter(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateCostcenter/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.costcenterDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				updateCostcenterTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteCostcenter(dialog)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteCostcenter/',
		data: {
			'id': dialogContainer[dialog.attr('dialogId')][1],
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.dialog('close');
				updateCostcenterTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertCostcenter(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertCostcenter/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.dialog('close');
				updateCostcenterTable();
				getCostcenter(ret.new_id);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}



function fixCostcenterDialog(dialog)
{
	dialog.find('.costcenterDetailTabs').tabs(
	{
		'active': 0,
	});
	
	dialog.find('.datepicker').datepicker(
	{
		'dateFormat': 'dd.mm.yy',
		'changeMonth': true,
		'changeYear': true,
		'altFormat': 'yy-mm-dd',
	});

	dialog.find('.combobox').combobox();
	
	dialog.find('input[type="submit"]').button();
	dialog.find('.dialogAction').button();
}



function toggleCostcenterDetailListeners(toggle, dialog)
{
	if(toggle)
	{
		dialog.find('.costcenterEditForm').on('submit', function(e)
		{
			e.preventDefault();
			updateCostcenter(dialog, $(this));
		});
		
		dialog.find('.costcenterAddForm').on('submit', function(e)
		{
			e.preventDefault();
			insertCostcenter(dialog, $(this));
		});
		
		dialog.find('.costcenterDelete').on('click', function()
		{
			confirmationDialog('info', langStrings.costcenterdetail_delete_text, [langStrings.objectdetail_delete_yes, 'deleteCostcenter', dialog], [langStrings.objectdetail_delete_no, null, null]);
		});
		
		dialog.find('.costcenterClone').on('click', function()
		{
			newCostcenter(dialogContainer[dialog.attr('dialogid')][1]);
			dialog.dialog('close');
		});
		
	}
	else
	{
		dialog.find('.costcenterEditForm').off('submit');
		dialog.find('.costcenterAddForm').off('submit');
		dialog.find('.costcenterDelete').off('click');
		dialog.find('.costcenterClone').off('click');
	}
}


function toggleCostcenterMenuListeners(toggle)
{
	if(toggle)
	{
		$('.costcenter_new').on('click', function()
		{
			newCostcenter(null);
		});
		
		$('.costcenter_filter').on('click', function()
		{
			costcenterFilter();
		});
	}
	else
	{
		$('.costcenter_new').off('click');
		$('.costcenter_filter').off('click');
	}
}

function toggleCostcenterTableListeners(toggle)
{
	if(toggle)
	{
		$('#costcenterTab').find('.costcenterrow').on('dblclick', function()
		{
			activeCostcenterrow = $(this).attr('costcenter_id');
			getCostcenter(activeCostcenterrow);
		});
		
		$('#costcenterTab').find('.costcenterrow').on('touchstart', function()
		{
			activeCostcenterrow = $(this).attr('costcenter_id');
			getCostcenter(activeCostcenterrow);
		});
				
		
		$('#costcenterTab').find('.listSortable').on('click', function()
		{
			updateCostcenterSorting($(this));
		});
		
		$('#costcenterTab').find('.costcenterrow').on('mouseenter mouseleave', function()
		{
			$(this).toggleClass('filterActive');
		});

	}
	else
	{
		$('#costcenterTab').find('.costcenterrow').off('dblclick');
		$('#costcenterTab').find('.costcenterrow').off('touchstart');
		$('#costcenterTab').find('.costcenterrow').off('mouseenter mouseleave');
		$('#costcenterTab').find('.listSortable').off('click');
	}
}


function costcenterFilter()
{
	var dialogId = filterDialog($('#costcenter_filter'), 'Filter ' + langStrings.costcenter_tab);
	dialogContainer[dialogId] = ['costcenterfilter', dialogId, 'open'];
	
	
	$('#costcenterFilterForm').on('submit', function(e)
	{
		e.preventDefault();
		updateCostcenterFilters(false);
	});
	
	$('#costcenterFilterForm').on('reset', function(e)
	{
		updateCostcenterFilters(true);
	});
}


function updateCostcenterFilters(reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		$('#costcenterFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		$('#costcenterFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		if(i > 0)
			$('.costcenter_filter').addClass('filterActive');
		else
			$('.costcenter_filter').removeClass('filterActive');
	}
	else
		$('.costcenter_filter').removeClass('filterActive');
	
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateFilters/',
		data: {'filters': filters, 'tablename': 'costcenter'},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateCostcenterTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			$('#costcenter_filter').dialog( "close" );
			progressOverlay(false);
		},
	});	
}

function updateCostcenterSorting(th)
{
	var sortfield = $('#costcentertable').find('.listSortableIcon').parent().attr('sortfield')
	var sortdirection = $('#costcentertable').find('.listSortableIcon').hasClass('asc') ? 'asc' : 'desc';
	
	if(th.attr('sortfield') == sortfield)
	{
		if(sortdirection == 'asc')
			sortdirection = 'desc';
		else
			sortdirection = 'asc';
	}
	else
	{
		sortfield = th.attr('sortfield');
		sortdirection = 'asc';
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateSorting/',
		data: {'tablename': 'costcenter', 'sortfield': sortfield, 'sortdirection': sortdirection},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateCostcenterTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}