$(document).ready(function()
{
	igniteCalendar();
	toggleObjectMenuListeners(true);
	toggleObjectTableListeners(true);
});


function igniteCalendar()
{
	$('#calendar').fullCalendar(
	{
		'lang': 'de',
		'contentHeight': $('#calendarTab').height()- 90,
		'events': rootUrl + 'Inventory/calendarEvents',
		'eventClick': function(calEvent, jsEvent, view) 
		{
			getObject(calEvent.id);
	    },
	    'loading': function(isLoading, view)
	    {
	    	progressOverlay(isLoading);
	    },
	    'header': 
    	{
	    	'left': 'prevYear,nextYear',
	    	'center': 'title',
	    	'right': 'today prev,next',
    	},
    	'theme': true,
    	'fixedWeekCount': true,
    	'themeButtonIcons':
    	{
    		'prev': '',
    		'next': '',
    		'prevYear': '',
    		'nextYear': '',
    	}
    	
	});
}

function updateObjectTable()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/refreshObjectList/',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleObjectTableListeners(false);
				$('#objecttable').replaceWith(ret.list);
				toggleObjectTableListeners(true);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}


function getObject(objectId)
{
	if( objectId == null || (objectId != null && !isDialogOpen('object', objectId)) )
	{	
		$.ajax(
		{
			url: rootUrl + 'Inventory/getObject/' + objectId,
			data: 
			{
			},
			method: 'GET',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					var dID = contentDialog(ret.data.html, ret.data.title);
					dialogContainer[dID] = ['object', ret.data.objectId, 'open'];
					var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
					fixObjectDialog(dialog);
					fixDialogCss(dialog, 142);
					toggleObjectDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			}
		});
	}
}


function newObject(objectId)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/newObject/',
		data: 
		{
			'objectId': objectId,
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.data.html, ret.data.title);
				dialogContainer[dID] = ['object', ret.data.objectId, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixDialogCss(dialog, 0);
				fixObjectDialog(dialog);
				
				toggleObjectDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		}
	});
}


function updateObject(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateObject/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.objectDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				updateObjectTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteObject(dialog)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteObject/',
		data: {
			'id': dialogContainer[dialog.attr('dialogId')][1],
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.dialog('close');
				updateObjectTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertObject(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertObject/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.dialog('close');
				updateObjectTable();
				getObject(ret.new_id);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertObjectHistory(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertObjectHistory/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.history_items').empty().append(ret.history);
				dialog.find('.history_edit_content').val('');
				dialog.find('.history_item_delete').off('click');
				dialog.find('.history_item_delete').on('click', function(e)
				{
					deleteObjectHistoryItem(dialog, $(this));
				});
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteObjectHistoryItem(dialog, del)
{
	
	
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteObjectHistoryItem/',
		data: {'id': del.attr('item_id'), 'object_id': dialog.find('.object_history_id_dummy').val()},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.history_items').empty().append(ret.history);
				
				dialog.find('.history_item_delete').off('click');
				dialog.find('.history_item_delete').on('click', function(e)
				{
					deleteObjectHistoryItem(dialog, $(this));
				});

			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}



function serviceObject(dialog, objectId)
{
	
	$('#object_service').dialog(
	{
		'title': langStrings.object_service_text,
		'width': wWidth * 0.4,
		'height': wHeight * 0.3,
		'position': {'my': 'center', 'at': 'center'},
		'modal': true,
		
		'focus': function(event, ui)
		{
			$(this).find('#service_comment').css({'height': $(this).height() - 90});
			$(this).find('.filterButton').button();
		},

	});
	
	$('#objectServiceForm').on('submit', function(e)
	{
		e.preventDefault();
		$.ajax(
		{
			url: rootUrl + 'Inventory/serviceObject/',
			data: {
				'object_id': objectId,
				'comment': $('#service_comment').val(),
				},
			method: 'POST',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					$('#object_service').dialog('close');
					getObject(objectId);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			},
		});
	});
	
	$('#objectServiceForm').on('reset', function(e)
	{
		$('#object_service').dialog('close');
	});



}

function insertObjectFile(dialog)
{
	var xhr = new XMLHttpRequest();		
	var fd = new FormData;
	var files = dialog.find('.fileupload_edit_file').prop('files');
	
	if(files.length != 1)
		messageDialog('error', dialog.find('.fileupload_edit_file').attr('error_empty'));
	else
	{
		fd.append('data', files[0]);
		fd.append('filename', files[0].name);
		fd.append('filedescription', dialog.find('.objectFileDesc').val());
		fd.append('object_id', dialog.find('.objectFileId').val());
		
		xhr.addEventListener('load', function(e) 
		{
			var ret = $.parseJSON(this.responseText);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.objectFileDesc').val('');
				dialog.find('.fileupload_edit_file').replaceWith( dialog.find('.fileupload_edit_file').clone( true ) );
				dialog.find('.fileupload_items').empty().append(ret.files);
			}
			else
			{
				messageDialog('error', ret.message);
			}
	    });
		
		xhr.open('post', rootUrl + 'Inventory/insertObjectFile/');
		xhr.send(fd);
	}
	
}



function fixObjectDialog(dialog)
{
	dialog.find('.objectDetailTabs').tabs(
	{
		'active': 0,
	});
	
	dialog.find('.datepicker').datepicker(
	{
		'dateFormat': 'dd.mm.yy',
		'changeMonth': true,
		'changeYear': true,
		'altFormat': 'yy-mm-dd',
	});

	dialog.find('.combobox').combobox();
	
	dialog.find('input[type="submit"]').button();
	dialog.find('.dialogAction').button();
	dialog.find('.object_upload_img').button();
}


function toggleObjectDetailListeners(toggle, dialog)
{
	if(toggle)
	{
		dialog.find('.objectEditForm').on('submit', function(e)
		{
			e.preventDefault();
			updateObject(dialog, $(this));
		});
		
		dialog.find('.objectAddForm').on('submit', function(e)
		{
			e.preventDefault();
			insertObject(dialog, $(this));
		});
		
		dialog.find('.history_edit_form').on('submit', function(e)
		{
			e.preventDefault();
			insertObjectHistory(dialog, $(this));
		});
		
		dialog.find('.history_item_delete').on('click', function(e)
		{
			deleteObjectHistoryItem(dialog, $(this));
		});
		
		dialog.find('.fileupload_edit_form').on('submit', function(e)
		{
			e.preventDefault();
			insertObjectFile(dialog, $(this));
		});		
		
		dialog.find('.fileupload_edit_file').on('click', function(e)
		{
			if($(this).prop('files').length > 0)
			{
				$(this).wrap('<form>').closest('form').get(0).reset();
				$(this).unwrap();
				e.preventDefault();
				$(this).click();
			}
			progressOverlay(true);
		});
		
		dialog.find('.fileupload_edit_file').on('change', function()
		{
			progressOverlay(false);
		});
		
		dialog.find('.objectDelete').on('click', function()
		{
			confirmationDialog('info', langStrings.objectdetail_delete_text, [langStrings.objectdetail_delete_yes, 'deleteObject', dialog], [langStrings.objectdetail_delete_no, null, null]);
		});
		
		dialog.find('.objectClone').on('click', function()
		{
			newObject(dialogContainer[dialog.attr('dialogid')][1]);
			dialog.dialog('close');
		});

		dialog.find('.objectService').on('click', function()
		{
			serviceObject(dialog, dialogContainer[dialog.attr('dialogid')][1]);
		});
		
		dialog.find('.objectimg').on('click', function()
		{
			dialog.find('.objectimg').fancybox(
			{
				prevEffect		: 'none',
				nextEffect		: 'none',
				closeBtn		: true,
			});
		});
		
		dialog.find('.object_upload_img').on('click', function()
		{
			if($(this).parent().find('.object_upload_img_file').prop('files').length > 0)
			{
				$(this).parent().find('.object_upload_img_file').wrap('<form>').closest('form').get(0).reset();
				$(this).parent().find('.object_upload_img_file').unwrap();
				e.preventDefault();
				$(this).parent().find('.object_upload_img_file').click();
			}
			$(this).parent().find('.object_upload_img_file').click();
		});
		
		dialog.find('.object_upload_img_file').on('change', function()
		{
			progressOverlay(true);
			updateObjectImage(dialog);
		});
		
	}
	else
	{
		dialog.find('.objectEditForm').off('submit');
		dialog.find('.objectAddForm').off('submit');
		dialog.find('.history_edit_form').off('submit');
		dialog.find('.fileupload_edit_form').off('submit');
		dialog.find('.fileupload_edit_file').off('change');
		dialog.find('.fileupload_edit_file').off('click');
		dialog.find('.objectDelete').off('click');
		dialog.find('.objectClone').off('click');
		dialog.find('.objectimg').off('click');
	}
}


function updateObjectImage(dialog)
{
	var xhr = new XMLHttpRequest();		
	var fd = new FormData;
	var files = dialog.find('.object_upload_img_file').prop('files');
	
	if(files.length != 1)
		messageDialog('error', dialog.find('.fileupload_edit_file').attr('error_empty'));
	else
	{
		fd.append('data', files[0]);
		fd.append('filename', files[0].name);
		fd.append('object_id', dialog.find('.objectFileId').val());
		
		xhr.addEventListener('load', function(e) 
		{
			var ret = $.parseJSON(this.responseText);
			progressOverlay(false);
			
			if(ret.success)
			{
				dialog.find('.objectDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				dialog.find('.objectDetailTabs').tabs({ active: 0 });
			}
			else
			{
				messageDialog('error', ret.message);
			}
	    });
		
		xhr.open('post', rootUrl + 'Inventory/updateObjectImage/');
		xhr.send(fd);
	}
	
}

function toggleObjectMenuListeners(toggle)
{
	if(toggle)
	{
		$('.object_new').on('click', function()
		{
			newObject(null);
		});
		
		$('.object_filter').on('click', function()
		{
			objectFilter();
		});
	}
	else
	{
		$('.object_new').off('click');
		$('.object_filter').off('click');
	}
}

function toggleObjectTableListeners(toggle)
{
	if(toggle)
	{
		$('#objectTab').find('.objectrow').on('dblclick', function()
		{
			activeObjectrow = $(this).attr('object_id');
			getObject(activeObjectrow);
		});
		
		$('#objectTab').find('.objectrow').on('touchstart', function()
		{
			activeObjectrow = $(this).attr('object_id');
			getObject(activeObjectrow);
		});
				
		$('#objectTab').find('.listSortable').on('click', function()
		{
			updateObjectSorting($(this));
		});
		
		$('#objectTab').find('.objectrow').on('mouseenter mouseleave', function()
		{
			$(this).toggleClass('filterActive');
		});
		
		$('#objectTab').find('.objectrow').on('touchstart', function()
		{
			activeObjectrow = $(this).attr('object_id');
			getObject(activeObjectrow);
			//alert('test');
		});

	}
	else
	{
		$('#objectTab').find('.objectrow').off('dblclick');
		$('#objectTab').find('.objectrow').off('touchend');
		$('#objectTab').find('.objectrow').off('mouseenter mouseleave');
		$('#objectTab').find('.listSortable').off('click');
	}
}


function objectFilter()
{
	var dialogId = filterDialog($('#object_filter'), 'Filter ' + langStrings.objectlist_header);
	dialogContainer[dialogId] = ['objectfilter', dialogId, 'open'];
	
	
	$('#objectFilterForm').on('submit', function(e)
	{
		e.preventDefault();
		updateObjectFilters(false);
	});
	
	$('#objectFilterForm').on('reset', function(e)
	{
		updateObjectFilters(true);
	});
}


function updateObjectFilters(reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		$('#objectFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		$('#objectFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		if(i > 0)
			$('.object_filter').addClass('filterActive');
		else
			$('.object_filter').removeClass('filterActive');
	}
	else
		$('.object_filter').removeClass('filterActive');
	
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateFilters/',
		data: {'filters': filters, 'tablename': 'object'},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateObjectTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			$('#object_filter').dialog( "close" );
			progressOverlay(false);
		},
	});	
}

function updateObjectSorting(th)
{
	var sortfield = $('#objecttable').find('.listSortableIcon').parent().attr('sortfield')
	var sortdirection = $('#objecttable').find('.listSortableIcon').hasClass('asc') ? 'asc' : 'desc';
	
	if(th.attr('sortfield') == sortfield)
	{
		if(sortdirection == 'asc')
			sortdirection = 'desc';
		else
			sortdirection = 'asc';
	}
	else
	{
		sortfield = th.attr('sortfield');
		sortdirection = 'asc';
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateSorting/',
		data: {'tablename': 'object', 'sortfield': sortfield, 'sortdirection': sortdirection},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateObjectTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}



