$(document).ready(function()
{
	getAppData();
	ignite();
	
});

var activeObjectrow = null;
var activeContractrow = null;
var activeUserrow = null;
var activeClientrow = null;
var activeEquipmentgrouprow = null;
var dialogId = 0;
var dialogContainer = [];
var langStrings = [];
var user = [];


function getAppData()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/getAppData/',
		data: 
		{
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			langStrings = ret.lang;
			user = ret.user;
		}
	});
}




function ignite()
{
	$('#container').tabs(
	{
		'active': 0,
	});
	$('.menu li').button();
	$('#logout').button();
	$('#notifications').button();
	$('#profile').button();
	
	$('#notifications').on('click', function()
	{
		$('#notificationlist').toggleClass('notificationlistHidden');
	});
	
	$('#profile').on('click', function()
	{
		showProfile();
	});
	
	$('.notification_text').on('click', function()
	{
		switch(parseInt($(this).parent().attr('type')))
		{
			case 0:
				getContract(parseInt($(this).parent().attr('contract_id')));
				$('#notificationlist').toggleClass('notificationlistHidden');
				break;
		}
	});
	
	$('.notification_dismiss').on('click', function()
	{
		switch(parseInt($(this).parent().attr('type')))
		{
			case 0:
				dismissNotification($(this));
				break;
		}
		
	});
}

function contentDialog(html, title)
{
	var newDialogId = dialogId++;
	$('#dialogContainer').append('<div class="dialogWindow" dialogId="' + newDialogId + '">' + html + '</div>');
	$('.dialogWindow[dialogId="' + newDialogId + '"]').dialog(
	{
		'title': title,
		'maxHeight': wHeight * 0.95,
		'maxWidth': wWidth * 0.95,
		'width': wWidth * 0.9,
		'height': wHeight * 0.9,
		'position': {'my': 'center', 'at': 'center'},
		'beforeClose': function(event, ui)
		{
			dialogContainer[$('#' + event.target.id).attr('dialogId')][2] = 'closed';
		},
	});
	
	$('.dialogWindow[dialogId="' + newDialogId + '"]').dialogExtend({
		'minimizable': true,
		'beforeMinimize': function(event)
		{
			dialogContainer[$(this).attr('dialogId')][2] = 'minimized';
		},
		'beforeRestore': function(event)
		{
			dialogContainer[$(this).attr('dialogId')][2] = 'open';
		},
	});
	
	return newDialogId;
}

function messageDialog(type, content)
{
	$('#dialogContainer').append('<div class="dialogInfo" type="' + type + '" dialogId="-1">' + content + '</div>');
	
	$('.dialogInfo[dialogId="-1"]').dialog(
	{
		'title': type == 'error' ? 'ERROR' : 'INFO',
		'width': wWidth * 0.3,
		'height': wHeight * 0.2,
		'position': {'my': 'center', 'at': 'center'},
		'modal': true,
		'buttons':
		{
			'OK': function()
			{
				$(this).dialog('close');
			}
		},
		'focus': function(event, ui)
		{
			switch($(this).attr('type'))
			{
			case 'error':
				$(this).parent().find('.ui-dialog-titlebar').addClass('ui-state-error');
				break;
			case 'info':
				$(this).parent().find('.ui-dialog-titlebar').addClass('ui-state-info');
				break;
			case 'success':
				$(this).parent().find('.ui-dialog-titlebar').addClass('ui-state-active');
				break;
			}
		},
		
		'close': function(event, ui)
		{
			$(this).dialog('destroy').remove();
		}
	});
}

function filterDialog(filterDialog, title)
{
	var newDialogId = dialogId++;
	filterDialog.attr('dialogid', newDialogId);
	filterDialog.dialog(
	{
		'title': title,
		'maxHeight': wHeight * 0.95,
		'maxWidth': wWidth * 0.95,
		'width': 500,
		'position': {'my': 'center', 'at': 'center'},
		'dialogClass': 'filterDialogOverflow',
		'beforeClose': function(event, ui)
		{
			dialogContainer[$('#' + event.target.id).attr('dialogId')][2] = 'closed';
		}
	});
	
	filterDialog.dialogExtend({
		'minimizable': true,
		'beforeMinimize': function(event)
		{
			dialogContainer[$(this).attr('dialogId')][2] = 'minimized';
		},
		'beforeRestore': function(event)
		{
			dialogContainer[$(this).attr('dialogId')][2] = 'open';
		},
	});
	
	filterDialog.find('.filterButton').button();
	filterDialog.find('.combobox').combobox();
	
	return newDialogId;	
}

function confirmationDialog(type, content, option1, option2)
{
	$('#dialogContainer').append('<div class="dialogConfirmation" type="' + type + '" dialogId="-1">' + content + '</div>');
	$('.dialogConfirmation[dialogId="-1"]').dialog(
	{
		'title': 'INFO',
		'width': wWidth * 0.3,
		'height': wHeight * 0.2,
		'position': {'my': 'center', 'at': 'center'},
		'modal': true,
		
		'focus': function(event, ui)
		{
			switch($(this).attr('type'))
			{
			case 'error':
				$(this).parent().find('.ui-dialog-titlebar').addClass('ui-state-error');
				break;
			case 'info':
				$(this).parent().find('.ui-dialog-titlebar').addClass('ui-state-info');
				break;
			case 'success':
				$(this).parent().find('.ui-dialog-titlebar').addClass('ui-state-active');
				break;
			}
		},
		
		'close': function(event, ui)
		{
			$(this).dialog('destroy').remove();
		}
	});
	
	$('.dialogConfirmation[dialogId="-1"]').dialog('option', 'buttons', [
    {
    	'text': option1[0],
    	'click': function()
    	{
    		if(option1[1] != null)
    			window[option1[1]](option1[2]);
    		$( this ).dialog( "close" );
    	},
    },
    {
    	'text': option2[0],
    	'click': function()
    	{
    		if(option2[1] != null)
    			window[option2[1]](option2[2]);
    		$( this ).dialog( "close" );
    	},
    }]);
	
}


function isDialogOpen(type, id)
{
	ret = false;
	for(var i = 0 ; i < dialogContainer.length ; i++)
	{
		if(dialogContainer[i][0] == type && dialogContainer[i][1] == id && ret == false)
		{
			switch(dialogContainer[i][2])
			{
				case 'minimized':
					$('.dialogWindow[dialogId="' + i + '"]').dialogExtend('restore');
					ret = true;
				case 'open':
					$('.dialogWindow[dialogId="' + i + '"]').dialog('moveToTop');
					ret = true;
					break;
			}
		}
	}
	
	return ret;
}



function fixDialogCss(dialog, offset)
{
	dialog.find('.dialogColumnContainer').height(parseInt(dialog.height()) - offset);
	if(wWidth < 1200)
	{
		$('.dialogColumn').css({'float': 'none', 'margin-right': 0, 'width': '100%'});
	}
}


function progressOverlay(toggle)
{
	if(toggle)
	{
		if(!$('#progressOverlay').is(':visible'))
		{
			$('#progressOverlay').show();
		}
	}
	else
	{
		if($('#progressOverlay').is(':visible'))
		{
			$('#progressOverlay').hide();
		}
	}
}


function dismissNotification(notification)
{
	
	$.ajax(
	{
		url: rootUrl + 'Inventory/dismissNotification/' + notification.parent().attr('notification_id'),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				notification.parent().fadeOut(150, function()
				{
					$(this).remove();
					if($('.notification').length <= 0)
					{
						$('#notifications').removeClass('notificationsActive');
						$('#notificationlist').toggleClass('notificationlistHidden');
					}
				});
			else
				messageDialog('error', ret.message);
		},
	});	
}



function showProfile()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/profile',
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.html, langStrings.profile);
				dialogContainer[dID] = ['profile', null, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixObjectDialog(dialog);
				fixDialogCss(dialog, 142);
				dialog.find('form').on('submit', function(e)
				{
					e.preventDefault();
					$.ajax(
					{
						url: rootUrl + 'Inventory/updateUser/',
						data: $(this).serialize(),
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							updateUserTable();
							
							if(ret.success)
							{
								messageDialog('success', ret.message);
							}
							else
							{
								messageDialog('error', ret.message);
							}
						},
					});
				});
			}
			else
				messageDialog('error', ret.message);
		},
	});		
}


(function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );

