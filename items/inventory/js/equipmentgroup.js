$(document).ready(function()
{
	toggleEquipmentgroupMenuListeners(true);
	toggleEquipmentgroupTableListeners(true);
});



function updateEquipmentgroupTable()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/refreshEquipmentgroupList/',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleEquipmentgroupTableListeners(false);
				$('#equipmentgrouptable').replaceWith(ret.list);
				toggleEquipmentgroupTableListeners(true);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}


function getEquipmentgroup(equipmentgroupId)
{
	if( equipmentgroupId == null || (equipmentgroupId != null && !isDialogOpen('equipmentgroup', equipmentgroupId)) )
	{	
		$.ajax(
		{
			url: rootUrl + 'Inventory/getEquipmentgroup/' + equipmentgroupId,
			data: 
			{
			},
			method: 'GET',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					var dID = contentDialog(ret.data.html, ret.data.title);
					dialogContainer[dID] = ['equipmentgroup', ret.data.equipmentgroupId, 'open'];
					var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
					fixEquipmentgroupDialog(dialog);
					fixDialogCss(dialog, 142);
					toggleEquipmentgroupDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			}
		});
	}
}


function newEquipmentgroup(equipmentgroupId)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/newEquipmentgroup/',
		data: 
		{
			'equipmentgroupId': equipmentgroupId,
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.data.html, ret.data.title);
				dialogContainer[dID] = ['equipmentgroup', ret.data.equipmentgroupId, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixDialogCss(dialog, 0);
				fixEquipmentgroupDialog(dialog);
				
				toggleEquipmentgroupDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		}
	});
}


function updateEquipmentgroup(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateEquipmentgroup/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.equipmentgroupDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				updateEquipmentgroupTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteEquipmentgroup(dialog)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteEquipmentgroup/',
		data: {
			'id': dialogContainer[dialog.attr('dialogId')][1],
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.dialog('close');
				updateEquipmentgroupTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertEquipmentgroup(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertEquipmentgroup/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.dialog('close');
				updateEquipmentgroupTable();
				getEquipmentgroup(ret.new_id);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}



function fixEquipmentgroupDialog(dialog)
{
	dialog.find('.equipmentgroupDetailTabs').tabs(
	{
		'active': 0,
	});
	
	dialog.find('.datepicker').datepicker(
	{
		'dateFormat': 'dd.mm.yy',
		'changeMonth': true,
		'changeYear': true,
		'altFormat': 'yy-mm-dd',
	});

	dialog.find('.combobox').combobox();
	
	dialog.find('input[type="submit"]').button();
	dialog.find('.dialogAction').button();
}



function toggleEquipmentgroupDetailListeners(toggle, dialog)
{
	if(toggle)
	{
		dialog.find('.equipmentgroupEditForm').on('submit', function(e)
		{
			e.preventDefault();
			updateEquipmentgroup(dialog, $(this));
		});
		
		dialog.find('.equipmentgroupAddForm').on('submit', function(e)
		{
			e.preventDefault();
			insertEquipmentgroup(dialog, $(this));
		});
		
		dialog.find('.equipmentgroupDelete').on('click', function()
		{
			confirmationDialog('info', langStrings.equipmentgroup_delete_text, [langStrings.objectdetail_delete_yes, 'deleteEquipmentgroup', dialog], [langStrings.objectdetail_delete_no, null, null]);
		});
		
		dialog.find('.equipmentgroupClone').on('click', function()
		{
			newEquipmentgroup(dialogContainer[dialog.attr('dialogid')][1]);
			dialog.dialog('close');
		});
		
	}
	else
	{
		dialog.find('.equipmentgroupEditForm').off('submit');
		dialog.find('.equipmentgroupAddForm').off('submit');
		dialog.find('.equipmentgroupDelete').off('click');
		dialog.find('.equipmentgroupClone').off('click');
	}
}


function toggleEquipmentgroupMenuListeners(toggle)
{
	if(toggle)
	{
		$('.equipmentgroup_new').on('click', function()
		{
			newEquipmentgroup(null);
		});
		
		$('.equipmentgroup_filter').on('click', function()
		{
			equipmentgroupFilter();
		});
	}
	else
	{
		$('.equipmentgroup_new').off('click');
		$('.equipmentgroup_filter').off('click');
	}
}

function toggleEquipmentgroupTableListeners(toggle)
{
	if(toggle)
	{
		$('#equipmentgroupTab').find('.equipmentgrouprow').on('dblclick', function()
		{
			activeEquipmentgrouprow = $(this).attr('equipmentgroup_id');
			getEquipmentgroup(activeEquipmentgrouprow);
		});
		
		$('#equipmentgroupTab').find('.equipmentgrouprow').on('touchstart', function()
		{
			activeEquipmentgrouprow = $(this).attr('equipmentgroup_id');
			getEquipmentgroup(activeEquipmentgrouprow);
		});
		
		$('#equipmentgroupTab').find('.listSortable').on('click', function()
		{
			updateEquipmentgroupSorting($(this));
		});
		
		$('#equipmentgroupTab').find('.equipmentgrouprow').on('mouseenter mouseleave', function()
		{
			$(this).toggleClass('filterActive');
		});

	}
	else
	{
		$('#equipmentgroupTab').find('.equipmentgrouprow').off('dblclick');
		$('#equipmentgroupTab').find('.equipmentgrouprow').off('touchstart');
		$('#equipmentgroupTab').find('.equipmentgrouprow').off('mouseenter mouseleave');
		$('#equipmentgroupTab').find('.listSortable').off('click');
	}
}


function equipmentgroupFilter()
{
	var dialogId = filterDialog($('#equipmentgroup_filter'), 'Filter ' + langStrings.equipmentgroup_tab);
	dialogContainer[dialogId] = ['equipmentgroupfilter', dialogId, 'open'];
	
	
	$('#equipmentgroupFilterForm').on('submit', function(e)
	{
		e.preventDefault();
		updateEquipmentgroupFilters(false);
	});
	
	$('#equipmentgroupFilterForm').on('reset', function(e)
	{
		updateEquipmentgroupFilters(true);
	});
}


function updateEquipmentgroupFilters(reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		$('#equipmentgroupFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		$('#equipmentgroupFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		if(i > 0)
			$('.equipmentgroup_filter').addClass('filterActive');
		else
			$('.equipmentgroup_filter').removeClass('filterActive');
	}
	else
		$('.equipmentgroup_filter').removeClass('filterActive');
	
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateFilters/',
		data: {'filters': filters, 'tablename': 'equipmentgroup'},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateEquipmentgroupTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			$('#equipmentgroup_filter').dialog( "close" );
			progressOverlay(false);
		},
	});	
}

function updateEquipmentgroupSorting(th)
{
	var sortfield = $('#equipmentgrouptable').find('.listSortableIcon').parent().attr('sortfield')
	var sortdirection = $('#equipmentgrouptable').find('.listSortableIcon').hasClass('asc') ? 'asc' : 'desc';
	
	if(th.attr('sortfield') == sortfield)
	{
		if(sortdirection == 'asc')
			sortdirection = 'desc';
		else
			sortdirection = 'asc';
	}
	else
	{
		sortfield = th.attr('sortfield');
		sortdirection = 'asc';
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateSorting/',
		data: {'tablename': 'equipmentgroup', 'sortfield': sortfield, 'sortdirection': sortdirection},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateEquipmentgroupTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}