var wWidth;
var wHeight;

$(document).ready(function()
{	
	resize();
	$(window).resize(function()
	{
		resize();
	});
});

function resize()
{
	wWidth = $(window).width();
	wHeight = $(window).height();
	
	$('.entityTabContent').height(wHeight - 130);
}
