$(document).ready(function()
{
	toggleUserMenuListeners(true);
	toggleUserTableListeners(true);
});



function updateUserTable()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/refreshUserList/',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleUserTableListeners(false);
				$('#usertable').replaceWith(ret.list);
				toggleUserTableListeners(true);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}


function getUser(userId)
{
	if( userId == null || (userId != null && !isDialogOpen('user', userId)) )
	{	
		$.ajax(
		{
			url: rootUrl + 'Inventory/getUser/' + userId,
			data: 
			{
			},
			method: 'GET',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					var dID = contentDialog(ret.data.html, ret.data.title);
					dialogContainer[dID] = ['user', ret.data.userId, 'open'];
					var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
					fixUserDialog(dialog);
					fixDialogCss(dialog, 142);
					toggleUserDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			}
		});
	}
}


function newUser(userId)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/newUser/',
		data: 
		{
			'userId': userId,
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.data.html, ret.data.title);
				dialogContainer[dID] = ['user', ret.data.userId, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixDialogCss(dialog, 0);
				fixUserDialog(dialog);
				
				toggleUserDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		}
	});
}


function updateUser(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateUser/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.userDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				updateUserTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteUser(dialog)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteUser/',
		data: {
			'id': dialogContainer[dialog.attr('dialogId')][1],
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.dialog('close');
				updateUserTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertUser(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertUser/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.dialog('close');
				updateUserTable();
				getUser(ret.new_id);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}



function fixUserDialog(dialog)
{
	dialog.find('.userDetailTabs').tabs(
	{
		'active': 0,
	});
	
	dialog.find('.datepicker').datepicker(
	{
		'dateFormat': 'dd.mm.yy',
		'changeMonth': true,
		'changeYear': true,
		'altFormat': 'yy-mm-dd',
	});

	dialog.find('.combobox').combobox();
	
	dialog.find('input[type="submit"]').button();
	dialog.find('.dialogAction').button();
}



function toggleUserDetailListeners(toggle, dialog)
{
	if(toggle)
	{
		dialog.find('.userEditForm').on('submit', function(e)
		{
			e.preventDefault();
			updateUser(dialog, $(this));
		});
		
		dialog.find('.userAddForm').on('submit', function(e)
		{
			e.preventDefault();
			insertUser(dialog, $(this));
		});
		
		dialog.find('.userDelete').on('click', function()
		{
			confirmationDialog('info', langStrings.userdetail_delete_text, [langStrings.objectdetail_delete_yes, 'deleteUser', dialog], [langStrings.objectdetail_delete_no, null, null]);
		});
		
		dialog.find('.userClone').on('click', function()
		{
			newUser(dialogContainer[dialog.attr('dialogid')][1]);
			dialog.dialog('close');
		});
		
	}
	else
	{
		dialog.find('.userEditForm').off('submit');
		dialog.find('.userAddForm').off('submit');
		dialog.find('.userDelete').off('click');
		dialog.find('.userClone').off('click');
	}
}


function toggleUserMenuListeners(toggle)
{
	if(toggle)
	{
		$('.user_new').on('click', function()
		{
			newUser(null);
		});
		
		$('.user_filter').on('click', function()
		{
			userFilter();
		});
	}
	else
	{
		$('.user_new').off('click');
		$('.user_filter').off('click');
	}
}

function toggleUserTableListeners(toggle)
{
	if(toggle)
	{
		$('#userTab').find('.userrow').on('dblclick', function()
		{
			activeUserrow = $(this).attr('user_id');
			getUser(activeUserrow);
		});
		
		$('#userTab').find('.userrow').on('touchstart', function()
		{
			activeUserrow = $(this).attr('user_id');
			getUser(activeUserrow);
		});
		
		$('#userTab').find('.listSortable').on('click', function()
		{
			updateUserSorting($(this));
		});
		
		$('#userTab').find('.userrow').on('mouseenter mouseleave', function()
		{
			$(this).toggleClass('filterActive');
		});

	}
	else
	{
		$('#userTab').find('.userrow').off('dblclick');
		$('#userTab').find('.userrow').off('touchstart');
		$('#userTab').find('.userrow').off('mouseenter mouseleave');
		$('#userTab').find('.listSortable').off('click');
	}
}


function userFilter()
{
	var dialogId = filterDialog($('#user_filter'), 'Filter ' + langStrings.user_tab);
	dialogContainer[dialogId] = ['userfilter', dialogId, 'open'];
	
	
	$('#userFilterForm').on('submit', function(e)
	{
		e.preventDefault();
		updateUserFilters(false);
	});
	
	$('#userFilterForm').on('reset', function(e)
	{
		updateUserFilters(true);
	});
}


function updateUserFilters(reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		$('#userFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		$('#userFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		if(i > 0)
			$('.user_filter').addClass('filterActive');
		else
			$('.user_filter').removeClass('filterActive');
	}
	else
		$('.user_filter').removeClass('filterActive');
	
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateFilters/',
		data: {'filters': filters, 'tablename': 'user'},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateUserTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			$('#user_filter').dialog( "close" );
			progressOverlay(false);
		},
	});	
}

function updateUserSorting(th)
{
	var sortfield = $('#usertable').find('.listSortableIcon').parent().attr('sortfield')
	var sortdirection = $('#usertable').find('.listSortableIcon').hasClass('asc') ? 'asc' : 'desc';
	
	if(th.attr('sortfield') == sortfield)
	{
		if(sortdirection == 'asc')
			sortdirection = 'desc';
		else
			sortdirection = 'asc';
	}
	else
	{
		sortfield = th.attr('sortfield');
		sortdirection = 'asc';
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateSorting/',
		data: {'tablename': 'user', 'sortfield': sortfield, 'sortdirection': sortdirection},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateUserTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}