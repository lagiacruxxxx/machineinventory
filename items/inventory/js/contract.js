$(document).ready(function()
{
	toggleContractMenuListeners(true);
	toggleContractTableListeners(true);
});


function updateContractTable()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/refreshContractList/',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleContractTableListeners(false);
				$('#contracttable').replaceWith(ret.list);
				toggleContractTableListeners(true);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}


function getContract(contractId)
{
	if( contractId == null || (contractId != null && !isDialogOpen('contract', contractId)) )
	{	
		$.ajax(
		{
			url: rootUrl + 'Inventory/getContract/' + contractId,
			data: 
			{
			},
			method: 'GET',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					var dID = contentDialog(ret.data.html, ret.data.title);
					dialogContainer[dID] = ['contract', ret.data.contractId, 'open'];
					var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
					fixContractDialog(dialog);
					fixDialogCss(dialog, 142);
					toggleContractDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			}
		});
	}
}


function newContract(contractId)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/newContract/',
		data: 
		{
			'contractId': contractId,
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.data.html, ret.data.title);
				dialogContainer[dID] = ['contract', ret.data.contractId, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixDialogCss(dialog, 0);
				fixContractDialog(dialog);
				
				toggleContractDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		}
	});
}


function updateContract(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateContract/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.contractDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				updateContractTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteContract(dialog)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteContract/',
		data: {
			'id': dialogContainer[dialog.attr('dialogId')][1],
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.dialog('close');
				updateContractTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertContract(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertContract/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.dialog('close');
				updateContractTable();
				getContract(ret.new_id);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertContractFile(dialog)
{
	var xhr = new XMLHttpRequest();		
	var fd = new FormData;
	var files = dialog.find('.fileupload_edit_file').prop('files');
	
	if(files.length != 1)
		messageDialog('error', dialog.find('.fileupload_edit_file').attr('error_empty'));
	else
	{
		fd.append('data', files[0]);
		fd.append('filename', files[0].name);
		fd.append('filedescription', dialog.find('.contractFileDesc').val());
		fd.append('contract_id', dialog.find('.contractFileId').val());
		
		xhr.addEventListener('load', function(e) 
		{
			var ret = $.parseJSON(this.responseText);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.contractFileDesc').val('');
				dialog.find('.fileupload_edit_file').replaceWith( dialog.find('.fileupload_edit_file').clone( true ) );
				dialog.find('.fileupload_items').empty().append(ret.files);
			}
			else
			{
				messageDialog('error', ret.message);
			}
	    });
		
		xhr.open('post', rootUrl + 'Inventory/insertContractFile/');
		xhr.send(fd);
	}
	
}



function fixContractDialog(dialog)
{
	dialog.find('.contractDetailTabs').tabs(
	{
		'active': 0,
	});
	
	$('.datepicker').datepicker(
	{
		'dateFormat': 'dd.mm.yy',
		'changeMonth': true,
		'changeYear': true,
		'altFormat': 'yy-mm-dd',
	});

	dialog.find('.combobox').combobox();
	
	dialog.find('input[type="submit"]').button();
	dialog.find('.dialogAction').button();
	
	dialog.find('.contract_object_select').button();
	dialog.find('.contract_object_select_add').button();
	dialog.find('.contract_object_service').button();
	dialog.find('.contract_object_add').button();
	dialog.find('.contractobjectFilterSubmit').button();
}



function toggleContractDetailListeners(toggle, dialog)
{
	if(toggle)
	{
		dialog.find('.contractEditForm').on('submit', function(e)
		{
			e.preventDefault();
			updateContract(dialog, $(this));
		});
		
		dialog.find('.contractAddForm').on('submit', function(e)
		{
			e.preventDefault();
			insertContract(dialog, $(this));
		});
		
		dialog.find('.fileupload_edit_form').on('submit', function(e)
		{
			e.preventDefault();
			insertContractFile(dialog, $(this));
		});		
		
		dialog.find('.fileupload_edit_file').on('click', function(e)
		{
			if($(this).prop('files').length > 0)
			{
				$(this).wrap('<form>').closest('form').get(0).reset();
				$(this).unwrap();
				e.preventDefault();
				$(this).click();
			}
			progressOverlay(true);
		});
		
		dialog.find('.fileupload_edit_file').on('change', function()
		{
			progressOverlay(false);
		});
		
		dialog.find('.contractDelete').on('click', function()
		{
			confirmationDialog('info', langStrings.contractdetail_delete_text, [langStrings.objectdetail_delete_yes, 'deleteContract', dialog], [langStrings.objectdetail_delete_no, null, null]);
		});
		
		dialog.find('.contractClone').on('click', function()
		{
			newContract(dialogContainer[dialog.attr('dialogid')][1]);
			dialog.dialog('close');
		});
		
		dialog.find('.contract_objectrow').on('dblclick', function()
		{
			getObject($(this).attr('object_id'));
		});
		
		dialog.find('.contract_objectrow').on('touchstart', function()
		{
			getObject($(this).attr('object_id'));
		});
		
		dialog.find('.contract_object_select, .contract_object_select_add').on('click', function()
		{
			if($(this).attr('checkstate') == 'unchecked')
			{
				$(this).attr('checkstate', 'checked');
				$(this).parent().find('input[type="checkbox"]').prop('checked', true);
			}
			else
			{
				$(this).attr('checkstate', 'unchecked');
				$(this).parent().find('input[type="checkbox"]').prop('checked', false);
			}
		});
		
		dialog.find('.contract_object_service').on('click', function()
		{
			serviceContractObjects(dialog);
		});
		
		dialog.find('.contract_object_add').on('click', function()
		{
			addContractObjects(dialog);
		});
		
		dialog.find('.contractobjectFilterForm').on('submit', function(e)
		{
			e.preventDefault();
			filterAddView(dialog, false);
		});
		
		dialog.find('.contractobjectFilterForm').on('reset', function(e)
		{
			e.preventDefault();
			filterAddView(dialog, true);
		});
	}
	else
	{
		dialog.find('.contractEditForm').off('submit');
		dialog.find('.contractAddForm').off('submit');
		dialog.find('.history_edit_form').off('submit');
		dialog.find('.fileupload_edit_form').off('submit');
		dialog.find('.fileupload_edit_file').off('change');
		dialog.find('.fileupload_edit_file').off('click');
		dialog.find('.contractDelete').off('click');
		dialog.find('.contractClone').off('click');
		dialog.find('.contract_object_select, .contract_object_select_add').off('click');
		dialog.find('.contract_object_service').off('click');
		dialog.find('.contract_object_add').off('click');
		dialog.find('.contractobjectFilterForm').off('submit');
		dialog.find('.contractobjectFilterForm').off('reset');
		
	}
}

function toggleContractMenuListeners(toggle)
{
	if(toggle)
	{
		$('.contract_new').on('click', function()
		{
			newContract(null);
		});
		
		$('.contract_filter').on('click', function()
		{
			contractFilter();
		});
	}
	else
	{
		$('.contract_new').off('click');
		$('.contract_filter').off('click');
	}
}

function toggleContractTableListeners(toggle)
{
	if(toggle)
	{
		$('#contractTab').find('.contractrow').on('dblclick', function()
		{
			activeContractrow = $(this).attr('contract_id');
			getContract(activeContractrow);
		});
		
		$('#contractTab').find('.contractrow').on('touchstart', function()
		{
			activeContractrow = $(this).attr('contract_id');
			getContract(activeContractrow);
		});
		
		$('#contractTab').find('.listSortable').on('click', function()
		{
			updateContractSorting($(this));
		});
		
		$('#contractTab').find('.contractrow').on('mouseenter mouseleave', function()
		{
			$(this).toggleClass('filterActive');
		});

	}
	else
	{
		$('#contractTab').find('.contractrow').off('dblclick');
		$('#contractTab').find('.contractrow').off('touchstart');
		$('#contractTab').find('.contractrow').off('mouseenter mouseleave');
		$('#contractTab').find('.listSortable').off('click');
	}
}


function updateContractFilters(reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		$('#contractFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		$('#contractFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		if(i > 0)
			$('.contract_filter').addClass('filterActive');
		else
			$('.contract_filter').removeClass('filterActive');
	}
	else
		$('.contract_filter').removeClass('filterActive');
	
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateFilters/',
		data: {'filters': filters, 'tablename': 'contract'},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateContractTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			$('#contract_filter').dialog( "close" );
			progressOverlay(false);
		},
	});	
}

function updateContractSorting(th)
{
	var sortfield = $('#contracttable').find('.listSortableIcon').parent().attr('sortfield')
	var sortdirection = $('#contracttable').find('.listSortableIcon').hasClass('asc') ? 'asc' : 'desc';
	
	if(th.attr('sortfield') == sortfield)
	{
		if(sortdirection == 'asc')
			sortdirection = 'desc';
		else
			sortdirection = 'asc';
	}
	else
	{
		sortfield = th.attr('sortfield');
		sortdirection = 'asc';
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateSorting/',
		data: {'tablename': 'contract', 'sortfield': sortfield, 'sortdirection': sortdirection},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateContractTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}


function contractFilter()
{
	var dialogId = filterDialog($('#contract_filter'), 'Filter ' + langStrings.contract_tab);
	dialogContainer[dialogId] = ['contractfilter', dialogId, 'open'];
	
	
	$('#contractFilterForm').on('submit', function(e)
	{
		e.preventDefault();
		updateContractFilters(false);
	});
	
	$('#contractFilterForm').on('reset', function(e)
	{
		updateContractFilters(true);
	});
}


function serviceContractObjects(dialog)
{
	
	$('#contract_service').dialog(
	{
		'title': langStrings.object_service_text,
		'width': wWidth * 0.4,
		'height': wHeight * 0.3,
		'position': {'my': 'center', 'at': 'center'},
		'modal': true,
		
		'focus': function(event, ui)
		{
			$(this).find('#contract_service_comment').css({'height': $(this).height() - 90});
			$(this).find('.filterButton').button();
		},

	});
	
	$('#contractServiceForm').on('submit', function(e)
	{
		e.preventDefault();
		
		var objects = [];
		dialog.find('.contract_objecttable').find('input[type="checkbox"]:checked').each(function()
		{
			objects.push($(this).parent().parent().attr('object_id'));
		});
		$.ajax(
		{
			url: rootUrl + 'Inventory/serviceContractObjects/',
			data: {
				'objects': JSON.stringify(objects),
				'comment': $('#contract_service_comment').val(),
				'contractId': dialogContainer[dialog.attr('dialogId')][1],
				},
			method: 'POST',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					toggleContractDetailListeners(false, dialog);
					$('#contract_service').dialog('close');
					dialog.find('.contract_objecttable').empty().append(ret.table);
					messageDialog('success', ret.message);
					toggleContractDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			},
		});
		$('#contractServiceForm').off('submit');
		$('#contractServiceForm').off('reset');
	});
	
	$('#contractServiceForm').on('reset', function(e)
	{
		$('#contract_service').dialog('close');
		$('#contractServiceForm').off('submit');
		$('#contractServiceForm').off('reset');

	});
}


function addContractObjects(dialog)
{
	var objects = [];
	dialog.find('.contract_objectadd').find('input[type="checkbox"]:checked').each(function()
	{
		objects.push($(this).parent().parent().attr('object_id'));
	});
	
	var i = 0;
	var filters = [];
	dialog.find('.contractobjectFilterForm input[type="text"]').each(function()
	{
		if($(this).val() != '')
			filters[i++] = [$(this).attr('name'), $(this).val()];
	});
	
	
	dialog.find('.contractobjectFilterForm .combobox').each(function()
	{
		if($(this).val() != '-1')
			filters[i++] = [$(this).attr('name'), $(this).val()];
	});
	
	$.ajax(
	{
		url: rootUrl + 'Inventory/addContractObjects/',
		data: {
			'objects': JSON.stringify(objects),
			'contractId': dialogContainer[dialog.attr('dialogId')][1],
			'filters': filters,
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleContractDetailListeners(false, dialog);
				dialog.find('.contract_objectadd').empty().append(ret.table);
				dialog.find('.contract_objecttable').empty().append(ret.list);
				messageDialog('success', ret.message);
				toggleContractDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});	
}


function filterAddView(dialog, reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		dialog.find('.contractobjectFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		dialog.find('.contractobjectFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateContractObjectFilters',
		data: {'filters': filters, 'contractId': dialogContainer[dialog.attr('dialogId')][1]},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.find('.contract_objectadd').replaceWith(ret.html);
			}
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}


