$(document).ready(function()
{
	toggleContactMenuListeners(true);
	toggleContactTableListeners(true);
});



function updateContactTable()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/refreshContactList/',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleContactTableListeners(false);
				$('#contacttable').replaceWith(ret.list);
				toggleContactTableListeners(true);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}


function getContact(contactId)
{
	if( contactId == null || (contactId != null && !isDialogOpen('contact', contactId)) )
	{	
		$.ajax(
		{
			url: rootUrl + 'Inventory/getContact/' + contactId,
			data: 
			{
			},
			method: 'GET',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					var dID = contentDialog(ret.data.html, ret.data.title);
					dialogContainer[dID] = ['contact', ret.data.contactId, 'open'];
					var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
					fixContactDialog(dialog);
					fixDialogCss(dialog, 142);
					toggleContactDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			}
		});
	}
}


function newContact(contactId)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/newContact/',
		data: 
		{
			'contactId': contactId,
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.data.html, ret.data.title);
				dialogContainer[dID] = ['contact', ret.data.contactId, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixDialogCss(dialog, 0);
				fixContactDialog(dialog);
				
				toggleContactDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		}
	});
}


function updateContact(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateContact/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.contactDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				updateContactTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteContact(dialog)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteContact/',
		data: {
			'id': dialogContainer[dialog.attr('dialogId')][1],
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.dialog('close');
				updateContactTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertContact(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertContact/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.dialog('close');
				updateContactTable();
				getContact(ret.new_id);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}



function fixContactDialog(dialog)
{
	dialog.find('.contactDetailTabs').tabs(
	{
		'active': 0,
	});
	
	dialog.find('.datepicker').datepicker(
	{
		'dateFormat': 'dd.mm.yy',
		'changeMonth': true,
		'changeYear': true,
		'altFormat': 'yy-mm-dd',
	});

	dialog.find('.combobox').combobox();
	
	dialog.find('input[type="submit"]').button();
	dialog.find('.dialogAction').button();
}



function toggleContactDetailListeners(toggle, dialog)
{
	if(toggle)
	{
		dialog.find('.contactEditForm').on('submit', function(e)
		{
			e.preventDefault();
			updateContact(dialog, $(this));
		});
		
		dialog.find('.contactAddForm').on('submit', function(e)
		{
			e.preventDefault();
			insertContact(dialog, $(this));
		});
		
		dialog.find('.contactDelete').on('click', function()
		{
			confirmationDialog('info', langStrings.contactdetail_delete_text, [langStrings.objectdetail_delete_yes, 'deleteContact', dialog], [langStrings.objectdetail_delete_no, null, null]);
		});
		
		dialog.find('.contactClone').on('click', function()
		{
			newContact(dialogContainer[dialog.attr('dialogid')][1]);
			dialog.dialog('close');
		});
		
	}
	else
	{
		dialog.find('.contactEditForm').off('submit');
		dialog.find('.contactAddForm').off('submit');
		dialog.find('.contactDelete').off('click');
		dialog.find('.contactClone').off('click');
	}
}


function toggleContactMenuListeners(toggle)
{
	if(toggle)
	{
		$('.contact_new').on('click', function()
		{
			newContact(null);
		});
		
		$('.contact_filter').on('click', function()
		{
			contactFilter();
		});
	}
	else
	{
		$('.contact_new').off('click');
		$('.contact_filter').off('click');
	}
}

function toggleContactTableListeners(toggle)
{
	if(toggle)
	{
		$('#contactTab').find('.contactrow').on('dblclick', function()
		{
			activeContactrow = $(this).attr('contact_id');
			getContact(activeContactrow);
		});
		
		$('#contactTab').find('.contactrow').on('touchstart', function()
		{
			activeContactrow = $(this).attr('contact_id');
			getContact(activeContactrow);
		});
		
		$('#contactTab').find('.listSortable').on('click', function()
		{
			updateContactSorting($(this));
		});
		
		$('#contactTab').find('.contactrow').on('mouseenter mouseleave', function()
		{
			$(this).toggleClass('filterActive');
		});

	}
	else
	{
		$('#contactTab').find('.contactrow').off('dblclick');
		$('#contactTab').find('.contactrow').off('touchstart');
		$('#contactTab').find('.contactrow').off('mouseenter mouseleave');
		$('#contactTab').find('.listSortable').off('click');
	}
}


function contactFilter()
{
	var dialogId = filterDialog($('#contact_filter'), 'Filter ' + langStrings.contact_tab);
	dialogContainer[dialogId] = ['contactfilter', dialogId, 'open'];
	
	
	$('#contactFilterForm').on('submit', function(event)
	{
		event.preventDefault();
		updateContactFilters(false);
		return false;
	});
	
	$('#contactFilterForm').on('reset', function(event)
	{
		updateContactFilters(true);
	});
}


function updateContactFilters(reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		$('#contactFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		$('#contactFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		if(i > 0)
			$('.contact_filter').addClass('filterActive');
		else
			$('.contact_filter').removeClass('filterActive');
	}
	else
		$('.contact_filter').removeClass('filterActive');
	
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateFilters/',
		data: {'filters': filters, 'tablename': 'contact'},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateContactTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			$('#contact_filter').dialog( "close" );
			progressOverlay(false);
		},
	});	
}

function updateContactSorting(th)
{
	var sortfield = $('#contacttable').find('.listSortableIcon').parent().attr('sortfield')
	var sortdirection = $('#contacttable').find('.listSortableIcon').hasClass('asc') ? 'asc' : 'desc';
	
	if(th.attr('sortfield') == sortfield)
	{
		if(sortdirection == 'asc')
			sortdirection = 'desc';
		else
			sortdirection = 'asc';
	}
	else
	{
		sortfield = th.attr('sortfield');
		sortdirection = 'asc';
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateSorting/',
		data: {'tablename': 'contact', 'sortfield': sortfield, 'sortdirection': sortdirection},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateContactTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}