$(document).ready(function()
{
	toggleClientMenuListeners(true);
	toggleClientTableListeners(true);
});



function updateClientTable()
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/refreshClientList/',
		data: {},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				toggleClientTableListeners(false);
				$('#clienttable').replaceWith(ret.list);
				toggleClientTableListeners(true);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}


function getClient(clientId)
{
	if( clientId == null || (clientId != null && !isDialogOpen('client', clientId)) )
	{	
		$.ajax(
		{
			url: rootUrl + 'Inventory/getClient/' + clientId,
			data: 
			{
			},
			method: 'GET',
			success: function(data)
			{
				var ret = $.parseJSON(data);
				
				if(ret.success)
				{
					var dID = contentDialog(ret.data.html, ret.data.title);
					dialogContainer[dID] = ['client', ret.data.clientId, 'open'];
					var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
					fixClientDialog(dialog);
					fixDialogCss(dialog, 142);
					toggleClientDetailListeners(true, dialog);
				}
				else
				{
					messageDialog('error', ret.message);
				}
			}
		});
	}
}


function newClient(clientId)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/newClient/',
		data: 
		{
			'clientId': clientId,
		},
		method: 'GET',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				var dID = contentDialog(ret.data.html, ret.data.title);
				dialogContainer[dID] = ['client', ret.data.userId, 'open'];
				var dialog = $('.dialogWindow[dialogId="' + dID + '"]');
				fixDialogCss(dialog, 0);
				fixClientDialog(dialog);
				
				toggleClientDetailListeners(true, dialog);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		}
	});
}


function updateClient(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateClient/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.find('.clientDetail_tab_overview').empty().append(ret.overview);
				fixDialogCss(dialog, 142);
				updateClientTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function deleteClient(dialog)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/deleteClient/',
		data: {
			'id': dialogContainer[dialog.attr('dialogId')][1],
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				messageDialog('success', ret.message);
				dialog.dialog('close');
				updateClientTable();
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}

function insertClient(dialog, form)
{
	$.ajax(
	{
		url: rootUrl + 'Inventory/insertClient/',
		data: form.serialize(),
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				dialog.dialog('close');
				updateClientTable();
				getClient(ret.new_id);
			}
			else
			{
				messageDialog('error', ret.message);
			}
		},
	});
}



function fixClientDialog(dialog)
{
	dialog.find('.clientDetailTabs').tabs(
	{
		'active': 0,
	});
	
	dialog.find('.datepicker').datepicker(
	{
		'dateFormat': 'dd.mm.yy',
		'changeMonth': true,
		'changeYear': true,
		'altFormat': 'yy-mm-dd',
	});

	dialog.find('.combobox').combobox();
	
	dialog.find('input[type="submit"]').button();
	dialog.find('.dialogAction').button();
}



function toggleClientDetailListeners(toggle, dialog)
{
	if(toggle)
	{
		dialog.find('.clientEditForm').on('submit', function(e)
		{
			e.preventDefault();
			updateClient(dialog, $(this));
		});
		
		dialog.find('.clientAddForm').on('submit', function(e)
		{
			e.preventDefault();
			insertClient(dialog, $(this));
		});
		
		dialog.find('.clientDelete').on('click', function()
		{
			confirmationDialog('info', langStrings.client_delete_text, [langStrings.objectdetail_delete_yes, 'deleteClient', dialog], [langStrings.objectdetail_delete_no, null, null]);
		});
		
		dialog.find('.clientClone').on('click', function()
		{
			newClient(dialogContainer[dialog.attr('dialogid')][1]);
			dialog.dialog('close');
		});
		
	}
	else
	{
		dialog.find('.clientEditForm').off('submit');
		dialog.find('.clientAddForm').off('submit');
		dialog.find('.clientDelete').off('click');
		dialog.find('.clientClone').off('click');
	}
}


function toggleClientMenuListeners(toggle)
{
	if(toggle)
	{
		$('.client_new').on('click', function()
		{
			newClient(null);
		});
		
		$('.client_filter').on('click', function()
		{
			clientFilter();
		});
	}
	else
	{
		$('.client_new').off('click');
		$('.client_filter').off('click');
	}
}

function toggleClientTableListeners(toggle)
{
	if(toggle)
	{
		$('#clientTab').find('.clientrow').on('dblclick', function()
		{
			activeClientrow = $(this).attr('client_id');
			getClient(activeClientrow);
		});
		
		$('#clientTab').find('.clientrow').on('touchstart', function()
		{
			activeClientrow = $(this).attr('client_id');
			getClient(activeClientrow);
		});
		
		$('#clientTab').find('.listSortable').on('click', function()
		{
			updateClientSorting($(this));
		});
		
		$('#clientTab').find('.clientrow').on('mouseenter mouseleave', function()
		{
			$(this).toggleClass('filterActive');
		});

	}
	else
	{
		$('#clientTab').find('.clientrow').off('dblclick');
		$('#clientTab').find('.clientrow').off('touchstart');
		$('#clientTab').find('.clientrow').off('mouseenter mouseleave');
		$('#clientTab').find('.listSortable').off('click');
	}
}


function clientFilter()
{
	var dialogId = filterDialog($('#client_filter'), 'Filter ' + langStrings.client_tab);
	dialogContainer[dialogId] = ['clientfilter', dialogId, 'open'];
	
	
	$('#clientFilterForm').on('submit', function(e)
	{
		e.preventDefault();
		updateClientFilters(false);
	});
	
	$('#clientFilterForm').on('reset', function(e)
	{
		updateClientFilters(true);
	});
}


function updateClientFilters(reset)
{
	var i = 0;
	var filters = [];
	
	if(!reset)
	{
		$('#clientFilterForm input[type="text"]').each(function()
		{
			if($(this).val() != '')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		$('#clientFilterForm .combobox').each(function()
		{
			if($(this).val() != '-1')
				filters[i++] = [$(this).attr('name'), $(this).val()];
		});
		
		if(i > 0)
			$('.client_filter').addClass('filterActive');
		else
			$('.client_filter').removeClass('filterActive');
	}
	else
		$('.client_filter').removeClass('filterActive');
	
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateFilters/',
		data: {'filters': filters, 'tablename': 'client'},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateClientTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			$('#client_filter').dialog( "close" );
			progressOverlay(false);
		},
	});	
}

function updateClientSorting(th)
{
	var sortfield = $('#clienttable').find('.listSortableIcon').parent().attr('sortfield')
	var sortdirection = $('#clienttable').find('.listSortableIcon').hasClass('asc') ? 'asc' : 'desc';
	
	if(th.attr('sortfield') == sortfield)
	{
		if(sortdirection == 'asc')
			sortdirection = 'desc';
		else
			sortdirection = 'asc';
	}
	else
	{
		sortfield = th.attr('sortfield');
		sortdirection = 'asc';
	}
	
	progressOverlay(true);
	$.ajax(
	{
		url: rootUrl + 'Inventory/updateSorting/',
		data: {'tablename': 'client', 'sortfield': sortfield, 'sortdirection': sortdirection},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
				updateClientTable();
			else
				messageDialog('error', ret.message);
		},
		complete: function()
		{
			progressOverlay(false);
		},
	});		
}